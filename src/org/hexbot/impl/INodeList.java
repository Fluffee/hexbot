package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/4/13
 * Time: 2:01 AM
 * To change this template use File | Settings | File Templates.
 */
public interface INodeList {
    public INode getHead();
    public INode getCurrent();
}
