package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/23/13
 * Time: 8:56 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ISceneObject {
    public IAnimable getModel();

    public int getId();

    public int getWorldX();

    public int getWorldY();

}
