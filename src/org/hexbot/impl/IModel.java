package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/23/13
 * Time: 8:58 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IModel extends IAnimable {
    public int[] getXVerticies();

    public int[] getYVerticies();

    public int[] getZVerticies();

    public int[] getXTriangles();

    public int[] getYTriangles();

    public int[] getZTriangles();


}
