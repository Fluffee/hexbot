package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/23/13
 * Time: 8:57 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IObjectDef {
    public String getName();
    public String[] getActions();
}
