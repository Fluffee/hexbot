package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Chris
 * Date: 2/24/13
 * Time: 12:04 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IItemDef {
    public String getName();

    public String[] getActions();
}
