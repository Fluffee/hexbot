package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 5/21/13
 * Time: 9:25 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IFriend {
    public int getWorld();

    public String getName();
}
