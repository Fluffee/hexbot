package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/23/13
 * Time: 8:55 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IAnimable extends INodeSub {
    Object getModel();

    public int getModelHeight();

    public void setModel(Object a);

}
