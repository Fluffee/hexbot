package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/23/13
 * Time: 8:56 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IGround {
    public IWallObject getWallObject();

    public IFloorDecoration getFloorDecoration();

    public IObject3 getObject3();

    public IObject4 getObject4();

    public ISceneObject[] getSceneObjects();
}
