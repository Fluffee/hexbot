package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/23/13
 * Time: 7:31 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IEntity extends IAnimable {
    public int getX();

    public int getY();

    public int getAnimation();

    public String getText();

    public int getHp();

    public int getMaxHp();

    public int getInteractingIndex();

    public int getOrientation();

    public int[] getQueueX();

    public int[] getQueueY();

    public boolean isAnimating();

    public int getSpeed();
}
