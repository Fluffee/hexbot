package org.hexbot.impl;

public interface IWidget {
    public IWidget getParent();

    public IWidget[] getComponents();

    public int getX();

    public int getY();

    public int getBorderThickness();

    public int getWidth();

    public int getHeight();

    public int getTradeId();

    public int getTradeStack();

    public boolean isHidden();

    public int getParentId();

    public int getId();

    public int getType();

    public int getComponentIndex();

    public String[] getActions();

    public String getComponentName();

    public String getText();

    public int getXRotation();

    public int getYRotation();

    public int getZRotation();

    public int[] getSlotIds();

    public int[] getStackSizes();

    public int getTextureId();

    public int getModelId();

    public int getScrollX();

    public int getScrollY();

    public int getMasterX();

    public int getMasterY();
}
