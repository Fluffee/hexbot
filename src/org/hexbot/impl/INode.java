package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/22/13
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
public interface INode {
    INode getNext();

    INode getPrevious();
}
