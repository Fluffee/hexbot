package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/20/13
 * Time: 4:59 PM
 * To change this template use File | Settings | File Templates.
 */
public interface INpc extends IEntity{
    public INpcDef getDefinition();
}
