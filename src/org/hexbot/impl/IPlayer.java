package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/20/13
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IPlayer extends IEntity {

    public String getName();

    public IPlayerDef getDefinition();

    public int getSkullIcon();

    public int getPrayerIcon();
}
