package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/22/13
 * Time: 3:30 PM
 * To change this template use File | Settings | File Templates.
 */
public interface INodeSub extends INode {

    INodeSub getNext();

    INodeSub getPrevious();
}
