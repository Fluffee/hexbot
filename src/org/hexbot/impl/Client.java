package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/20/13
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Client {
    public IItemDef getItemDefinition(int i);

    public int getMenuCount();

    public int getBaseX();

    public int getBaseY();

    public int getRunEnergy();

    public int CameraX();

    public int CameraY();

    public int CameraZ();

    public int getPitch();

    public int getYaw();

    public Object getMouse();

    public Object getKeyboard();

    public INodeList[][][] getItemArray();

    public ICollisionMap[] getCollisionMap();

    public int getLoginIndex();

    public int getGameState();

    public int[] getSkillLevelArray();

    public int[] getRealSkillLevelArray();

    public int[] getSkillExpArray();

    public byte[][][] getTileBytes();

    public int[][][] getTileHeights();

    public boolean isMenuOpen();

    public boolean isItemSelected();

    public int getMenuX();

    public int getMenuY();

    public int getMenuWidth();

    public int getMenuHeight();

    public String[] getMenuActions();

    public String[] getMenuOptions();

    public int getCompassAngle();

    public int getMapOffset();

    public int getMapScale();

    public int getPlane();

    public IFriend[] getFriends();

    public String getUsername();

    public IWidget[][] getWidgets();

    public int[] getSettings();

    public IPlayer getLocalPlayer();

    public IPlayer[] getPlayers();

    public INpc[] getNpcs();

    public IWorldController getWorldController();

    public int getMyPlayerIndex();
}
