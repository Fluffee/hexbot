package org.hexbot.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/24/13
 * Time: 1:04 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IWorldController {
    public IGround[][][] getGroundArray();
    public ISceneObject[] getObject5Cache();

}
