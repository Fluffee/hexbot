package org.hexbot.gui;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 5/1/13
 * Time: 10:56 PM
 * To change this template use File | Settings | File Templates.
 */

import org.hexbot.bot.Account;
import org.hexbot.bot.AccountManager;
import org.hexbot.bot.Bot;
import org.hexbot.bot.event.randoms.BreakHandler;
import org.hexbot.script.ScriptHandler;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.*;


public class AccountFrame extends JFrame {

	private final DefaultTableModel tableModel = new DefaultTableModel(
			new Object[][]{
			},
			new String[]{
					"Login", "Password", "Pin"
			}
	) {
		Class[] types = new Class[]{
				String.class, String.class, String.class
		};

		public Class getColumnClass(int columnIndex) {
			return types[columnIndex];
		}
	};
	private final JTable dataTable = new JTable();
	private final JScrollPane scrollPane = new JScrollPane(dataTable);
	private final JCheckBox breaks = new JCheckBox("Break");
	private final JSlider amount = new JSlider();
	private final Label text = new Label();
	private final JSlider frequency = new JSlider();
	private final JButton addButton = new JButton("Add");
	private final JButton removeButton = new JButton("Remove");
	private final JButton selectButton = new JButton("Select");
	private final AccountManager manager = new AccountManager();
	int a = 0;
	int b = 0;
	long time = 0;
	long fre = 0;

	public AccountFrame() {
		super("Accounts");
		setPreferredSize(new Dimension(300, 290));
		setLayout(null);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(false);
		breaks.setSize(80, 25);
		breaks.setLocation(4, 170);
		text.setSize(220, 25);
		text.setLocation(85, 170);
		amount.setSize(180, 25);
		amount.setLocation(125, 210);
		Label am = new Label("Frequency:");
		am.setLocation(5, 210);
		am.setSize(125, 25);
		Label fr = new Label("Break Duration:");
		fr.setLocation(5, 230);
		fr.setSize(125, 25);
		add(am);
		add(fr);
		amount.setMinimum(1);
		amount.setMaximum(5);
		amount.setMajorTickSpacing(1);
		amount.setSnapToTicks(true);
		frequency.setSize(180, 25);
		frequency.setLocation(125, 230);
		frequency.setMinimum(1);
		frequency.setMaximum(3);
		frequency.setMajorTickSpacing(1);
		frequency.setSnapToTicks(true);
		add(breaks);
		add(text);
		add(amount);
		add(frequency);
		scrollPane.setSize(295, 180);
		scrollPane.setLocation(1, 1);
		dataTable.setModel(tableModel);
		for (final Account account : manager.accounts) {
			tableModel.addRow(new Object[]{account.getUser(), account.getPass(), account.getPin()});
		}
		setEn();
		setAlwaysOnTop(false);
		dataTable.getTableHeader().setReorderingAllowed(false);
		dataTable.getColumnModel().getColumn(0).setMinWidth(125);
		dataTable.getColumnModel().getColumn(1).setMinWidth(125);
		dataTable.getColumnModel().getColumn(0).setResizable(false);
		dataTable.getColumnModel().getColumn(1).setResizable(false);
		dataTable.getColumnModel().getColumn(2).setResizable(false);
		dataTable.getColumnModel().getColumn(1).setCellRenderer(new PasswordCellRenderer());
		dataTable.getColumnModel().getColumn(1).setCellEditor(new PasswordCellEditor());
		dataTable.getColumnModel().getColumn(2).setCellRenderer(new PasswordCellRenderer());
		dataTable.getColumnModel().getColumn(2).setCellEditor(new PinCellEditor());
		dataTable.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				for (int i = 0; i < tableModel.getRowCount(); i++) {
					if (tableModel.getValueAt(i, 2).toString().length() > 0) {
						if (tableModel.getValueAt(i, 2).toString().length() != 4) {
							tableModel.setValueAt("", i, 2);
						}
					}
					if (tableModel.getValueAt(i, 0).toString().isEmpty()) {
						tableModel.removeRow(i);
					}
					tableModel.setValueAt(tableModel.getValueAt(i, 0).toString().replaceAll(" ", "_"), i, 0);
				}
			}
		});
		addButton.setSize(80, 25);
		addButton.setLocation(5, 125);
		addButton.addActionListener(new
											ActionListener() {
												@Override
												public void actionPerformed(ActionEvent e) {
													String account = JOptionPane.showInputDialog("Enter username");
													for (int i = 0; i < tableModel.getRowCount(); i++) {
														if (tableModel.getValueAt(i, 0).toString().equalsIgnoreCase(account)) {
															return;
														}
													}
													if (account != null && !account.isEmpty()) {
														tableModel.addRow(new Object[]{account.replaceAll(" ", "_"), "", ""});
													}
												}
											});
		removeButton.setSize(80, 25);
		removeButton.setLocation(100, 125);
		removeButton.addActionListener(new
											   ActionListener() {
												   @Override
												   public void actionPerformed(ActionEvent e) {
													   if (dataTable.getSelectedRow() != -1) {
														   manager.accounts.remove(dataTable.getSelectedRow());
														   tableModel.removeRow(dataTable.getSelectedRow());
													   }
												   }
											   });
		breaks.addActionListener(new
										 ActionListener() {
											 @Override
											 public void actionPerformed(ActionEvent e) {
												 setEn();
											 }
										 });
		amount.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				if (!source.getValueIsAdjusting()) {
					int val = source.getValue();
					a = val;
					updateSliders();
				}
			}
		});
		frequency.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				if (!source.getValueIsAdjusting()) {
					int val = source.getValue();
					b = val;
					updateSliders();
				}
			}
		});
		selectButton.setSize(80, 25);
		selectButton.setLocation(195, 125);
		selectButton.addActionListener(new
											   ActionListener() {
												   @Override
												   public void actionPerformed(ActionEvent e) {
													   if (dataTable.getSelectedRow() != -1 && manager.accounts.size() > 0) {
														   ScriptHandler.Secure.current = manager.accounts.get(dataTable.getSelectedRow());
														   ScriptHandler.getScript().isRunning = true;
														   Bot.getScriptHandler().runScript();
														   if (breaks.isSelected()) {
															   BreakHandler.enabled = true;
															   BreakHandler.setBreakFrequency(fre);
															   BreakHandler.setBreakTime(time);
														   }
														   dispose();
													   } else {
                                                           ScriptHandler.getScript().isRunning = true;
														   Bot.getScriptHandler().runScript();
														   dispose();
													   }
												   }
											   });
		add(addButton);
		add(removeButton);
		add(selectButton);
		add(scrollPane);
		pack();
		setLocationRelativeTo(null);
		addWindowListener(new
								  WindowAdapter() {
									  @Override
									  public void windowClosing(WindowEvent e) {
										  for (int i = 0; i < tableModel.getRowCount(); i++) {
											  tableModel.setValueAt(tableModel.getValueAt(i, 0).toString().replaceAll(" ", "_"), i, 0);
											  Account acc;
											  String pin = tableModel.getValueAt(i, 2).toString();
											  if (pin.length() > 0) {
												  acc = new Account(tableModel.getValueAt(i, 0).toString(), tableModel.getValueAt(i, 1).toString(), Integer.parseInt(tableModel.getValueAt(i, 2).toString()));
											  } else {
												  acc = new Account(tableModel.getValueAt(i, 0).toString(), tableModel.getValueAt(i, 1).toString());
											  }
											  manager.add(acc);
										  }
										  manager.save();
										  //TODO -  Selector.populateAccountBox();
									  }
								  });
	}

	private void updateSliders() {
		String d = "30 mins";
		fre = BreakHandler.MINUTE * 30;
		String c = "30 mins";
		time = BreakHandler.MINUTE * 30;
		if (a == 2) {
			c = "1 hour";
			fre = BreakHandler.HOUR;
		}
		if (a == 3) {
			c = "2 hours";
			fre = BreakHandler.HOUR * 2;
		}
		if (a == 4) {
			c = "4 hours";
			fre = BreakHandler.HOUR * 4;
		}
		if (a == 5) {
			c = "8 hours";
			fre = BreakHandler.HOUR * 8;
		}
		if (b == 2) {
			d = "1 hour";
			time = BreakHandler.MINUTE * 60;
		}
		if (b == 3) {
			d = "3 hours";
			time = BreakHandler.HOUR * 3;
		}
		text.setText("Time: " + d + " Frequency: " + c);
		text.invalidate();
	}

	private void setEn() {
		boolean b = breaks.isSelected();
		amount.setEnabled(b);
		frequency.setEnabled(b);
		BreakHandler.enabled = b;
		if (!b) {
			text.setText("Disabled");
			text.invalidate();
		}
	}

	private class PasswordCellEditor extends DefaultCellEditor {

		public PasswordCellEditor() {
			super(new JPasswordField());
		}
	}

	private class PasswordCellRenderer extends DefaultTableCellRenderer {

		@Override
		protected void setValue(final Object value) {
			if (value == null) {
				setText("");
			} else {
				final String str = value.toString();
				setText(str.replaceAll(".", "\\*"));
			}
		}
	}

	private class PinCellEditor extends DefaultCellEditor {

		public PinCellEditor() {
			super(new JPasswordField(new PinDocument(), "", 0));
		}
	}

	public class PinDocument extends PlainDocument {

		private final int limit = 4;

		public PinDocument() {
			super();
		}

		public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
			str = str.replaceAll("[a-zA-Z]", "");
			if (str == null) {
				return;
			}
			super.insertString(offset, str, attr);
			if (getLength() > limit) {
				super.remove(limit, getLength() - limit);
			}
		}
	}

	public static void setAccount() {
		AccountFrame af = new AccountFrame();
		af.setVisible(true);
	}
}
