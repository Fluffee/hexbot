package org.hexbot.gui.explorer;

import org.hexbot.api.methods.Widgets;
import org.hexbot.api.wrapper.Widget;
import org.hexbot.bot.event.EventManager;
import org.hexbot.api.listeners.Paintable;

import javax.swing.*;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventListener;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/5/13
 * Time: 10:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class InterfaceExplorer implements Paintable {
    public InterfaceExplorer() {
        EventManager.addListener((EventListener) this);
        onStart();
    }

    @Override
    public void paint(Graphics g) {
        if (highlightArea != null) {
            g.setColor(Color.RED);
            g.drawRect(highlightArea.x, highlightArea.y, highlightArea.width,
                    highlightArea.height);
        }
    }

    private class InterfaceTreeModel implements TreeModel {
        private final Object root = new Object();
        private final ArrayList<TreeModelListener> treeModelListeners = new ArrayList<TreeModelListener>();

        // only call getAllInterfaces() once per GUI update, because
        // otherwise closed interfaces might mess up the indexes
        private final ArrayList<InterfaceWrap> interfaceWraps = new ArrayList<InterfaceWrap>();

        @Override
        public void addTreeModelListener(final TreeModelListener l) {
            treeModelListeners.add(l);
        }

        private void fireTreeStructureChanged(final Object oldRoot) {
            treeModelListeners.size();
            final TreeModelEvent e = new TreeModelEvent(this,
                    new Object[]{oldRoot});
            for (final TreeModelListener tml : treeModelListeners) {
                tml.treeStructureChanged(e);
            }
        }

        @Override
        public Object getChild(final Object parent, final int index) {
            if (parent == root) {
                return interfaceWraps.get(index);
            } else if (parent instanceof InterfaceWrap) {
                return new InterfaceChildWrap(
                        ((InterfaceWrap) parent).wrapped.getChildren()[index]);
            } else if (parent instanceof InterfaceChildWrap) {
                return new InterfaceChildWrap(
                        ((InterfaceChildWrap) parent).wrapped.getChildren()[index]);
            }
            return null;
        }

        @Override
        public int getChildCount(final Object parent) {
            if (parent == null)
                return 0;
            if (parent == root) {
                return interfaceWraps.size();
            } else if (parent instanceof InterfaceWrap) {
                return ((InterfaceWrap) parent).wrapped.getChildren() != null ? ((InterfaceWrap) parent).wrapped.getChildren().length : 0;
            } else if (parent instanceof InterfaceChildWrap) {
                return ((InterfaceChildWrap) parent).wrapped.getChildren() != null ? ((InterfaceChildWrap) parent).wrapped.getChildren().length : 0;
            }
            return 0;
        }


        @Override
        public Object getRoot() {
            return root;
        }

        @Override
        public boolean isLeaf(final Object o) {
            if (o == null)
                return false;
            return (o instanceof InterfaceChildWrap) && ((InterfaceChildWrap) o).wrapped.isValid() && ((InterfaceChildWrap) o).wrapped.getChildren() != null && ((InterfaceChildWrap) o).wrapped.getChildren().length == 0;
        }

        @Override
        public void removeTreeModelListener(final TreeModelListener l) {
            treeModelListeners.remove(l);
        }

        public boolean searchMatches(final org.hexbot.api.wrapper.Component iface,
                                     final String contains) {
            return iface.getText().toLowerCase()
                    .contains(contains.toLowerCase());
        }

        public void update(final String search) {
            interfaceWraps.clear();

            for (final Widget iface : Widgets.getLoaded()) {
                if (iface == null)
                    continue;
                org.hexbot.api.wrapper.Component[] childz = iface.getChildren();
                if (childz == null)
                    continue;
                toBreak:
                for (final org.hexbot.api.wrapper.Component child : childz) {
                    if (searchMatches(child, search)) {
                        interfaceWraps.add(new InterfaceWrap(iface));
                        break;
                    }
                    org.hexbot.api.wrapper.Component[] chil = child.getChildren();
                    if (chil == null)
                        continue;
                    for (final org.hexbot.api.wrapper.Component component : child.getChildren()) {
                        if (searchMatches(component, search)) {
                            interfaceWraps.add(new InterfaceWrap(iface));
                            break toBreak;
                        }
                    }
                }
            }
            fireTreeStructureChanged(root);
        }

        @Override
        public void valueForPathChanged(final TreePath path,
                                        final Object newValue) {
            // tree represented by this model isn't editable
        }

        @Override
        public int getIndexOfChild(Object parent, Object child) {
            if (parent == root) {
                return interfaceWraps.indexOf(child);
            } else if (parent instanceof InterfaceWrap) {
                return Arrays.asList(
                        ((InterfaceWrap) parent).wrapped.getChildren())
                        .indexOf(((InterfaceChildWrap) child).wrapped);
            } else if (parent instanceof InterfaceChildWrap) {
                return Arrays.asList(
                        ((InterfaceChildWrap) parent).wrapped.getChildren())
                        .indexOf(((InterfaceChildWrap) child).wrapped);
            }
            return -1;
        }
    }

    private class InterfaceChildWrap {
        public org.hexbot.api.wrapper.Component wrapped;

        public InterfaceChildWrap(final org.hexbot.api.wrapper.Component wrapped) {
            this.wrapped = wrapped;
        }

        @Override
        public boolean equals(final Object o) {
            return o instanceof InterfaceChildWrap
                    && wrapped == ((InterfaceChildWrap) o).wrapped;
        }

        @Override
        public String toString() {
            return "Component " + (wrapped.getId());
        }
    }

    // these wrappers just add toString() methods
    private class InterfaceWrap {
        public Widget wrapped;

        public InterfaceWrap(final Widget wrapped) {
            this.wrapped = wrapped;
        }

        @Override
        public boolean equals(final Object o) {
            return o instanceof InterfaceWrap
                    && wrapped == ((InterfaceWrap) o).wrapped;
        }

        @Override
        public String toString() {
            return "IWidget " + wrapped.getId();
        }
    }

    private JFrame window;
    private JTree tree;

    private InterfaceTreeModel treeModel;

    private JPanel infoArea;

    private JTextField searchBox;

    private Rectangle highlightArea = null;


    public boolean onStart() {
        window = new JFrame("IWidget Explorer");

        treeModel = new InterfaceTreeModel();
        treeModel.update("");
        tree = new JTree(treeModel);
        tree.setRootVisible(false);
        tree.setEditable(false);
        tree.getSelectionModel().setSelectionMode(
                TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.addTreeSelectionListener(new TreeSelectionListener() {
            private void addInfo(final String key, final String value) {
                final JPanel row = new JPanel();
                row.setAlignmentX(Component.LEFT_ALIGNMENT);
                row.setLayout(new BoxLayout(row, BoxLayout.X_AXIS));

                for (final String data : new String[]{key, value}) {
                    final JLabel label = new JLabel(data);
                    label.setAlignmentY(Component.TOP_ALIGNMENT);
                    row.add(label);
                }
                infoArea.add(row);
            }

            @Override
            public void valueChanged(final TreeSelectionEvent e) {
                final Object node = tree.getLastSelectedPathComponent();
                if (node == null || node instanceof InterfaceWrap) {
                    return;
                }
                // at this point the node can only be an instace of
                // InterfaceChildWrap
                // or of InterfaceComponentWrap

                infoArea.removeAll();
                org.hexbot.api.wrapper.Component iface = null;
                if (node instanceof InterfaceChildWrap) {
                    highlightArea = ((InterfaceChildWrap) node).wrapped.getBounds();
                    iface = ((InterfaceChildWrap) node).wrapped;
                }
                if (iface == null) {
                    return;
                }
                addInfo("Action type: ", "-1" /* + iface.getActionType() */);
                addInfo("Type: ", "" + -1);
                addInfo("SpecialType: ", "" + -1);
                addInfo("IModel ID: ", "" + iface.getModelId());
                addInfo("Texture ID: ", "" + iface.getTextureId());
                addInfo("Parent ID: ", "" + (iface.getParentId()));
                addInfo("Trade ID: ", "" + (iface.getTradeId()));
                addInfo("Trade Size: ", "" + (iface.getTradeStack()));
                addInfo("Text: ", "" + iface.getText());
                addInfo("BorderThickness: ", "" + iface.getBorderThickness());
                addInfo("Name: ", "" + iface.getComponentName());
                if (iface.getActions() != null) {
                    String actions = "";
                    for (final String action : iface.getActions()) {
                        if (!actions.equals("")) {
                            actions += "\n";
                        }
                        actions += action;
                    }
                    addInfo("Actions: ", actions);
                }
                if (iface.getSlotIds() != null) {
                    String actions = "";
                    for (final int action : iface.getSlotIds()) {
                        actions += action;
                    }
                    addInfo("Slot Id's: ", actions);
                }
                addInfo("Component ID: ", "" + (iface.getId()));
                addInfo("Component Stack Size: ",
                        "" + (iface.getChildren() != null ? iface.getChildren().length : 0));
                addInfo("Parent ", "" + (iface.getParent() == null));
                addInfo("Parent Location ", "" + (iface.getParent() != null ? ("(" + iface.getParent().getX() + ","
                        + iface.getParent().getY() + ")") : "null"));
                addInfo("Absolute Location: ", "(" + iface.getX() + ","
                        + iface.getY() + ")");

                infoArea.validate();
                infoArea.repaint();
            }
        });

        JScrollPane scrollPane = new JScrollPane(tree);
        scrollPane.setPreferredSize(new Dimension(250, 500));
        window.add(scrollPane, BorderLayout.WEST);

        infoArea = new JPanel();
        infoArea.setLayout(new BoxLayout(infoArea, BoxLayout.Y_AXIS));
        scrollPane = new JScrollPane(infoArea);
        scrollPane.setPreferredSize(new Dimension(250, 500));
        window.add(scrollPane, BorderLayout.CENTER);

        final ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                treeModel.update(searchBox.getText());
                infoArea.removeAll();
                infoArea.validate();
                infoArea.repaint();
            }
        };

        final JPanel toolArea = new JPanel();
        toolArea.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolArea.add(new JLabel("Filter:"));

        searchBox = new JTextField(20);
        searchBox.addActionListener(actionListener);
        toolArea.add(searchBox);

        final JButton updateButton = new JButton("Update");
        updateButton.addActionListener(actionListener);
        toolArea.add(updateButton);
        window.add(toolArea, BorderLayout.NORTH);

        window.pack();
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        window.setVisible(true);
                    }
                });
            }
        });
        return true;
    }
}
