/*package org.hexbot.gui.explorer;

import org.hexbot.api.methods.Game;

import javax.swing.*;
import java.util.Vector;

public class Settings extends JFrame {


    public Settings() {
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        jScrollPane1 = new JScrollPane();
        jList1 = new JList();
        jLabel1 = new JLabel();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        jList1.setModel(new AbstractListModel() {
            String[] strings = {"IItem 1", "IItem 2", "IItem 3", "IItem 4", "IItem 5"};

            public int getSize() {
                return strings.length;
            }

            public Object getElementAt(int i) {
                return strings[i];
            }
        });
        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList1);

        jLabel1.setText("Value");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                                .addGap(63, 63, 63)
                                .addComponent(jLabel1)
                                .addGap(0, 123, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(95, 95, 95)
                                .addComponent(jLabel1)
                                .addContainerGap())
        );

        pack();
        Vector<Integer> list = new Vector<>();
        for (int i = 0; i < Game.getSettings().length; i++)
            list.add(i);
        jList1.setListData(list);
    }// </editor-fold>

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {
        jLabel1.setText("Setting " + jList1.getSelectedValue() + ": " + Game.getSettings()[Integer.parseInt(jList1.getSelectedValue().toString())]);
    }



    // Variables declaration - do not modify
    private JLabel jLabel1;
    private JList jList1;
    private JScrollPane jScrollPane1;
    // End of variables declaration
} */