package org.hexbot.gui;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class EventButton extends JButton {

    public EventButton() {
        this(null);
    }

    public EventButton(String s) {
        setText(s);
        addActionListener(
                (new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        onClick();
                    }
                })
        );
    }

    public abstract void onClick();
}
