package org.hexbot.gui;

import org.hexbot.bot.Bot;
import org.hexbot.script.Manifest;
import org.hexbot.script.Script;
import org.hexbot.util.IOHelper;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 12/1/12
 * Time: 11:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScriptPanel extends JPanel {
    boolean local = false;
    // Variables declaration - do not modify
    private JButton jButton1;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JScrollPane jScrollPane1;
    private JTextArea jTextArea1;

    public ScriptPanel(final Script script) {
        local = true;
        initComponents();
        setSize(220, 225);
        Manifest manifest = script.getClass().getAnnotation(Manifest.class);
        jLabel1.setText(manifest.name());
        jLabel1.setForeground(Color.GREEN);
        jLabel3.setText("By: " + manifest.author());
        jLabel3.setForeground(Color.GREEN);
        jTextArea1.setText(manifest.description());
        jButton1.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent arg0) {
                Bot.getScriptHandler().setScript(script);
                Selector.get().dispose();
                //this will run the script after an account is selected.
                AccountFrame.setAccount();
            }
        });
    }

    //id,name,desc,author
    public ScriptPanel(final String[] info) {
        int ty = Integer.parseInt(info[6]);
        Color[] c = new Color[]{Color.WHITE,Color.WHITE,Color.MAGENTA,Color.YELLOW};
        Color color = c[ty];
        initComponents();
        setSize(220, 225);
        String type = info[2].trim();
        jLabel1.setText(info[0]);
        jLabel1.setForeground(Color.WHITE);
        jLabel3.setForeground(Color.WHITE);
        jLabel2.setIcon(new ImageIcon(Config.getSkill(type)));
        jLabel3.setText("By: " + info[3]);
        jTextArea1.setText(info[4]);
        jButton1.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent arg0) {
                Script script = null;
                try {
                    script = (Script) IOHelper.getMainClass(info[1]).newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if (script != null) {
                    Bot.getScriptHandler().setScript(script);
                    Selector.get().dispose();
                    //this will run the script after an account is selected.
                    AccountFrame.setAccount();
                } else {
                    Selector.get().dispose();
                }
            }
        });
    }

    public boolean isLocal() {
        return local;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {
        setBorder(new BevelBorder(0));
        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jButton1 = new JButton();
        jLabel3 = new JLabel();
        jScrollPane1 = new JScrollPane();
        jTextArea1 = new JTextArea();

        jLabel1.setFont(new Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Name");

        jLabel2.setText("");

        jButton1.setText("Start");

        jLabel3.setFont(new Font("Tahoma", 2, 12)); // NOI18N
        jLabel3.setText("Author");
        jTextArea1.setEditable(false);
        jTextArea1.setBackground(new Color(102, 102, 102));
        jTextArea1.setColumns(20);
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportView(jTextArea1);

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel2, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabel3, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(jButton1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jScrollPane1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)))
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel3, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabel2, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

    }// </editor-fold>
    // End of variables declaration
}
