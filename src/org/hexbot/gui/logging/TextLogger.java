package org.hexbot.gui.logging;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;


public class TextLogger {
	public static LogTextArea jTextArea1 = new LogTextArea();

	private static java.util.logging.Logger log = java.util.logging.Logger.getLogger("Hexbot");

    public static java.util.logging.Logger getLogger() {
        return log;
    }

    public static void log(String owner, String s, Color color) {
        setLog(owner);
        log(s, color);
    }

    public static void log(String owner, String s) {
        log(owner, s, Color.WHITE);
    }

    public static void log(String s, Color color) {
        log.log(Level.INFO, s, color);
    }

	public static void log(String s) {
        log(s, Color.WHITE);
	}

	public static void error(String s) {
		log.severe(s);
	}

    public static void setLog(String owner) {
        setLog(java.util.logging.Logger.getLogger(owner));
    }

    public static void setLog(java.util.logging.Logger logger) {
        log = logger;
    }

	public static void clearLog() {
		jTextArea1.clearSelection();
	}

	public static void registerLogging() {
		final Properties logging = new Properties();
		final String logFormatter = LogFormatter.class.getCanonicalName();
		final String fileHandler = FileHandler.class.getCanonicalName();
		logging.setProperty("handlers",
				TextAreaLogHandler.class.getCanonicalName() + "," + fileHandler);
		logging.setProperty(".level", "INFO");
		logging.setProperty(SystemConsoleHandler.class.getCanonicalName()
				+ ".formatter", logFormatter);
		logging.setProperty(fileHandler + ".formatter", logFormatter);
		logging.setProperty(TextAreaLogHandler.class.getCanonicalName()
				+ ".formatter", logFormatter);
		final ByteArrayOutputStream logout = new ByteArrayOutputStream();
		try {
			logging.store(logout, "");
			LogManager.getLogManager().readConfiguration(
					new ByteArrayInputStream(logout.toByteArray()));
		} catch (final Exception ignored) {
		}
	}

}
