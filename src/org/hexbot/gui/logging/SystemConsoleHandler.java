package org.hexbot.gui.logging;

import java.util.logging.ConsoleHandler;


public class SystemConsoleHandler extends ConsoleHandler {
	public SystemConsoleHandler() {
		super();
		setOutputStream(System.out);
	}
}
