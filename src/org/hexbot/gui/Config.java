package org.hexbot.gui;
/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 11/29/12
 * Time: 12:35 AM
 * To change this template use File | Settings | File Templates.
 */


import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.CodeSource;

public class Config {

    public static boolean FROMJAR = false;

    static {
        try {
            CodeSource cs = Config.class.getProtectionDomain().getCodeSource();
            FROMJAR = cs.getLocation().toURI().getPath().endsWith(".jar");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static URL getResourceURL(final String path) throws MalformedURLException {
        return FROMJAR ? Config.class.getResource("/" + path) : new File("src/" + path).toURI().toURL();
    }

    public static Image getImage(final String resource) {
        try {
            return ImageIO.read(getResourceURL(resource));
        } catch (final Exception ignored) {
            ignored.printStackTrace();
        }
        return null;
    }

    public static Image getSkill(String skill) {
        return getImage("org/res/" + skill + ".png");
    }

    public class Path {
        public static final String PLUS = "org/res/add.png";
        public static final String LOGO = "org/res/logo.png";
        public static final String ICON = "org/res/icon.png";
        public static final String PLAY = "org/res/control_play.png";
        public static final String PAUSE = "org/res/pause.png";
        public static final String STOP = "org/res/control_stop.png";
        public static final String MENU = "org/res/gear.png";
        public static final String HOME = "org/res/home.png";
        public static final String INPUT = "org/res/mouse.png";
        public static final String INPUT_OFF = "org/res/close.png";
        public static final String DISPALY = "org/res/logo.png";
        public static final String TWITTER = "org/res/twitter.png";
        public static final String UBUNTU = "org/res/ubuntu.ttf";
        public static final String COLOR = "org/res/color.png";
        public static final String SOURCE = "org/res/source.png";
        public static final String MAP = "org/res/map.jpg";
    }
}
