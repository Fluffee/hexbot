package org.hexbot.gui;

import org.hexbot.gui.logging.TextLogger;
import org.hexbot.script.Manifest;
import org.hexbot.script.Script;
import org.hexbot.util.IOHelper;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.awt.*;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class Selector extends JFrame {

    static Selector selector;
    private static URLClassLoader clazzLoader;

    public Selector() {
        super("Script Selector");
        setIconImage(Config.getImage(Config.Path.ICON));
        selector = this;
        initComponents();
        setSize(730, 490);
        setResizable(false);
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(900, 1200));
        setBackground(Color.GREEN);
        JToolBar jmb = new JToolBar();
        jmb.setFloatable(false);
        jmb.setSize(300, 20);
        jmb.setBackground(Color.BLACK);
        ArrayList<ScriptPanel> scriptPanels = new ArrayList<>();
        //add(jmb);
        int y = 0;
        //load networked

        final Container container = panel;
        getContentPane().setLayout(null);
        panel.setLayout(null);
        int xoff = 0;
        ArrayList<Script> scripts = new ArrayList<>();
        final String scriptsDir = System.getProperty("user.dir") + System.getProperty("file.separator") + "scripts";
        System.out.println("Loading local scripts from: " + scriptsDir);
        loadScripts(scripts, new File(scriptsDir));
        for (Script script : scripts) {
            scriptPanels.add(new ScriptPanel(script));
        }
        final int start = scriptPanels.size();
        //networked scripts
        try {
            String input = IOHelper.getScriptsMessage();
            System.out.println(input);
            JSONArray object = new JSONArray(input);
            for (int i = 0; i < object.length(); i++) {
                JSONObject jsonObject = object.getJSONObject(i);
                String[] info = new String[]{(String) jsonObject.get("name"), ""+jsonObject.get("sid"), (String) jsonObject.get("cname"), (String) jsonObject.get("author"), (String) jsonObject.get("description"),(String) jsonObject.get("tyid"),};
                for (String str : info)
                    System.out.println(str);
                scriptPanels.add(new ScriptPanel(info));
            }
            TextLogger.log("Loaded local scripts from: " + System.getProperty("user.dir") + File.separator + "scripts", Color.GREEN);
            TextLogger.log("Loaded " + (scriptPanels.size() - start) + " networked scripts.", Color.CYAN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int k = scriptPanels.size();
        for (int i = 0; i < (k / 3) + 1; i++) {
            for (int j = 0; j < 3; j++) {
                if (i * 3 + j < k) {
                    ScriptPanel sp = scriptPanels.get(i * 3 + j);
                    sp.setLocation(j * 225, i * 235);
                    container.add(sp);
                }
            }
        }
        JScrollPane jsp = new JScrollPane(panel);
        jsp.setSize(685, 395);
        getContentPane().add(jsp);
        pack();
        setLocationRelativeTo(null);
        SwingUtilities.invokeLater(new
                                           Runnable() {
                                               @Override
                                               public void run() {
                                                   setVisible(true);
                                               }
                                           });
    }

    public static Selector get() {
        if (selector.isVisible()) {
            return selector;
        }
        return (selector = new Selector());
    }

    public void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(700, 400));
    }

    public void loadScripts(ArrayList<Script> scripts, final File loc) {
        final File out = new File(System.getProperty("user.dir") + "/scripts");
        if (!out.exists())
            return;
        for (final File file : loc.listFiles()) {
            if (file.isDirectory()) {
                loadScripts(scripts, file);
            } else if (file.isFile()) {
                try {
                    final String name = file.getName();
                    if (name.endsWith(".class") && !name.contains("$")) {
                        final URL src = out.getCanonicalFile().toURI().toURL();
                        final ClassLoader cl = new URLClassLoader(new URL[]{src});
                        String className = file.getCanonicalPath().substring(out.getCanonicalPath().length() + 1);
                        className = className.replace(".class", "");
                        className = className.replace(File.separatorChar, '.');
                        final Class<?> toCheck = cl.loadClass(className);
                        if (Script.class.isAssignableFrom(toCheck)) {
                            final Class<? extends Script> toLoad = toCheck.asSubclass(Script.class);
                            if (toLoad.isAnnotationPresent(Manifest.class)) {
                                scripts.add(toLoad.newInstance());
                            }
                        }
                    } else if (file.getName().endsWith(".jar")) {
                        clazzLoader = URLClassLoader.newInstance(new URL[]{file.toURI().toURL()});
                        JarFile jar = new JarFile(file);
                        Enumeration en = jar.entries();
                        while (en.hasMoreElements()) {
                            JarEntry entry = (JarEntry) en.nextElement();
                            if (entry.getName().contains(".class")) {
                                try {
                                    Class clazz = clazzLoader.loadClass(entry.getName().replaceAll(".class", "").replaceAll("/", "."));
                                    if (clazz.newInstance() instanceof Script)
                                        scripts.add((Script) clazz.newInstance());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                } catch (final Throwable t) {
                    t.printStackTrace();
                }
            }
        }
    }
}
