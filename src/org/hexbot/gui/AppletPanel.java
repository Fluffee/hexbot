package org.hexbot.gui;

import org.hexbot.bot.rsi.ClientStore;
import org.hexbot.bot.rsi.Crawler;
import org.hexbot.gui.logging.TextLogger;
import org.hexbot.util.Debug;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The panel that contains the applet. Crawling, class identification, as well as
 * applet initialization are all dispatched from within this class.
 */
public final class AppletPanel extends JPanel {

    private static final ExecutorService pool = Executors.newFixedThreadPool(2);
    private final ClientStore store;
    private State state = State.SPLASH;
    private volatile Crawler crawler;
    private volatile Applet applet;
    private volatile String error;
    private Image logo;

    public AppletPanel(final ClientStore store) {
        super(new GridLayout(1, 1));
        this.store = store;
        // this.setBackground(new Color(77, 77, 77));
        logo = Config.getImage(Config.Path.LOGO);
    }

    public static String getLatestTweet() {
        String URL = "https://api.twitter.com/1/statuses/user_timeline.rss?screen_name=powerbuddyORG";
        String tweet = "";
        try {
            final BufferedReader br = new BufferedReader(new InputStreamReader(
                    new java.net.URL(URL).openStream()));
            String input;
            while ((input = br.readLine()) != null) {
                if (input.contains("<description>p")) {
                    tweet = input.substring(input.indexOf('>') + 1, input.lastIndexOf('<'));
                    tweet = tweet.split(":")[1];
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tweet;
    }

    public void reload() {
        //Use the following to debug errors
//        try {
//            crawler = new Crawler();
//            Debug.println("AppletPanel", "Downloading...");
//            store.load(crawler.getGamePackLocation());
//            applet = store.createApplet();
//            AppletPanel.this.applet.setStub(AppletPanel.this.crawler);
//            AppletPanel.this.applet.init();
//            applet.start();
//            applet.setLayout(null);
//            applet.setBounds(0, 0, 765, 503);
//            add(applet);
//            state = State.OSR;
//            Debug.println("AppletPanel", "Load successful.");
//            revalidate();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        synchronized (this) {
            if (state == State.CONFIG || state == State.DOWNLOAD || state == State.IDENTIFY) return;
            state = State.CONFIG;
        }
        if (applet != null) {
            remove(applet);
            applet.destroy();
            System.gc();
            applet = null;
            error = null;
        }
        pool.submit(new Runnable() {
            public void run() {
                try {
                    crawler = new Crawler();
                } catch (IOException e) {
                    e.printStackTrace();
                    error(e.toString());
                    return;
                }
                state = State.DOWNLOAD;
                try {
                    Debug.println("AppletPanel", "Downloading...");
                    store.load(crawler.getGamePackLocation());
                } catch (Exception e) {
                    e.printStackTrace();
                    error(e.toString());
                    return;
                }
                state = State.INITIALIZE;
                try {
                    Debug.println("AppletPanel", "Initializing applet...");
                    applet = store.createApplet();
                } catch (Exception e) {
                    TextLogger.log(e.getMessage());
                    e.printStackTrace();
                }
                try {
                    AppletPanel.this.applet.setStub(AppletPanel.this.crawler);
                    AppletPanel.this.applet.init();
                    applet.start();
                    applet.setLayout(null);
                    applet.setBounds(0, 0, 765, 503);
                    add(applet);
                    state = State.OSR;
                    Debug.println("AppletPanel", "Load successful.");
                    revalidate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        new Thread(new Runnable() {
            public void run() {
                while (state != State.OSR && state != State.ERROR) {
                    repaint();
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException ex) {
                        break;
                    }
                }
                repaint();
            }
        }).start();
    }

    public void paintComponent(Graphics g1) {
        super.paintComponent(g1);
        Graphics2D g = (Graphics2D) g1;
        g.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
        g.setFont(Font.getFont(Font.SERIF));
        //    g.setColor(new Color(77, 77, 77));
        //  g.fillRect(0, 0, getWidth(), getHeight());
        g.drawImage(logo, 230, 165, null);
        switch (state) {
            case CONFIG: {
                String message = "Loading configuration...";
                FontMetrics fm = g.getFontMetrics();
                g.setColor(Color.LIGHT_GRAY);
                g.drawString(message, getWidth() / 2 - fm.stringWidth(message) / 2, 325);
                break;
            }
            case DOWNLOAD: {
                String message = "Downloading:  " + store.current();
                FontMetrics fm = g.getFontMetrics();
                g.setColor(Color.WHITE);
                g.drawString(message, getWidth() / 2 - fm.stringWidth(message) / 2, 325);
                g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
                g.setColor(Color.LIGHT_GRAY);
                g.drawRoundRect(getWidth() / 2 - 100, 340, 200, 15, 2, 2);
                g.setColor(new Color(0, 0 + (int) (store.downloaded() * 255), 255 - (int) (store.downloaded() * 255)));
                g.fillRect(getWidth() / 2 - 99, 341, (int) (store.downloaded() * 199), 7);
                g.setColor(new Color(0, 0 + (int) (store.downloaded() * 235), 235 - (int) (store.downloaded() * 235)));
                g.fillRect(getWidth() / 2 - 99, 348, (int) (store.downloaded() * 199), 7);
                break;
            }
            case IDENTIFY: {
                String message = "Identifying classes...";
                FontMetrics fm = g.getFontMetrics();
                g.setColor(Color.WHITE);
                g.drawString(message, getWidth() / 2 - fm.stringWidth(message) / 2, 325);
                g.setColor(Color.WHITE);
                break;
            }
            case INITIALIZE: {
                String message = "Initializing...";
                FontMetrics fm = g.getFontMetrics();
                g.setColor(Color.LIGHT_GRAY);
                g.drawString(message, getWidth() / 2 - fm.stringWidth(message) / 2, 325);
                break;
            }
            case ERROR: {
                FontMetrics fm = g.getFontMetrics();
                g.drawString(error, getWidth() / 2 - fm.stringWidth(error) / 2, 350);
                break;
            }
        }
    }

    private synchronized void error(String message) {
        state = State.ERROR;
        this.error = message;
    }

    public void setSize(final Dimension dimension) {
        super.setSize(dimension);
        if (applet != null) {
            applet.setSize(dimension);
        }
    }

    public void setPreferredSize(final Dimension dimension) {
        super.setPreferredSize(dimension);
        if (applet != null) {
            applet.setPreferredSize(dimension);
        }
    }

    enum State {
        SPLASH,
        CONFIG,
        DOWNLOAD,
        IDENTIFY,
        INITIALIZE,
        ERROR,
        OSR
    }

}
