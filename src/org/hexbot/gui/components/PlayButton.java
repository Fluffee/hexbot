package org.hexbot.gui.components;


import org.hexbot.bot.Bot;
import org.hexbot.gui.Config;
import org.hexbot.gui.Selector;
import org.hexbot.gui.logging.TextLogger;
import org.hexbot.script.ScriptHandler;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlayButton extends JButton implements ActionListener {
    public static int state = 0;//1 = script running
    public static PlayButton play;

    public PlayButton() {
        super(new ImageIcon(Config.getImage(Config.Path.PLAY)));
        play = this;
        addActionListener(this);
        setFocusable(false);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (state == 0) {
            if (Bot.client == null) {
                JOptionPane.showMessageDialog(null, "Open a bot first", "No Bots...", JOptionPane.OK_OPTION);
            } else {
                new Selector().setVisible(true);
                state = 1;
            }
        } else {
            if (Bot.getScriptHandler().isPaused()) {
                ScriptHandler.resume();
                TextLogger.log("Script resumed");
            } else {
                ScriptHandler.pause();
                TextLogger.log("Script paused");
            }
        }
    }

    public static void setIcon() {
        switch (state) {
            case 0:
                play.setIcon(new ImageIcon(Config.getImage(Config.Path.PLAY)));
                break;
            case 1:
                play.setIcon(new ImageIcon(Config.getImage(Config.Path.PAUSE)));
                break;
        }
        play.invalidate();
    }
}
