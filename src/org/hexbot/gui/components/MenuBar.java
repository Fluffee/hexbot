package org.hexbot.gui.components;


import org.hexbot.gui.Config;
import org.hexbot.gui.MainFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuBar extends JToolBar {
    DebugSelector ds;

    protected static MainFrame frame;

    public MenuBar(MainFrame frame) {
        this.frame = frame;
        setSize(new Dimension(20, 530));
        setMaximumSize(new Dimension(20, 530));
        setFloatable(false);
        setBorder(null);
        setBorderPainted(false);
        setOrientation(SwingConstants.HORIZONTAL);
        HomeButton hb = new HomeButton();
        hb.setFocusable(false);
        add(hb);
        AddButton ab = new AddButton();
        ab.setFocusable(false);
        add(ab);
        add(Box.createHorizontalGlue());
        add(Box.createHorizontalStrut(16));
        ds = new DebugSelector();
        InputButton in = new InputButton();
        in.setFocusable(false);
        PlayButton pb = new PlayButton();
        pb.setFocusable(false);
        StopButton sb = new StopButton();
        sb.setFocusable(false);
        //ColorButton cb = new ColorButton();
        CompileButton cb = new CompileButton();
        cb.setFocusable(false);
        add(pb);
        add(sb);
        add(in);
        add(cb);
        //add(cb);
        final JButton settings = new JButton(new ImageIcon(Config.getImage(Config.Path.MENU)));
        settings.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent arg0) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                ds.setRequestFocusEnabled(false);
                                ds.setFocusable(false);
                                ds.show(settings, -95, 0);
                            }
                        });
                    }
                });
            }
        });
        settings.setFocusable(false);
        add(settings);
        final JButton plus = new JButton(new ImageIcon(Config.getImage(Config.Path.PLUS)));
        plus.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent arg0) {
                try {
                    //load new
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        plus.setFocusable(false);
        // add(plus);
    }
}
