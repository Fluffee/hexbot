package org.hexbot.gui.components;

import org.hexbot.bot.BotCanvas;
import org.hexbot.gui.Config;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/18/13
 * Time: 6:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class InputButton extends JButton implements ActionListener {
    protected static InputButton ib;

    public InputButton() {
        super(new ImageIcon(Config.getImage(Config.Path.INPUT)));
        addActionListener(this);
        setFocusable(false);
        ib = this;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        BotCanvas.input = !BotCanvas.input;
        setIcon();
    }

    public static void setIcon() {
        ib.setIcon((BotCanvas.input ? new ImageIcon(Config.getImage(Config.Path.INPUT)) : new ImageIcon(Config.getImage(Config.Path.INPUT_OFF))));
    }

}