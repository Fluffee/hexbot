package org.hexbot.gui.components;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 6/8/13
 * Time: 12:04 PM
 * To change this template use File | Settings | File Templates.
 */

import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.List;
import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Settings;

import java.awt.TextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class SettingsExplorer extends JFrame implements Paintable {

    public static List listSettings;
    public static TextArea textAreaChanges;
    public static TextArea textAreaInfo;




    public static void display(){
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    SettingsExplorer frame = new SettingsExplorer();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


        private JPanel contentPane;


        public SettingsExplorer() {

            setResizable(false);
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setBounds(100, 100, 562, 437);
            contentPane = new JPanel();
            contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
            setContentPane(contentPane);
            contentPane.setLayout(null);

            listSettings = new List();
            listSettings.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    int i = Integer.parseInt(SettingsExplorer.listSettings.getSelectedItem().toString().split(" : ")[1]);
                    textAreaInfo.setText("Setting : "+Integer.parseInt(SettingsExplorer.listSettings.getSelectedItem().toString().split(" ")[1])+"\n"+
                            "Value : "+i+"\n"+
                            "Binary : "+Integer.toBinaryString(i)+"\n"+
                            "Hex : "+Integer.toHexString(i)
                    );

                }
            });
            listSettings.setBounds(0, 0, 164, 399);
            contentPane.add(listSettings);

            textAreaInfo = new TextArea();
            textAreaInfo.setBounds(170, 269, 380, 130);
            contentPane.add(textAreaInfo);

            textAreaInfo.setText("Setting : "+"null"+"\n"+
                    "Value : "+0+"\n"+
                    "Binary : "+0+"\n"+
                    "Hex : "+0
            );


            textAreaChanges = new TextArea();
            textAreaChanges.setBounds(170, 0, 380, 263);
            contentPane.add(textAreaChanges);
        }
    }
