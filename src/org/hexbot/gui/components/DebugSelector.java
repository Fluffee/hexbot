package org.hexbot.gui.components;


import org.hexbot.bot.Bot;
import org.hexbot.bot.event.EventManager;
import org.hexbot.bot.event.debug.*;
import org.hexbot.gui.explorer.InterfaceExplorer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;

public class DebugSelector extends JPopupMenu implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private static final String IDENTIFIER = "debug";
    private static final String[] OPTIONS = {"No Debug", "Game", "Npc", "Player", "Inventory", "Objects", "GroundItems", "Widgets", "Mouse", "Less CPU", "Settings","Other"};

    public DebugSelector() {
        for (String s : OPTIONS) {
            JMenuItem ji = new JMenuItem(s);
            ji.addActionListener(this);
            add(ji);
        }
    }

    @Override
    public void actionPerformed(final ActionEvent event) {
        Bot.draw = true;
        EventManager.removeDebugListeners();
        if (event.getActionCommand().equals(OPTIONS[1])) {
            EventManager.addListener((EventListener) new GameDebug());
        } else if (event.getActionCommand().equals(OPTIONS[2])) {
            EventManager.addListener((EventListener) new NpcDebug());
        } else if (event.getActionCommand().equals(OPTIONS[3])) {
            EventManager.addListener((EventListener) new PlayerDebug());
        } else if (event.getActionCommand().equals(OPTIONS[4])) {
            EventManager.addListener((EventListener) new InventoryDebug());
        } else if (event.getActionCommand().equals(OPTIONS[5])) {
            EventManager.addListener((EventListener) new ObjectDebug());
        } else if (event.getActionCommand().equals(OPTIONS[6])) {
            EventManager.addListener((EventListener) new GroundDebug());
        } else if (event.getActionCommand().equals(OPTIONS[7])) {
            EventManager.addListener((EventListener) new InterfaceExplorer());
        } else if (event.getActionCommand().equals(OPTIONS[8])) {
            EventManager.addListener((EventListener) new MouseDebug());
        } else if (event.getActionCommand().equals(OPTIONS[9])) {
            Bot.draw = !Bot.draw;
        } else if (event.getActionCommand().equals(OPTIONS[10])) {
            EventManager.addListener((EventListener) new SettingsDebug());
            SettingsExplorer.display();
        }

    }
}
