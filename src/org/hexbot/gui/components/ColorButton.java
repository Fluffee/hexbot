package org.hexbot.gui.components;

import org.hexbot.gui.Config;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 29-4-13
 * Time: 2:43
 * To change this template use File | Settings | File Templates.
 */
public class ColorButton extends JButton implements ActionListener {

    private Color color;

    public ColorButton() {
        super(new ImageIcon(Config.getImage(Config.Path.COLOR)));
        addActionListener(this);
        setFocusable(false);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        color = JColorChooser.showDialog(this, "Customize color", Color.BLUE);
        if(color == null) {
            color = Color.BLACK;
        }

        MenuBar.frame.setForeground(color);
    }
}
