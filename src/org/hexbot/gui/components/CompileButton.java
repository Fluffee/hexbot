package org.hexbot.gui.components;

import org.hexbot.gui.Config;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 4-5-13
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
public class CompileButton extends JButton implements ActionListener {

    final JDialog dialog = new JDialog();
    final JTextArea area = new JTextArea();
    final JButton compile = new JButton();


    @Override
    public void actionPerformed(ActionEvent e) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame map = new JFrame("World Map");
                map.setSize(700, 700);
                JScrollPane pictureScrollPane = new JScrollPane();
                pictureScrollPane.setViewportView(new JLabel(new ImageIcon(Config.getImage(Config.Path.MAP))));
                pictureScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
                pictureScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
                map.add(pictureScrollPane);
                map.setIconImage(Config.getImage(Config.Path.ICON));
                map.setVisible(true);
            }
        });

    }

    public CompileButton() {

        super(new ImageIcon(Config.getImage(Config.Path.SOURCE)));

        try {
            addActionListener(this);
            setFocusable(false);
        } catch (Exception e) {
            //IGNORING CNF
        }


    }

}
