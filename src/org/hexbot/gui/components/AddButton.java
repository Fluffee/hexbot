package org.hexbot.gui.components;

import org.hexbot.gui.AppletPanel;
import org.hexbot.gui.Config;
import org.hexbot.gui.MainFrame;
import org.hexbot.util.Storage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/31/13
 * Time: 6:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddButton extends JButton implements ActionListener {
    public static boolean loaded = false;
    public static AppletPanel panel = null;

    public AddButton() {
        super(new ImageIcon(Config.getImage(Config.Path.PLUS)));
        addActionListener(this);
        setFocusable(false);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (loaded) {
            //new window
        } else {
            AppletPanel appletPanel = new AppletPanel(Storage.clientStore);
            panel = appletPanel;
            appletPanel.reload();
            MainFrame.removeLabel();
            MainFrame.get().getContentPane().add(appletPanel, BorderLayout.CENTER);
            MainFrame.get().setVisible(true);
            loaded = true;
        }
    }
}
