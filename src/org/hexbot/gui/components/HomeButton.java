package org.hexbot.gui.components;

import org.hexbot.gui.Config;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/31/13
 * Time: 6:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class HomeButton extends JButton implements ActionListener {
    public HomeButton() {
        super(new ImageIcon(Config.getImage(Config.Path.HOME)));
        addActionListener(this);
        setFocusable(false);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
        try {
            desktop.browse(new URI("http://www.hexbot.org/scripts/"));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
