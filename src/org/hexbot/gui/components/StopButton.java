package org.hexbot.gui.components;

import org.hexbot.bot.Bot;
import org.hexbot.gui.Config;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StopButton extends JButton implements ActionListener {
    public StopButton() {
        super(new ImageIcon(Config.getImage(Config.Path.STOP)));
        addActionListener(this);
        setFocusable(false);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Bot.getScriptHandler().stop();
        PlayButton.state = 0;

    }
}
