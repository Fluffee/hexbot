package org.hexbot.gui;

import org.hexbot.gui.components.MenuBar;
import org.hexbot.gui.logging.TextAreaLogHandler;
import org.hexbot.gui.logging.TextLogger;
import org.hexbot.util.Configuration;
import org.hexbot.util.IOHelper;
import org.hexbot.util.VersionChecker;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * The Main GUI class for the Boot client. Just using swing for testing
 * purposes, will switch to JavaFX at a later time.
 */
public final class MainFrame extends JFrame {
    protected static MainFrame frame;
    protected static JLabel label;

    public MainFrame() {
        super("Hexbot Local"); // + VersionChecker.getLocalVersion());
        frame = this;
       // setBackground(new Color(77, 77, 77));
        setIconImage(Config.getImage(Config.Path.ICON));
        final Container container = getContentPane();
        container.setLayout(new BorderLayout(4, 2));
        //container.setBackground(new Color(55, 55, 55));
        container.add(new MenuBar(this), BorderLayout.NORTH);
        container.add(label = new JLabel(new ImageIcon(Config.getImage(Config.Path.DISPALY))));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //setLocationRelativeTo(getParent());
        setMinimumSize(Configuration.CLIENT_MINIMUM_SIZE);
        setResizable(false);
        pack();

        TextLogger.jTextArea1.setPreferredSize(new Dimension(770, 80));
        final JScrollPane textScroll = new JScrollPane(
                TextAreaLogHandler.TEXT_AREA, 22, 31);
        textScroll.setBorder(null);
        textScroll.setPreferredSize(new Dimension(770, 100));
        textScroll.setVisible(true);
        TextLogger.registerLogging();
        container.add(textScroll, BorderLayout.SOUTH);
        TextLogger.log("Welcome to Hexbot v" + VersionChecker.getLocalVersion());
//        try {
////            TextLogger.log("Loaded Random Events (Version " + VersionChecker.getRandomsVersion() + ")");
//        } catch (IOException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
        setVisible(true);
    }

    public static void removeLabel() {
        frame.getContentPane().remove(label);
    }

    public static MainFrame get() {
        return frame;
    }

}
