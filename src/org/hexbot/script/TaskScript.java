package org.hexbot.script;

import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 8/17/13
 * Time: 12:31 PM
 * Copyright under GPL liscense by author.
 */
public abstract class TaskScript extends Script {
    protected LinkedList<Task> tasks = new LinkedList<>();

    public void add(Task task) {
        tasks.add(task);
    }

    public void remove(Task task) {
        tasks.remove(task);
    }

    @Override
    public void onStart() {
        Init();
    }

    public abstract void Init();

    @Override
    public int loop() {
        for (Task task : tasks) {
            if (task.validate()) {
                task.run();
                return 5;
            }
        }
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onEnd() {
       onStop();
    }

    protected abstract void onStop();
}
