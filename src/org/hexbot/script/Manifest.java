package org.hexbot.script;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 11/29/12
 * Time: 1:05 AM
 * To change this template use File | Settings | File Templates.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Manifest {

        String name();

        double version() default 1.0;

        String description() default "";

        String author();
}
