package org.hexbot.script;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 8/17/13
 * Time: 12:31 PM
 * Copyright under GPL liscense by author.
 */
public interface Task extends Runnable {
    public boolean validate();

    public void run();
}
