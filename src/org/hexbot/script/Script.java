package org.hexbot.script;


import org.hexbot.api.methods.Game;
import org.hexbot.api.util.Random;
import org.hexbot.bot.BotCanvas;
import org.hexbot.gui.components.InputButton;
import org.hexbot.gui.logging.TextLogger;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 11/29/12
 * Time: 1:06 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Script {

    public void log(Object string) {
        if (string != null)
            TextLogger.log(string.toString());
    }

    public void log(Object string, Color color) {
        TextLogger.log(string.toString(), color);
    }

    public void log(String owner, Object string) {
        TextLogger.log(owner, string.toString());
    }

    public void log(String owner, Object string, Color color) {
        TextLogger.log(owner, string.toString(), color);
    }

    public void clearLog() {
        TextLogger.clearLog();
    }

    public static void setInput(boolean on) {
        BotCanvas.input = on;
        InputButton.setIcon();
    }

    public void logout() {
        Game.logout();
    }

    public boolean isRunning = false;

    public abstract void onStart();

    public abstract int loop();

    public abstract void onEnd();

    public static void sleep(final int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        isRunning = false;
    }

    public static void sleep(final int min, final int max) {
        sleep(Random.nextInt(min, max));
    }


}
