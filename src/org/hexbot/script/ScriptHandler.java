package org.hexbot.script;


import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Players;
import org.hexbot.api.util.Time;
import org.hexbot.bot.Account;
import org.hexbot.bot.event.EventManager;
import org.hexbot.bot.event.RandomManager;
import org.hexbot.gui.components.PlayButton;
import org.hexbot.gui.logging.TextLogger;

import java.awt.*;
import java.util.EventListener;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 11/29/12
 * Time: 1:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class ScriptHandler implements Runnable {
    public static boolean paused = false;
    private static Thread thread;
    private static Script script;
    private static RandomManager manager;


    public ScriptHandler() {
        manager = new RandomManager();
    }

    public static void pause() {
        paused = true;
        PlayButton.setIcon();
    }

    public static void resume() {
        paused = false;
        PlayButton.setIcon();
    }

    public static Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    public static boolean isPaused() {
        return script != null && paused;
    }

    public static boolean isRunning() {
        return script != null && script.isRunning;
    }

    public static void stop() {
        PlayButton.setIcon();
        paused = false;
        if (script == null)
            return;
        thread.interrupt();
        script.stop();
        script.onEnd();
        script = null;
    }

    public void runScript() {
        if (script != null) {
            PlayButton.setIcon();
            thread = new Thread(this);
            thread.start();
            manager.start();
            script.isRunning = true;
        }
    }

    /**
     * Called when a scrip starts.  Must not exit until a script is stopped.
     */
    @Override
    public void run() {
        try {
            //Add the script's listeners if it is valid
            if (script instanceof EventListener)
                EventManager.addListener((EventListener) script);
            //If we are still logging in, we will wait
            while (Players.getLocal() == null || !Game.isLoggedIn())
                Time.sleep(40, 60);
            //set script running and call onStart once
            script.isRunning = true;
            script.onStart();
            //while the script is running, loop and sleep that amount returned.
            while (script.isRunning) {
                //if script is not paused continue, else loop
                if (!paused) {
                    //if randoms are not started, start them
                    if (!manager.running)
                        manager.start();
                    //loop & sleep
                    int sleep = script.loop();
                    Thread.sleep(sleep);
                } else {
                   //some bullshit that seems to work...
                    Time.sleep(1);
                }
            }
        } catch (Exception e) {
            //log and print all errors
            TextLogger.error(e.getMessage());
            e.printStackTrace();
        }
        //remove listeners if needed and clean lists
        if (script instanceof EventListener)
            EventManager.removeListener((EventListener) script);
        stop();
        TextLogger.log("Script", "Script stopped.", Color.RED);
        EventManager.removePaints();
        EventManager.hardDelete();
        System.gc();

    }

    public static class Secure {
        public static Account current;
    }
}

