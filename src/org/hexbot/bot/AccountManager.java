package org.hexbot.bot;

import org.hexbot.gui.logging.TextLogger;
import org.hexbot.util.Crypto;
import java.io.*;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 5/1/13
 * Time: 10:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class AccountManager {
    public static String displayName = "";
    public LinkedList<Account> accounts = new LinkedList<Account>();
    public Account current;

    public AccountManager() {
        load();
    }

    public void setCurrent(final int account) {
        current = accounts.get(account);
    }

    public void add(final Account account) {
        for (final Account acc : accounts) {
            if (acc.hashCode() == account.hashCode()) {
                return;
            }
        }
        accounts.add(account);
    }

    public void save() {
        try {
            final File file = new File(System.getenv("APPDATA") + "/cdp44");
            final BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            for (final Account account : accounts) {
                bw.write(encrypt((account.getUser() + ","
                        + account.getPass()
                        + (account.getPin() != 0 ? ","
                        + account.getPin() : "")).getBytes()));
                bw.newLine();
            }
            bw.flush();
            bw.close();
        } catch (Throwable ignored) {
        }
    }

    public void load() {
        try {
            final File fileold = new File(System.getenv("APPDATA") + "/rsa12");
            if (fileold.exists()) {
                fileold.delete();
                TextLogger.log("Security", "Encryption Upgraded, Re-enter account details.");
            }
            final File file = new File(System.getenv("APPDATA") + "/cdp44");
            final BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            accounts = new LinkedList<Account>();
            while ((line = br.readLine()) != null) {
                line = decrypt(line);
                if (line.isEmpty()) {
                    continue;
                }
                final String[] data = line.split(",");
                Account account;
                if (data.length > 2) {
                    account = new Account(data[0], data[1], Integer.parseInt(data[2]));
                } else {
                    account = new Account(data[0], data[1]);
                }
                accounts.add(account);
            }
            br.close();
        } catch (Throwable ignored) {
        }
    }

    private String encrypt(final byte[] bytes) {
        try {
            return Crypto.encrypt("69barbequesheep85", new String(bytes));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String decrypt(final String str) {
        if (!str.isEmpty()) {
            try {
                return Crypto.decrypt("69barbequesheep85", str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
