package org.hexbot.bot;

import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Players;
import org.hexbot.bot.event.EventManager;
import org.hexbot.bot.event.debug.Debug;
import org.hexbot.gui.logging.TextLogger;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.image.BufferedImage;
import java.util.EventListener;

public class BotCanvas extends Canvas {
    public static boolean input = true;
    public BufferedImage botImage = new BufferedImage(765, 503, BufferedImage.TYPE_INT_RGB);
    public BufferedImage gameImage = new BufferedImage(765, 503, BufferedImage.TYPE_INT_RGB);
    private static final long serialVersionUID = 1L;

    @Override
    public Graphics getGraphics() {
        Graphics g = botImage.getGraphics();
        g.drawImage(gameImage, 0, 0, null);
        g.dispose();
        Graphics2D rend = (Graphics2D) super.getGraphics();
        for (EventListener listener : EventManager.get()) {
            if (listener instanceof Paintable) {
                //prevents spam when not logged in.
                if (listener instanceof Debug && Players.getLocal() == null)
                    continue;
                Paintable paint = (Paintable) listener;
                try {
                    if (paint != null)
                        paint.paint(botImage.getGraphics());
                } catch (Exception e) {
                    e.printStackTrace();
                    TextLogger.error(e.getMessage());
                }
            }
        }
        rend.drawImage(botImage, 0, 0, null);
        return gameImage.getGraphics();
    }

    protected final void processEvent(AWTEvent e) {
        if (!(e instanceof FocusEvent) && (e.toString().matches(".* on canvas\\d+")) && input) {
            super.processEvent(e);
        }
    }
}
