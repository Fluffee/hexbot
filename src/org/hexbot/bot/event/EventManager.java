package org.hexbot.bot.event;


import org.hexbot.api.listeners.Paintable;
import org.hexbot.bot.event.debug.Debug;

import java.util.ArrayList;
import java.util.EventListener;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 7-5-13
 * Time: 14:28
 * To change this template use File | Settings | File Templates.
 */
public class EventManager {

    private static  ArrayList<EventListener> listeners = new ArrayList<>();


    public static void addListener(EventListener listener) {
        listeners.add(listener);
    }

    public static void removeListener(EventListener listener) {
        listeners.remove(listener);
    }

    public static EventListener[] get() {
        return listeners.toArray(new EventListener[listeners.size()]);
    }

    public static void removeDebugListeners() {
        for (EventListener listener : get()) {
            if (listener instanceof Debug) {
                listeners.remove(listener);
            }
        }
    }

    public static void removePaints() {
        for (EventListener listener : get()) {
            if (listener instanceof Paintable) {
                listeners.remove(listener);
            }
        }
    }

    public static void hardDelete() {
        listeners = new ArrayList<>();
    }
}
