package org.hexbot.bot.event;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Time;
import org.hexbot.bot.event.randoms.Login;
import org.hexbot.bot.event.randoms.RandomEvent;
import org.hexbot.script.ScriptHandler;
import org.hexbot.util.VersionChecker;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/18/13
 * Time: 11:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class RandomManager extends Thread implements Paintable {
    static RandomEvent[] randoms;
    public static Thread randomThread;
    String current = null;
    public static boolean running = false;
    Image img = null;

    public RandomManager() {
        EventManager.addListener(this);
        Arrays.sort(randoms);
        randomThread = new Thread(this);
        try {
            img = ImageIO.read(new URL("http://i.imgur.com/IyMoIJN.png"));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static void load() throws IOException {
        ArrayList<RandomEvent> loaded = new ArrayList<>();
        File file = new File(VersionChecker.getHexFolder().getCanonicalPath() + File.separator + "r.jar");
        URLClassLoader clazzLoader = URLClassLoader.newInstance(new URL[]{file.toURI().toURL()});
        JarFile jar = new JarFile(file);
        Enumeration en = jar.entries();
        while (en.hasMoreElements()) {
            JarEntry entry = (JarEntry) en.nextElement();
            if (entry.getName().contains(".class") && !entry.getName().contains("$")) {
                try {
                    Class clazz = clazzLoader.loadClass(entry.getName().replaceAll(".class", "").replaceAll("/", "."));
                    if (clazz.getSuperclass().getSimpleName().contains("RandomEvent") && clazz.newInstance() instanceof RandomEvent) {
                        loaded.add((RandomEvent) clazz.newInstance());
                        // System.out.println("Loaded Random: " + clazz.getSimpleName());
                    }
                } catch (Exception e) {
                    //  e.printStackTrace();
                }
            }
        }
        randoms = loaded.toArray(new RandomEvent[loaded.size()]);
    }


    @Override
    public void paint(Graphics g) {
        if (current != null) {
            g.setColor(new Color(220, 30, 40, 100));
            g.fillRect(0, 0, Mouse.getX() - 2, Mouse.getY() - 2);
            g.fillRect(0, Mouse.getY() + 2, Mouse.getX() - 2, 503 - Mouse.getY());
            g.fillRect(Mouse.getX() + 2, 0, 765 - Mouse.getX(), Mouse.getY() - 2);
            g.fillRect(Mouse.getX() + 2, Mouse.getY() + 2, 765 - Mouse.getX(), 503 - Mouse.getY());
            g.setColor(Color.WHITE);
            g.drawString("Random System : Running!", 20, 50);
            if (img != null)
                g.drawImage(img, 190, 10, null);
        }
    }

    public void start() {
        randomThread = new Thread(this);
        randomThread.start();
    }

    public static boolean canLogin = true;
    Login login = new Login();

    /**
     * Continuously runs while a script is valid and running
     */
    @Override
    public void run() {
        try {
            //loop
            while (ScriptHandler.getScript() != null && ScriptHandler.isRunning()) {
                //establish that we are running
                running = true;
                while (!canLogin)
                    Time.sleep(10);
                //login has ultimate priority
                if (login.validate() && canLogin) {
                    if (!ScriptHandler.isPaused())
                        ScriptHandler.pause();
                    current = "Login Handler by: Swipe";
                    login.loop();
                    ScriptHandler.resume();
                    current = null;
                } else {
                    //else check the others
                    for (RandomEvent re : randoms) {
                        //while loop for efficiency when checking randoms
                        while (re.validate()) {
                            if (re instanceof Paintable)
                                EventManager.addListener((Paintable) re);
                            //obtain random name
                            current = re.getClass().getSimpleName();
                            //pause script if needed
                            if (!ScriptHandler.isPaused())
                                ScriptHandler.pause();
                            //loop the random and sleep
                            Time.sleep(re.loop());
                        }
                        //resume our script if needed
                        if (ScriptHandler.isPaused() && current != null || re == null) {
                            if (re instanceof Paintable)
                                EventManager.removeListener((Paintable) re);
                            re.onEnd();
                            ScriptHandler.resume();
                        }
                        //click continue plug
                        if (Widgets.canClickContinue()) {
                            Widgets.clickContinue();
                        }
                        //we are no longer in a random
                        current = null;
                    }

                    //sleep to save cpu
                    Time.sleep(3000, 4000);
                }
            }
            //end while loop, print exceptions
        } catch (Exception e) {
            e.printStackTrace();
        }
        //we are no way running at this point...
        running = false;
    }

}
