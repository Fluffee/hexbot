package org.hexbot.bot.event.debug;

import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Skills;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 5/21/13
 * Time: 6:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class SkillsDebug implements Paintable {
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.CYAN);
        g.drawString(Skills.CONSTITUTION.getCurrentLevel() + "/" + Skills.CONSTITUTION.getRealLevel(), 544, 45);
        g.drawString(Skills.PRAYER.getCurrentLevel() + "/" + Skills.PRAYER.getRealLevel(), 544, 60);
    }
}
