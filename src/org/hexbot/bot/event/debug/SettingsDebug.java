package org.hexbot.bot.event.debug;

import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Settings;
import org.hexbot.gui.components.SettingsExplorer;
import org.hexbot.gui.logging.TextLogger;

import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 5/29/13
 * Time: 10:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class SettingsDebug implements Paintable, Debug {
    HashMap<Integer, Long> map = new HashMap<>();

    @Override
    public void paint(Graphics arg0) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        if (SettingsExplorer.listSettings != null)
            return;
        if (SettingsExplorer.listSettings.getItemCount() == 0) {
            for (int i = 0; i < Settings.getAll().length; i++) {
                long a = Settings.get(i);
                map.put(i, a);
                SettingsExplorer.listSettings.add("Setting " + i + " : " + map.get(i));
            }


        } else if (SettingsExplorer.listSettings.getItemCount() > 0) {
            for (int i = 0; i < SettingsExplorer.listSettings.getItemCount(); i++) {
                long a = Settings.get(i);
                if (map.get(i) != null && map.get(i) == a) {
                } else if (map.get(i) != null && map.get(i) != a) {
                    SettingsExplorer.textAreaChanges.append(dateFormat.format(date) + "   Setting " + i + " : " + map.get(i) + "  ->  " + Game.getSettings()[i] + "\n");
                    map.remove(i);
                    map.put(i, a);
                    SettingsExplorer.listSettings.remove(i);
                    SettingsExplorer.listSettings.add("Setting " + i + " : " + map.get(i), i);
                }

            }
        }
    }
}

