package org.hexbot.bot.event.debug;

import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Bank;
import org.hexbot.api.methods.Inventory;
import org.hexbot.api.methods.ui.Shop;
import org.hexbot.api.wrapper.InventoryItem;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 11:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class InventoryDebug implements Paintable, Debug {
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.CYAN);
        if (Bank.isOpen()) {
            for (Bank.BankItem bankItem : Bank.getItems()) {
                if (bankItem != null) {
                    g.drawString(bankItem.getId() + "", bankItem.getPoint().x, bankItem.getPoint().y);
                }
            }
        }
        if (Shop.isOpen()) {
            for (Shop.ShopItem shopItem : Shop.getItems()) {
                if (shopItem != null) {
                    g.drawString(shopItem.getId() + "", shopItem.getPoint().x+3, shopItem.getPoint().y+10);
                }
            }
        }
        InventoryItem[] items = Inventory.getAll();
        for (InventoryItem item : items) {
            g.drawString(item.getId() + "", item.getPoint().x, item.getPoint().y);
        }
    }
}
