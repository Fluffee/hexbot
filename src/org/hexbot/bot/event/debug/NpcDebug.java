package org.hexbot.bot.event.debug;

import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Calculations;
import org.hexbot.api.methods.Npcs;
import org.hexbot.api.wrapper.Npc;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 9:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class NpcDebug implements Paintable, Debug {
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.CYAN);
        @SuppressWarnings("unused")
		int i = 0;
        for (Npc npc : Npcs.getLoaded()) {
            if (npc == null)
                continue;
            Point p = npc.getScreenLocation();
            if (Calculations.onGameScreen(p)) {
                g.fillRect(p.x, p.y, 2, 2);
                g.drawString(npc.getName() + " (" + npc.getId() + ") - " + npc.getAnimation(), p.x, p.y);
            }
        }
    }
}
