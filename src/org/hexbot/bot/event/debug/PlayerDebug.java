package org.hexbot.bot.event.debug;

import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Calculations;
import org.hexbot.api.methods.Players;
import org.hexbot.api.wrapper.Player;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 9:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class PlayerDebug implements Paintable ,Debug{
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.CYAN);
        for (Player player : Players.getLoaded()) {
            if (player == null)
                continue;
            Point p = player.getScreenLocation();
            if (Calculations.onGameScreen(p)) {
                g.fillRect(p.x, p.y, 2, 2);
                g.drawString(player.getName() + " (" + player.getAnimation() + ")" , p.x, p.y);
            }
        }
    }

    public void drawRect(Rectangle a, Graphics g) {
        g.drawRect(a.x, a.y, a.width, a.height);
    }
}
