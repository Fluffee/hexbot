package org.hexbot.bot.event.debug;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.listeners.Paintable;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 2-5-13
 * Time: 14:47
 * To change this template use File | Settings | File Templates.
 */
public class MouseDebug implements Paintable, Debug {

    private final ArrayList<String> debugs = new ArrayList<String>();

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.CYAN);
        debugs.add("Mouse: (" + Mouse.getX() + ", " + Mouse.getY() + ")");

        for (int i = 0; i < debugs.size(); i++) {
            g.drawString(debugs.get(i), 10, 40 + (i * 15));
        }

        debugs.clear();
        int x = 10;
        String[] str = org.hexbot.api.methods.Menu.getMenuItems().toArray(new String[]{});
        for (String s : str) {
            g.drawString(s, 400, x);
            x += 10;
        }

    }

}
