package org.hexbot.bot.event.debug;

import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Calculations;
import org.hexbot.api.methods.GroundItems;
import org.hexbot.api.wrapper.GroundItem;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/3/13
 * Time: 5:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class GroundDebug implements Paintable,Debug {
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.CYAN);
        GroundItem[] groundItems = GroundItems.getAll();
        if (groundItems != null) {
            for (GroundItem item : groundItems) {
                if (item != null) {
                    Point p = Calculations.tileToScreen(item.getLocation());
                    if (Calculations.onGameScreen(p)) {
                        g.fillRect(p.x, p.y, 2, 2);
                        g.drawString(item.getId() + " [" + item.getStackSize() + "]", p.x, p.y);
                    }
                }
            }
        }
    }
}
