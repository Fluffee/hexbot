package org.hexbot.bot.event.debug;

import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Calculations;
import org.hexbot.api.methods.GameObjects;
import org.hexbot.api.wrapper.FloorDecoration;
import org.hexbot.api.wrapper.GameObject;
import org.hexbot.api.wrapper.SceneObject;
import org.hexbot.api.wrapper.WallObject;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 11:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectDebug implements Paintable, Debug {
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.CYAN);
        for (GameObject gameObject : GameObjects.getLoaded()) {
            if (gameObject != null) {
                Point osl = gameObject.getScreenLocation();
                if (gameObject instanceof SceneObject) {
                    g.setColor(Color.CYAN);
                }
                if (gameObject instanceof WallObject) {
                    g.setColor(Color.RED);
                }
                if (gameObject instanceof FloorDecoration) {
                    g.setColor(Color.GREEN);
                }
                if(gameObject.getDefinition()!=null){
                    if(gameObject.getName()!=null){
                        if(gameObject.getName().contains("Leve")){
                            g.fillRect(osl.x, osl.y, 6, 6);
                        }
                    }
                }
                    if (Calculations.onGameScreen(osl)) {
                        g.fillRect(osl.x, osl.y, 2, 2);
                        g.drawString(gameObject.getId() + "", osl.x, osl.y);
                    }
                }
           // }
        }
    }
}
