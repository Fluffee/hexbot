package org.hexbot.bot.event.debug;


import org.hexbot.api.input.Mouse;
import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Magic;
import org.hexbot.api.methods.Players;
import org.hexbot.api.wrapper.Tile;
import org.hexbot.bot.Bot;
import org.hexbot.script.ScriptHandler;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/28/13
 * Time: 6:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class GameDebug implements Paintable, Debug {
    ArrayList<String> debugs = new ArrayList<String>();

    public void add() {
        debugs.clear();
        debugs.add("Location: " + Players.getLocal().getX() + ", " + Players.getLocal().getY());
        //TODO fix walking queue: debugs.add("Test: " + Players.getLocal().getQueueX()[0] + ", " + Players.getLocal().getQueueY()[0]);
        debugs.add("Login: State:" + Game.getGameState() + " Index: " + Game.getLoginIndex());
        debugs.add("Plane: " + Game.getPlane());
        debugs.add("BaseX: " + Game.getBaseX());
        debugs.add("BaseY: " + Game.getBaseY());
        debugs.add("CameraX: " + Game.getCameraX());
        debugs.add("CameraY: " + Game.getCameraY());
        debugs.add("CameraZ: " + Game.getCameraZ());
        debugs.add("Camera Pitch: " + Game.getPitch());
        debugs.add("Camera Yaw: " + Game.getYaw());
//        debugs.add("Map Compass: " + Game.getCompassAngle());
//        debugs.add("Map Scale: " + Game.getMapOffset());
//        debugs.add("Map Offset: " + Game.getMapOffset());
        debugs.add("Menu Count: " + Game.getMenuCount());
        debugs.add("Menu X: " + Game.getMenuX());
        debugs.add("Menu Y: " + Game.getMenuY());
        debugs.add("Menu Width: " + Game.getMenuWidth());
        debugs.add("Menu Height: " + Game.getMenuHeight());
        debugs.add("isSpellSelected: " + Magic.isSpellSelected());
        debugs.add("Player Index: " + Bot.client.getMyPlayerIndex());
        debugs.add(toString(Game.getMenuActions()));
        debugs.add(toString(Game.getMenuOptions()));
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.GREEN);
        g.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        add();
        for (int i = 0; i < debugs.size(); i++) {
            g.drawString(debugs.get(i), 10, 80 + (i * 15));
        }
        g.setColor(Color.RED);
        g.drawString("Paused: " + ScriptHandler.paused, 400, 100);
        g.drawLine(Mouse.getX() - 2, Mouse.getY() - 2, Mouse.getX() + 2, Mouse.getY() + 2);
        g.drawLine(Mouse.getX() + 2, Mouse.getY() - 2, Mouse.getX() - 2, Mouse.getY() + 2);
        for (int i = Players.getLocal().getX() - 8; i < Players.getLocal().getX() + 8; i++) {
            for (int j = Players.getLocal().getY() - 8; j < Players.getLocal().getY() + 8; j++) {
                Tile t = new Tile(i, j);
                if (t.getFlag() == 0) {
                    g.setColor(new Color(0, 200, 0, 150));
                } else {
                    g.setColor(new Color(200, 0, 0, 150));
                }
                if (t.isOnScreen()) {
                    t.draw(g);
                    g.fillRect(t.getScreenLocation().x - 1, t.getScreenLocation().y - 1, 2, 2);
                }
            }
        }
    }

    public String toString(String[] str) {
        String temp = "";
        for (String st : str)
            temp += st + ", ";
        return temp;
    }

}
