package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.*;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.GameObject;
import org.hexbot.api.wrapper.GroundItem;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.script.Script;

/*
*@author Bradsta
*/
public class FreakyForester extends RandomEvent {

	private Npc freakyForester = null;
	private Npc targetPheasant = null;
	private boolean submittedKill = false;

	private final int[] pheasantXTriangles = {310, 348, 378, 408, 310, 348, 378, 408};
	private final int rawPheasant = 11704;

	@Override
	public boolean validate() {
		freakyForester = Npcs.getNearest("Freaky Forester");
		return freakyForester != null && freakyForester.getInteracting() != null && freakyForester.getInteracting() == Players.getLocal();
	}

	@Override
	public int loop() {
		while (Npcs.get("Freaky Forester") != null) {
			if (!submittedKill) {
				Script.sleep(notSubmittedLoop());
			} else {
				Script.sleep(submittedLoop());
			}
		}
		return 500;
	}

	@Override
	public RandomEventPriority getPriority() {
		return RandomEventPriority.MEDIUM;
	}

	private int submittedLoop() {
		GameObject portal = GameObjects.getNearest("Exit portal");
		if (portal != null) {
			if (Players.getLocal().getLocation()
					.getDistanceTo(portal.getLocation()) > 3) {
				Walking.traverse(portal.getLocation());
			}
			if (!portal.isOnScreen()) {
				Camera.turnTo(portal);
			} else {
				portal.click();
				Script.sleep(3500);
			}
		}
		return 500;
	}

	private int notSubmittedLoop() {
		GroundItem rawProduct = GroundItems.getNearest(rawPheasant);
		if (rawProduct != null) {
			if (Players.getLocal().getLocation()
					.getDistanceTo(rawProduct.getLocation()) > 3) {
				Walking.traverse(rawProduct.getLocation());
			}
			if (!rawProduct.isOnScreen()) {
				Camera.turnTo(rawProduct);
			} else {
				rawProduct.interact("Take");
				Script.sleep(2500);
			}
		}
		if (Inventory.getCount(rawPheasant) > 0) {
			talkToForester();
			waitForInterface(241, 2);
			if (getInterfaceText(242, 2).contains("you may use the portal")
					|| getInterfaceText(241, 2).contains("you can leave")) {
				submittedKill = true;
			}
		}
		if (targetPheasant != null) {
			targetPheasant.interact("Attack");
			Script.sleep(2500, 3000);
		}
		if (targetPheasant == null && Inventory.getCount(rawPheasant) == 0
				&& Widgets.getChild(242, 2) == null
				&& Widgets.getChild(243, 2) == null) {
			talkToForester();
			waitForInterface(243, 2);
		}
		if (Widgets.getChild(242, 2) != null
				|| Widgets.getChild(243, 2) != null) {
			targetPheasant = getTarget();
		}
		return 500;
	}

	private Npc getTarget() {
		Component INTERFACE_1 = Widgets.getChild(242, 2);
		Component INTERFACE_2 = Widgets.getChild(243, 2);
		String[] tails = {"one-tailed", "two-tailed", "three-tailed",
				"four-tailed", "1-tailed", "2-tailed", "3-tailed", "4-tailed"};
		if (INTERFACE_1 == null && INTERFACE_2 == null) {
			return null;
		}
		for (int i = 0; i < tails.length; i++) {
			if (INTERFACE_1 != null && INTERFACE_1.getText().contains(tails[i])) {
				for (Npc n : Npcs.getLoaded()) {
					if (n.getModel().XTriangles.length == pheasantXTriangles[i]) {
						return n;
					}
				}
			} else if (INTERFACE_2 != null
					&& INTERFACE_2.getText().contains(tails[i])) {
				for (Npc n : Npcs.getLoaded()) {
					if (n.getModel().XTriangles.length == pheasantXTriangles[i]) {
						return n;
					}
				}
			}
		}
		return null;
	}

	private String getInterfaceText(int parent, int child) {
		Component widget = Widgets.getChild(parent, child);
		if (widget != null) {
			return widget.getText();
		}
		return null;
	}

	private void talkToForester() {
		freakyForester = Npcs.getNearest("Freaky Forester");
		if (freakyForester != null) {
			if (Players.getLocal().getLocation()
					.getDistanceTo(freakyForester.getLocation()) > 3) {
				Walking.traverse(freakyForester.getLocation());
			}
			if (!freakyForester.isOnScreen()) {
				Camera.turnTo(freakyForester);
			} else {
				freakyForester.interact("Talk-to");
			}
		}
	}

	private void waitForInterface(int parent, int child) {
		int tries = 0;
		while (Widgets.getChild(parent, child) == null && tries < 10) {
			Time.sleep(300);
			tries++;
		}
	}

}
