package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;
import org.hexbot.gui.logging.TextLogger;
import org.hexbot.script.ScriptHandler;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 8/22/13
 * Time: 10:21 AM
 * Copyright under GPL liscense by author.
 */
public class BankPin extends RandomEvent {

    private String[] bankPin;
    private final int PARENT_ID = 13;

    @Override
    public boolean validate() {
        if (isPinWindowOpen()) {
            String sPin = "" + ScriptHandler.Secure.current.getPin();
            if (sPin.length() < 4 || sPin.equals("null")) {
                TextLogger.log("Bank pin not detected!");
                return false;
            }
            bankPin = parseBankPin(sPin);
            return true;
        }
        return false;
    }

    @Override
    public int loop() {
        int digit = getDigitToTypeIndex();
        if (digit != -1) {
            Component click = getEnterDigit(bankPin[digit]);
            if (click.isValid()) {
                click.click();
                Time.sleep(1000, 1500);
            } else {
                Time.sleep(1000, 1500);
            }
        }
        if (!isPinWindowOpen()) {
            Time.sleep(1000, 1500);
        }
        return 175;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.LOW;
    }


    public String[] parseBankPin(String p) {
        char[] pin = p.toCharArray();
        String[] numPin = new String[4];
        for (int i = 0; i < 4; i++) {   //uses a 4 constant to prevent ONE ArrayOutOfBoundsExceptions
            //this will throw an exception if someone puts in a < 4 number pin
            numPin[i] = String.valueOf(pin[i]);
        }
        return numPin;
    }

    public Component getEnterDigit(String n) {
        for (int i = 110; i < 120; i++) {
            if (Widgets.getChild(PARENT_ID, i).getText().equals(n)) {
                return Widgets.getChild(PARENT_ID, i - 10);
            }
        }
        return null;
    }

    public int getDigitToTypeIndex() {
        Component clickText = Widgets.getChild(PARENT_ID, 151);
        if (clickText != null) {
            if (clickText.getText().equals("First click the FIRST digit."))
                return 0;
            else if (clickText.getText().equals("Now click the SECOND digit."))
                return 1;
            else if (clickText.getText().equals("Time for the THIRD digit."))
                return 2;
            else if (clickText.getText().equals("Finally, the FOURTH digit."))
                return 3;
        }
        return -1;  //shouldn't ever get here
    }

    public boolean isPinWindowOpen() {
        return Widgets.getLoaded()[PARENT_ID] != null && Widgets.getLoaded()[PARENT_ID].getChildren() != null;
    }
}