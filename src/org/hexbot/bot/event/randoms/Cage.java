package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.GameObjects;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.GameObject;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Fryslan
 * Date: 19-8-13
 * Time: 18:34
 */

public class Cage extends RandomEvent {

    private static HashMap<Integer, Integer> WIDGET_SOLUTIONS = new HashMap<Integer, Integer>(){{
        put(9753, 9749);//Ruit
        put(9756, 9752);//Triangle
        put(9755, 9751);//Circkle
        put(9754, 9750);//Square
    }};

    private static int MAIN_WIDGET_ID = 189;
    private static int CAGE_ID = 24974;

    @Override
    public boolean validate() {
        Component[] main_widgets = Widgets.getChildren(MAIN_WIDGET_ID);
        return main_widgets != null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int loop() {
        Component[] main_widgets = Widgets.getChildren(MAIN_WIDGET_ID);
        int wantedModelID = WIDGET_SOLUTIONS.get(main_widgets[1].getModelId());
        if(main_widgets != null && main_widgets.length >= 5){
        for(int i = 0;i < 3;i++){
            if(main_widgets[2 + i].getModelId() == wantedModelID){
                main_widgets[2 + i].click();
                Time.sleep(1000, 2000);
                break;
                }
             }
        } else {
        GameObject cage = GameObjects.getNearest(CAGE_ID);
        if(cage != null){
            if(cage.interact("Unlock")){
                Time.sleep(250, 500);
            }
        }
    }

        return 50;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.MEDIUM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
