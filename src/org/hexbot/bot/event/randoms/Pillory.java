package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.GameObjects;
import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Random;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Area;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.GameObject;
import org.hexbot.api.wrapper.Tile;

/**
 * @author Bradsta & Swipe
 */
public class Pillory extends RandomEvent {

    private final Area VARROCK_CAGES = new Area(new Tile(3225, 3407, 0),
            new Tile(3230, 3407, 0));
    private final Area YANILLE_CAGES = new Area(new Tile(2604, 3105, 0),
            new Tile(2608, 3105, 0));

    private int currentKey = 0;

    private void clickCage() {
        GameObject cage = GameObjects.getNearest("Cage");
        if (cage != null) {
            cage.interact("Unlock");
            Time.sleep(2000);
        }
    }

    private void selectKeyChoice(int keyId) {
        Component[] KEY_INTERFACE = Widgets.getChildren(189);
        if (KEY_INTERFACE == null) {
            return;
        } else {
            for (Component child : KEY_INTERFACE) {
                if (child.getModelId() == currentKey) {
                    child.click();
                    break;
                }
            }
        }
        currentKey = 0;
        Time.sleep(Random.nextInt(3000, 3500));
    }

    private int getKey() {
        Component[] KEY_INTERFACE_MAIN = Widgets.getChildren(189);
        if (KEY_INTERFACE_MAIN[1] != null) {
            switch (KEY_INTERFACE_MAIN[1].getModelId()) {
                case 9753:
                    currentKey = 9749;
                    break;
                case 9754:
                    currentKey = 9750;
                    break;
                case 9755:
                    currentKey = 9751;
                    break;
                case 9756:
                    currentKey = 9752;
                    break;
            }
        }
        return currentKey;
    }

    @Override
    public boolean validate() {
        return (VARROCK_CAGES.contains(Players.getLocal().getLocation())
                || YANILLE_CAGES.contains(Players.getLocal().getLocation()));
    }

    @Override
    public int loop() {
        if (VARROCK_CAGES.contains(Players.getLocal().getLocation())
                || YANILLE_CAGES.contains(Players.getLocal().getLocation())) {
            if (Widgets.getChild(189, 2) == null) {
                clickCage();
            } else if (currentKey == 0) {
                getKey();
            } else if (currentKey != 0) {
                selectKeyChoice(currentKey);
            }
        }
        return 500;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.LOW;  //To change body of implemented methods use File | Settings | File Templates.
    }


}
