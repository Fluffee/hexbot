package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Npcs;
import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.Walking;
import org.hexbot.api.util.Random;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.api.wrapper.Tile;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/3/13
 * Time: 10:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class AttackingNpcs extends RandomEvent {
    private static final String[] BAD_MONSTERS = {"Rock Golem", "Tree Spirit",
            "Shade", "Evil Chicken", "Swarm", "River Troll", "Ent", "Yew", "Oak", "Willow", "Tree", "Maple", "Strange plant"};

    @Override
    public boolean validate() {
        for (String s : BAD_MONSTERS) {
            Npc npc = Npcs.getNearest(s);
            if (npc != null && npc.getInteracting() != null && npc.getInteracting().equals(Players.getLocal()))
            return true;
            else if (npc != null && npc.getText() != null && npc.getText().contains(Players.getLocal().getName())) {
                return true;
            }
        }
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override

    public int loop() {
        if (Players.getLocal() == null) {
            return 1;
        }
        Tile origin = Players.getLocal().getLocation();
        Tile runTile = origin.derive(Random.nextInt(-8, -12), Random.nextInt(-8, -12));
        Walking.traverse(runTile);
        Walking.traverse(origin);
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.HIGH;  //To change body of implemented methods use File | Settings | File Templates.
    }

}
