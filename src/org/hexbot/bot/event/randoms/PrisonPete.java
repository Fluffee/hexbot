package org.hexbot.bot.event.randoms;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.*;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.GameObject;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.api.wrapper.Tile;
import org.hexbot.gui.logging.TextLogger;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 8/26/13
 * Time: 2:31 PM
 * Copyright under GPL liscense by author.
 */
public class PrisonPete extends RandomEvent {
    static final int PARENT = 273;
    static final int CLOSE = 4;
    static final int MODEL_ID = 3;
    //10751 == 2194 fat
    //10750 == 2193 long tail
    //10752 == 2192 horns
    //10749 == 2191 ball tail
    int tokill = -1;


    public boolean validate() {
        return Npcs.get("Prison Pete") != null && Game.getBaseX() == 2040 && Game.getBaseY() == 4416;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int loop() {
        if (validate()) {
            while (Widgets.canClickContinue()) {
                if ((Widgets.getChild(242, 3) != null && Widgets.getChild(242, 3).isOnScreen())) {
                    if (Widgets.getChild(242, 3).getText().contains("You")) {
                        Npc npc = Npcs.getNearest("Prison Pete");
                        if (npc != null) {
                            Walking.walkTileMM(new Tile(npc.getLocation().getX() + 3, npc.getLocation().getY()));
                        }
                    }
                }
                if (Widgets.getChild(242, 2) != null && Widgets.getChild(242, 2).isOnScreen()) {
                    tokill = -1;
                }
                Widgets.clickContinue();
                Time.sleep(500, 1000);
            }
            if (Inventory.contains("Prison key")) {
                tokill = -1;
                Inventory.getItem("Prison key").click();
                Time.sleep(6300, 8400);
            } else {
                if (tokill == -1) {
                    if (Widgets.getChild(PARENT, CLOSE) == null) {
                        GameObject obj = GameObjects.getNearest("Lever");
                        if (obj != null) {
                            Mouse.click(obj.getLocation().getScreenLocation());
                            Camera.setPitch(true);
                            Camera.setCameraRotation(90);
                            obj.click();
                            Time.sleep(1800, 2400);
                        }
                    } else {
                        org.hexbot.api.wrapper.Component id = Widgets.getChild(PARENT, MODEL_ID);
                        if (id != null && id.isOnScreen()) {
                            tokill = getId(id.getModelId());
                            TextLogger.log("Killing animal : " + tokill);
                            Widgets.getChild(PARENT, CLOSE).click();
                        }
                    }
                } else {
                    if (Widgets.getChild(PARENT, CLOSE) != null) {
                        Widgets.getChild(PARENT, CLOSE).click();
                    }
                    Npc npc = Npcs.getNearest(tokill);
                    if (npc != null) {
                        if (npc.isOnScreen()) {
                            npc.getLocation().click();
                            Time.sleep(2400, 3600);
                        } else {
                            Camera.turnTo(npc);
                        }
                    }
                }
            }
        }
        return 10;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getId(int modelId) {
        int[] ids = new int[4];
        int index = 0;
        for (Npc npc : Npcs.getLoaded()) {
            if (npc != null && (npc.getName() == null || npc.getName().equals("null"))) {
                if (!Arrays.asList(ids).contains(npc.getId())) {
                    if (index >= 4)
                        break;
                    ids[index] = npc.getId();
                    index++;
                }
            }
        }
        Arrays.sort(ids);
        TextLogger.log(ids[3] + "," + ids[2] + "," + ids[1] + "," + ids[0]);
        if (modelId == 10751)
            return ids[3];
        if (modelId == 10750)
            return ids[2];
        if (modelId == 10752)
            return ids[1];
        if (modelId == 10749)
            return ids[0];
        return -1;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.MEDIUM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
