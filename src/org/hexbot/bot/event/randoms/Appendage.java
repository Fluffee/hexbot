package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.GameObjects;
import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Filter;
import org.hexbot.api.util.Time;
import org.hexbot.api.util.Timer;
import org.hexbot.api.wrapper.GameObject;
import org.hexbot.api.wrapper.Tile;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 6/11/13
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class Appendage extends RandomEvent {

	private int[] levers = {1};//12724, 12723, 12722, 12725};

	private boolean inArray(int[] array, int id) {
		for (int i : array) {
			if (i == id) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean validate() {
		if (Game.isLoggedIn()) {
			GameObject lever = GameObjects.getNearest(new Filter<GameObject>() {
				@Override
				public boolean accept(GameObject element) {
					return inArray(levers, element.getId());
				}
			});
			if (lever != null && lever.isOnScreen()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int loop() {
		GameObject lever = GameObjects.getNearest(new Filter<GameObject>() {
			@Override
			public boolean accept(GameObject element) {
				return element.getId() == levers[0];
			}
		});
		if (lever != null) {
			Tile t = Players.getLocal().getLocation();
			lever.click();
			Timer timer = new Timer(5000);
			while (timer.isRunning()) {
				if (t.getDistanceTo(Players.getLocal().getLocation()) > 2) {
					return 200;
				}
				if (Widgets.canClickContinue()) {
					Widgets.clickContinue();
				}
				Time.sleep(50);
			}
		}
		return 300;
	}

	@Override
	public RandomEventPriority getPriority() {
		return RandomEventPriority.HIGH; //To change body of implemented methods use File | Settings | File Templates.
	}

}