package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Filter;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 6/11/13
 * Time: 12:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClickToPlay extends RandomEvent {
    @Override
    public boolean validate() {
        for (Component w : Widgets.getValidated(new Filter<Component>() {
            @Override
            public boolean accept(Component component) {
                return component != null;
            }
        })) {
            if (w.getText().equalsIgnoreCase("click here to play")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int loop() {
        if (Widgets.getChild(378, 17) != null) {         //super safe
            if (Widgets.getChild(378, 17).isValid()) {
                for (Component w : Widgets.getValidated(new Filter<Component>() {
                    @Override
                    public boolean accept(Component component) {
                        return component != null;
                    }
                })) {
                    if (w.getText()!=null && w.getText().equalsIgnoreCase("click here to play")) {
                        w.click();
                    }
                }
                Time.sleep(300, 500);
            }
        }
        return 500;
    }


    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.HIGH;
    }
}
