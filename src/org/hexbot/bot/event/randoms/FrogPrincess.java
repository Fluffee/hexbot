package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Npcs;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.api.wrapper.Tile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 5/31/13
 * Time: 12:24 AM
 * To change this template use File | Settings | File Templates.
 */
//Currently solves for when you fail the frog random and have to exit.
public class FrogPrincess extends RandomEvent {
    int princes = 0;

    @Override
    public boolean validate() {
        if (new Tile(2462, 4780).getDistance() < 15) {
            ArrayList<Npc> frogs = new ArrayList<>();
            for (Npc npc : Npcs.getLoaded()) {
                if (npc != null && npc.getName() != null && npc.getName().toLowerCase().contains("frog"))
                    frogs.add(npc);
            }
            if (frogs.size() == 0)
                return false;
            HashMap<Integer, Integer> ids = new HashMap<>();
            for (Npc npc : frogs) {
                if (ids.containsKey(npc.getId())) {
                    ids.put(npc.getId(), ids.get(npc.getId()) + 1);
                } else {
                    ids.put(npc.getId(), 1);
                }
            }
            for (Map.Entry<Integer, Integer> entry : ids.entrySet()) {
                if (entry.getValue() == 1) {
                    princes = entry.getKey();
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int loop() {
        while (Widgets.canClickContinue()) {
            Widgets.clickContinue();
        }
        if (Widgets.getChild(228, 1) != null) {
            Widgets.getChild(228, 1).click();
        } else {
            Npc princess = Npcs.get(princes);
            if (princess != null) {
                princess.click();
                Time.sleep(1000, 2700);
            }
        }
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.LOW;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
