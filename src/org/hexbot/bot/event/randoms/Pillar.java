package org.hexbot.bot.event.randoms;


import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.Camera;
import org.hexbot.api.methods.GameObjects;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.GameObject;
import org.hexbot.api.wrapper.Tile;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/4/13
 * Time: 12:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class Pillar extends RandomEvent {
	private final int[] objs = new int[]{15000, 15002, 15004, 15006, 15008, 15010};
	private final String EXIT = "Exit";

	@Override
	public boolean validate() {
		return GameObjects.getNearest(15003) != null || GameObjects.getNearest(15005) != null;
	}

	private int count = 0;

	@Override
	public int loop() {
		GameObject temp = null;
		for (int i : objs) {
			if ((temp = GameObjects.getNearest(i)) != null) {
				if (temp.isOnScreen()) {
					Mouse.click(temp.getScreenLocation());
					Time.sleep(4000);
				} else {
					Camera.turnTo(temp);
				}
				return 10;
			}
		}
		if (count >= 10) {
			GameObject exit = GameObjects.getNearest(EXIT);
			Tile leave = new Tile(exit.getX(), exit.getY() - 13);
			leave.click();
			Time.sleep(2500);
		}
		return 200;
	}

	@Override
	public RandomEventPriority getPriority() {
		return RandomEventPriority.HIGH;
	}

}
