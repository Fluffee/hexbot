package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Inventory;
import org.hexbot.api.methods.Settings;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Time;
import org.hexbot.api.util.Timer;
import org.hexbot.api.wrapper.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 5/30/13
 * Time: 7:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class StrangeBox extends RandomEvent{

    private static final int STRANGE_BOX_ID = 3062;
    private String lastChoice = "none";
    private static final int SETTINGS_ID = 312;
    private static final int WIDGET_PARENT_ID = 190;

    @Override
    public boolean validate() {
        if(!Tab.isOpen(Tab.INVENTORY)) {
            return false;
        }
        return Game.isLoggedIn() && Inventory.getCount(STRANGE_BOX_ID) > 0;
    }

    @Override
    public int loop() {
        Component boxWidget = Widgets.getChild(WIDGET_PARENT_ID, 10);
        if (boxWidget == null) {
            InventoryItem box = Inventory.getItem(STRANGE_BOX_ID);
            if (box != null) {
                if (box.interact("Open")) {
                    Time.sleep(700);
                    Timer t = new Timer(3000);
                    while (t.isRunning()) {
                        if (Widgets.getChild(192, 10) != null) {
                            break;
                        }
                        Time.sleep(200);
                    }
                }
            }
            return 100;
        }
        int answer = getWidgetIndex();
        String choice = Widgets.getChild(WIDGET_PARENT_ID, 10).getText();
        if (choice.equals(lastChoice)) {
            Timer t = new Timer(3000);
            while (t.isRunning()) {
                if (!Widgets.getChild(WIDGET_PARENT_ID, 10).getText().equals(lastChoice)) {
                    break;
                }
                Time.sleep(200);
            }
            return 200;
        }
        Component option = Widgets.getChild(WIDGET_PARENT_ID, answer);
        if (option != null) {
            option.click();
            Time.sleep(700);
            Timer t = new Timer(3000);
            while (t.isRunning()) {
                if (getWidgetIndex() != answer) {
                    lastChoice = choice;
                    break;
                }
                Time.sleep(100);
            }
        }
        return 200;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.LOW;
    }

    public int getWidgetIndex() {
        switch (Settings.get(SETTINGS_ID) >> 24) {
            case 0:
                return 10;
            case 1:
                return 11;
            case 2:
                return 12;
        }
        return 10;
    }

}
