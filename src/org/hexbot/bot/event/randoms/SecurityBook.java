package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Inventory;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/3/13
 * Time: 11:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class SecurityBook extends RandomEvent {
    @Override
    public boolean validate() {
        return Inventory.contains(9003);
    }

    @Override
    public int loop() {
        Inventory.getItem(9003).interact("drop");
        return 0;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.LOW;
    }

}
