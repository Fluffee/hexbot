package org.hexbot.bot.event.randoms;

import org.hexbot.gui.logging.TextLogger;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/18/13
 * Time: 11:03 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class RandomEvent implements Comparable<RandomEvent> {

    public abstract boolean validate();

    public abstract int loop();

    public abstract RandomEventPriority getPriority();

    @Override
    public int compareTo(RandomEvent other) {
        if (getPriority().priority() > other.getPriority().priority()) {
            return 1;
        } else if (getPriority().priority() < other.getPriority().priority()) {
            return -1;
        }

        return 0;
    }

    public final void onEnd() {
        TextLogger.log("RandomManager", "Random Event completed!", Color.GREEN);
    }

    public static enum RandomEventPriority {
        LOW(1), MEDIUM(2), HIGH(3);

        private int priority;

        RandomEventPriority(int priority) {
            this.priority = priority;
        }

        public int priority() {
            return priority;
        }
    }

}
