package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.*;
import org.hexbot.api.util.Random;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.InventoryItem;
import org.hexbot.api.wrapper.Tab;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 5/30/13
 * Time: 2:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class Lamp extends RandomEvent {

    private static final int LAMP_ID = 2529;
    private static final int CONFIRM_ID = 26;
    private static final int PARENT_ID = 134;
    private static final int SETTING = 261;

    @Override
    public boolean validate() {
        if (!Tab.isOpen(Tab.INVENTORY)) {
            return false;
        }
        return Game.isLoggedIn() && Players.getLocal() != null && Inventory.getCount(LAMP_ID) > 0;
    }

    @Override
    public int loop() {
        if (Widgets.getChild(PARENT_ID, CONFIRM_ID) == null) {
            InventoryItem lamp = Inventory.getItem(LAMP_ID);
            if (lamp != null) {
                if (lamp.interact("Rub")) {
                    return 800;
                }
            }
            return 300;
        }
        int widgetIndex = Choice.getWidgetIndex("Mining");
        Component choice = Widgets.getChild(PARENT_ID, widgetIndex);
        if (choice != null) {
            if (Settings.get(SETTING) == (widgetIndex - 2)) {
                Component confirm = Widgets.getChild(PARENT_ID, CONFIRM_ID);
                if (confirm != null) {
                    confirm.click();
                    return 1000;
                }
                return 500;
            } else {
                choice.click();
                return 1000;
            }
        }
        return 500;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.LOW;
    }


    private static enum Choice {
        ATTACK("Attack", 3), STRENGTH("Strength", 4), RANGED("Ranged", 5), MAGIC("Magic", 6),
        DEFENCE("Defence", 7), HITPOINTS("Constitution", 8), PRAYER("Prayer", 9), AGILITY("Agility", 10),
        HERBLORE("Herblore", 11), THIEVING("Thieving", 12), CRAFTING("Crafting", 13), RUNECRAFT("Runecrafting", 14),
        MINING("Mining", 15), SMITHING("Smithing", 16), FISHING("Fishing", 17), COOKING("Cooking", 18), FIREMAKING("Firemaking", 19),
        WOODCUTTING("Woodcutting", 20), FLETCHING("Fletching", 21), SLAYER("Slayer", 22), FARMING("Farming", 23),
        CONSTRUCTION("Construction", 24), HUNTER("Hunter", 25);
        private String name;
        private int widgetIndex;

        private Choice(String name, int widgetIndex) {
            this.name = name;
            this.widgetIndex = widgetIndex;
        }

        public static int getWidgetIndex(String skill) {
            for (Choice choice : Choice.values()) {
                if (choice.getName().equals(skill)) {
                    return choice.getWidgetIndex();
                }
            }
            return Random.nextInt(12, 23);
        }

        public String getName() {
            return name;
        }

        public int getWidgetIndex() {
            return widgetIndex;
        }
    }
}
