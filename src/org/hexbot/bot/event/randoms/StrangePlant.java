package org.hexbot.bot.event.randoms;

import org.hexbot.api.events.MessageEvent;
import org.hexbot.api.listeners.MessageListener;
import org.hexbot.api.methods.*;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.api.wrapper.Tab;
import org.hexbot.gui.logging.TextLogger;

/**
 * User: Applet
 * Date: 25/08/13
 * Time: 16:12
 */
public class StrangePlant extends RandomEvent implements MessageListener {

	private Npc plant;
	private long lastClicked = -1;
	private boolean isntOurs = false;


	@Override
	public boolean validate() {
		plant = Npcs.getNearest("Strange plant");
		return Game.isLoggedIn() && plant != null && isntOurs == false && (lastClicked == -1 || System.currentTimeMillis() - lastClicked > 12000);//make it wait 1 minute
	}

	@Override
	public int loop() {
		Time.sleep(100, 4000);
		if (!Tab.isOpen(Tab.INVENTORY)) {
			Tab.open(Tab.INVENTORY);
		}
		if (Calculations.distanceTo(plant.getLocation()) <= 5 && plant.getModelHeight() >= 171) {
			if (plant.isOnScreen()) {
				plant.click();
				Time.sleep(2800, 3200);
				lastClicked = System.currentTimeMillis();
				if (Inventory.contains("Strange Fruit")) {
					Inventory.getItem("Strange Fruit").click();
					Time.sleep(1300, 2500);
					TextLogger.log("Strange Plant", "Completed");
				}
			} else {
				Camera.turnTo(plant);
				Walking.walkTileMM(plant);
			}
		}
		return 30;
	}

	@Override
	public RandomEventPriority getPriority() {
		return RandomEventPriority.MEDIUM;
	}

	@Override
	public void onMessage(MessageEvent e) {
		String s = e.getText().toString();
		if (s.toLowerCase().contains("This is not your")) {
			isntOurs = true;
			TextLogger.log("Strange Plant", "Not our plant");
		}
	}
}
