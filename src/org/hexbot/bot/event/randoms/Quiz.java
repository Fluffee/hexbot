package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Npcs;
import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Filter;
import org.hexbot.api.util.Time;
import org.hexbot.api.util.Timer;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.bot.Bot;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 6/11/13
 * Time: 11:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class Quiz extends RandomEvent {

    private final static int[] NPC_IDS = {2538, 2536, 2537};
    private final static int PARENT_ID = 184;
    private final static int MODEL_W_ID = 7;

    @Override
    public boolean validate() {
        if (!Game.isLoggedIn()) {
            return false;
        }
        Npc iles = Npcs.getNearest(NPC_IDS);
        if (iles != null) {
            if (iles.getText() != null) {
                if (iles.getInteractingIndex()-32768 == Bot.client.getMyPlayerIndex()) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int loop() {
        if (Players.getLocal().isMoving() || Players.getLocal().getAnimation() != -1) {
            return 300;
        }
        if (Widgets.getChild(242, 4) != null) {
            Widgets.getChild(242, 4).click();
            Timer t = new Timer(5000);
            while (t.isRunning()) {
                Npc iles = Npcs.getNearest(NPC_IDS);
                if (iles == null) {
                    break;
                }
                Time.sleep(100);
            }
        }
        Component[] valid = Widgets.getValidated(new Filter<Component>() {
            @Override
            public boolean accept(Component element) {
                return element.getParentId() == PARENT_ID;
            }
        });
        if (valid.length == 14) {
            Answer answer = Answer.fromModel(valid[MODEL_W_ID].getModelId());
            if (answer != null) {
                for (Component widget : Widgets.getValidated(new Filter<Component>() {
                    @Override
                    public boolean accept(Component element) {
                        return element.getParentId() == PARENT_ID && element.getId() > 0 && element.getId() < 4;
                    }
                })) {
                    if (widget.getText().contains(answer.getName())) {
                        Component actual = Widgets.getChild(PARENT_ID, widget.getId() + 7);
                        actual.click();
                        Timer t = new Timer(3000);
                        while (t.isRunning()) {
                            if (answer.getModelId() != valid[MODEL_W_ID].getModelId()) {
                                break;
                            }
                            Time.sleep(150);
                        }
                    }
                }
            }
        }
        Npc iles = Npcs.getNearest(NPC_IDS);
        if (iles != null) {
            if (iles.interact("Talk-to")) {
                Timer t = new Timer(1500);
                while (t.isRunning()) {
                    if (Widgets.getValidated(new Filter<Component>() {
                        @Override
                        public boolean accept(Component element) {
                            return element.getParentId() == PARENT_ID;
                        }
                    }).length == 14) {
                        break;
                    }
                    Time.sleep(100);
                }
            }
            return 500;
        }
        return 200;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.MEDIUM;
    }


    private static enum Answer {
        FISH(8829, "fish"), RING(8834, "ring"), SWORD(8836, "sword"),
        SPADE(8837, "spade"), SHIELD(8832, "shield"), AXE(8828, "axe"),
        SHEARS(8835, "shears"), HELMET(8833, "helmet");

        private int modelId;
        private String name;

        private Answer(int modelId, String name) {
            this.modelId = modelId;
            this.name = name;
        }

        private static Answer fromModel(int model) {
            for (Answer answer : values()) {
                if (answer.getModelId() == model) {
                    return answer;
                }
            }
            return null;
        }

        public String getName() {
            return name;
        }

        public int getModelId() {
            return modelId;
        }
    }
}
