package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Npcs;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Filter;
import org.hexbot.api.util.Time;
import org.hexbot.api.util.Timer;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.Npc;

public class Mime extends RandomEvent {
    private static final int WIDGET_PARENT_ID = 188;
    private static final int MIME_ID = 1056;
    private Animation lastAnimation;
    private Animation lastPerformed;


    @Override
    public boolean validate() {
        return Npcs.getNearest("Mime") != null && Npcs.getNearest("Mime").isOnScreen();
    }

    @Override
    public int loop() {
        Npc mime = Npcs.getNearest("Mime");
        if (mime != null) {
            Animation anim = Animation.forAnim(mime.getAnimation());
            if (anim != null && anim != lastAnimation) {
                lastAnimation = anim;
            }
        }

        if (lastPerformed == lastAnimation) {
            return 50;
        }

        Component[] validatedWidgets = Widgets.getValidated(ANIM_FILTER);
        if (validatedWidgets.length == 1) {
            Component widget = validatedWidgets[0];
            if (widget.isValid()) {
                widget.click();
                Timer t = new Timer(1000);
                while (t.isRunning() && widget.isValid()) {
                    Time.sleep(50);
                }
                lastPerformed = lastAnimation;
                return 50;
            }
        }
        return 100;
    }


    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.HIGH;
    }

    private final Filter<Component> ANIM_FILTER = new Filter<Component>() {
        @Override
        public boolean accept(Component w) {
            return w.getParentId() == WIDGET_PARENT_ID
                    && w.getId() == lastAnimation.widget;
        }
    };

    private static enum Animation {

        CRY(860, 2),
        THINK(857, 3),
        LAUGH(861, 4),
        DANCE(866, 5),
        CLIMB_ROPE(1130, 6),
        LEAN(1129, 7),
        GLASS_WALL(1128, 8),
        GLASS_BOX(1131, 9);

        int anim, widget;

        Animation(int anim, int widget) {
            this.anim = anim;
            this.widget = widget;
        }

        static Animation forAnim(int anim) {
            for (Animation animation : values()) {
                if (anim == animation.anim) {
                    return animation;
                }
            }
            return null;
        }
    }
}