package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.*;
import org.hexbot.api.util.Random;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.*;

import java.util.ArrayList;


public class Molly extends RandomEvent {

    private static final int DOOR_CLOSED_ID = 14982;
    private static final int MOLLY_CHATBOX_INTERFACEGROUP = 228;
    private static final int MOLLY_CHATBOX_NOTHANKS = 3;
    private static final int CONTROL_INTERFACEGROUP = 240;
    private static final int CONTROLS_GRAB = 28;
    private static final int CONTROLS_UP = 29;
    private static final int CONTROLS_DOWN = 30;
    private static final int CONTROLS_LEFT = 31;
    private static final int CONTROLS_RIGHT = 32;
    private static final int CONTROLS_EXIT = 33;
    private static final int CLAW_ID = 14976;
    private static final int CONTROL_PANEL_ID = 14978;

    private Npc molly;
    private boolean talked = false;
    private boolean complete = false;

    @Override
    public int loop() {
        final GameObject door = GameObjects.getNearest(DOOR_CLOSED_ID);
        final Player local = Players.getLocal();
        if (door != null) {
            if (local.getX() > door.getX()) {
                if(complete) {
                    if (door.interact("Open")) {
                        return 500;
                    }
                } else {
                    completeRoom();
                    return 500;
                }
            } else {
                if(complete) {
                    final Entity interacting = molly.getInteracting();
                    if(interacting != null && interacting.equals(Players.getLocal())) {
                        if(Widgets.canClickContinue()) {
                            Widgets.clickContinue();
                        }
                    } else {
                        if (molly.interact("Talk")) {
                            return 100;
                        }
                    }
                } else if (talked) {
                    if (door.interact("Open")) {
                        return 500;
                    }
                } else {
                    final Entity interacting = molly.getInteracting();
                    if (interacting != null && interacting.equals(Players.getLocal())) {
                        if (Widgets.canClickContinue()) {
                            Widgets.clickContinue();
                        } else {
                            final Component no = Widgets.getChild(MOLLY_CHATBOX_INTERFACEGROUP, MOLLY_CHATBOX_NOTHANKS);
                            if (no != null && no.isValid()) {
                                no.click();
                                talked = true;
                                return 500;
                            }
                        }
                    } else {
                        if (molly.interact("Talk")) {
                            return 1000;
                        }
                    }
                }
            }
        }
        return 300;
    }

    private void completeRoom() {
        final Widget parent = new Widget(CONTROL_INTERFACEGROUP);
        if (parent.validate()) {
            navigate(parent);
            complete = Npcs.getNearest(molly.getId() - 40) == null;
        } else {
            final GameObject panel = GameObjects.getNearest(CONTROL_PANEL_ID);
            if(panel != null) {
                if(panel.interact("Use")) {
                    Time.sleep(1000, 1500);
                }
            }
        }
    }

    private void navigate(final Widget parent) {
        final GameObject claw = GameObjects.getNearest(CLAW_ID);
        final Npc suspect = Npcs.getNearest(molly.getId() - 40);
        while (claw != null && suspect != null) {
            final Tile clawLoc = claw.getLocation();
            final Tile susLoc = suspect.getLocation();
            final ArrayList<Integer> options = new ArrayList<Integer>();
            if (susLoc.getX() > clawLoc.getX()) {
                options.add(CONTROLS_LEFT);
            } else if (susLoc.getX() < clawLoc.getX()) {
                options.add(CONTROLS_RIGHT);
            }

            if (susLoc.getY() > clawLoc.getY()) {
                options.add(CONTROLS_DOWN);
            } else if (susLoc.getY() < clawLoc.getY()) {
                options.add(CONTROLS_UP);
            }

            if (options.isEmpty()) {
                options.add(CONTROLS_GRAB);
            }

            final Component interact = parent.getChild(options.get(Random.nextInt(0, options.size())));
            interact.click();
            sleep(2000, new Condition() {
                @Override
                public boolean accept() {
                    return claw.getX() != clawLoc.getX() || claw.getY() != clawLoc.getY();
                }
            });
            if(interact.getId() == CONTROLS_GRAB) {
                parent.getChild(CONTROLS_EXIT).click();
            }

        }

    }

    public interface Condition {
        public boolean accept();
    }

    public void sleep(long time, Condition condition) {
        for(int i = 0; i < time / 50 && !condition.accept(); i++) {
            Time.sleep(time / 50);
        }
    }

    @Override
    public boolean validate() {
        if (Game.isLoggedIn()) {
            Npc molly = Npcs.getNearest("Molly");
            if (molly != null) {
                this.molly = molly;
                return true;
            }
        }
        return false;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.HIGH;
    }
}
