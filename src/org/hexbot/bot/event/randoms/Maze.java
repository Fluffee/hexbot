package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.GameObjects;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Filter;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.GameObject;

public class Maze extends RandomEvent {

    @Override
    public boolean validate() {
        return Widgets.getChild(209, 0) != null;
    }

    private static GameObject last = null;

    @Override
    public int loop() {
        GameObject wall = GameObjects.getNearest(new Filter<GameObject>() {
            @Override
            public boolean accept(GameObject gameObject) {
                if (last == null) {
                    return gameObject.getId() == 15964;
                } else {
                    return gameObject.getId() == 15964 && !last.equals(gameObject);
                }
            }
        });
        if (wall != null) {
            wall.interact("open");
            Time.sleep(1200, 1500);
        }
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.LOW;  //To change body of implemented methods use File | Settings | File Templates.
    }
}