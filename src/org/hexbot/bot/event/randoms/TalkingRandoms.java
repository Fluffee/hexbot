package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Camera;
import org.hexbot.api.methods.Npcs;
import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Npc;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 6/11/13
 * Time: 11:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class TalkingRandoms extends RandomEvent {
    public String[] npcs = {"Mysterious Old Man",
            "Drunken Dwarf", "Genie", "Security Guard", "Rick Turpentine",
            "Dr Jekyll", "Cap'n Hand"};
    public Npc random;

    @Override
    public int loop() {
        if (random == null)
            return 100;
        if (!Widgets.canClickContinue()) {
            random.interact("Talk-to");
            Time.sleep(500, 900);
        } else while (Widgets.canClickContinue()) {
            Widgets.clickContinue();
            Time.sleep(500, 900);
        }

        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean validate() {
        random = Npcs.getNearest(npcs);
        if (random != null) {
            if (random.isOnScreen()) {
                if (random.getInteracting() != null && random.getInteracting().equals(Players.getLocal()) || (random.getText() != null && random.getText().contains(Players.getLocal().getName()))) {
                    return true;
                }
            } else {
                Camera.turnTo(random);
            }
        }
        return false;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.MEDIUM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
