package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Npcs;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.Npc;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 6/8/13
 * Time: 12:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class Beehive extends RandomEvent {
    @Override
    public boolean validate() {
        return Game.getBaseX() > 11270 && Game.getBaseY() > 9867 && Npcs.getNearest("Bee Keeper") != null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    boolean hasStarted = false;
    int TOP = 16036;
    int MID1 = 16025;
    int MID2 = 16022;
    int BOTTOM = 16034;

    @Override
    public int loop() {
        if (!hasStarted) {
            Npc keeper = Npcs.getNearest("Bee Keeper");
            if (keeper != null)
                keeper.click();
        }
        if (Widgets.canClickContinue())
            Widgets.clickContinue();
        if (Widgets.getChild(228, 1).isValid()) {
            Widgets.getChild(228, 1).click();
        }
        if (Widgets.getChild(420, 0) != null) {
            //in hive part
            Component top = getModelId(TOP);
            Component mid1 = getModelId(MID1);
            Component mid2 = getModelId(MID2);
            Component bot = getModelId(BOTTOM);
            //Get the all in correct X alignment
            Component[] all = new Component[]{top, mid1, mid2, bot};
            for (Component comp : all) {
                if (comp.getX() < 215) {
                    comp.click();
                    while (comp.getX() < 215)
                        Widgets.getChild(420, 5).click();
                }
            }
            //do each one vertically
            if (top.getY() > 5) {
                top.click();
                while (top.getY() > 5)
                    Widgets.getChild(420, 1).click();
            }
            if (mid1.getY() > 65) {
                mid1.click();
                while (mid1.getY() > 65)
                    Widgets.getChild(420, 1).click();
            }
            if (mid2.getY() > 125) {
                mid2.click();
                while (mid2.getY() > 125)
                    Widgets.getChild(420, 1).click();
            }
            if (bot.getY() > 185) {
                bot.click();
                while (bot.getY() < 185)
                    Widgets.getChild(420, 2).click();
            }
            if (mid1.getY() < 65) {
                mid1.click();
                while (mid1.getY() < 65)
                    Widgets.getChild(420, 2).click();
            }
            if (mid2.getY() < 125) {
                mid2.click();
                while (mid2.getY() < 125)
                    Widgets.getChild(420, 2).click();
            }
            if (bot.getY() < 185) {
                bot.click();
                while (bot.getY() < 185)
                    Widgets.getChild(420, 2).click();
            }
            if (top.getX() < 5) {
                if (mid1.getY() > 65) {
                    if (mid2.getY() > 125) {
                        if (bot.getY() > 185) {
                            Widgets.getChild(420, 3).click();
                        }
                    }
                }
            }
        }

        return 10;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Component getModelId(int model) {
        for (Component comp : Widgets.getChildren(420)) {
            if (comp.getModelId() == model)
                return comp;
        }
        return null;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.LOW;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
