package org.hexbot.bot.event.randoms;


import org.hexbot.api.listeners.Paintable;
import org.hexbot.api.methods.Game;
import org.hexbot.api.util.Timer;
import org.hexbot.bot.Bot;
import org.hexbot.gui.logging.TextLogger;
import org.hexbot.script.ScriptHandler;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 8/24/13
 * Time: 1:44 AM
 * Copyright under GPL liscense by author.
 */
public class BreakHandler extends RandomEvent implements Paintable {
    public static final long SECOND = 1000;
    public static final long MINUTE = 60000;
    public static final long HOUR = 3600000;
    static long breakTime = HOUR; //one hour
    static long frequency = HOUR * 4; //every 4 hours
    static long timeDifferential = MINUTE * 10; //10 minutes
    static long lastBreak = -1;
    static long nextBreak = -1;
    static long nextBreakEnd = -1;
    public static boolean enabled = false;

    public static void setBreakFrequency(long millis) {
        frequency = millis;
    }

    public static void setBreakTime(long millis) {
        breakTime = millis;
    }

    public static void setTimeDifferential(long millis) {
        timeDifferential = millis;
    }

    @Override
    public boolean validate() {
        return (lastBreak == -1 || (System.currentTimeMillis() >= nextBreak)) && enabled && ScriptHandler.getScript().isRunning;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int loop() {
        //why we logged in?  Who did this to us?
        if (nextBreakEnd != -1) {
            if (System.currentTimeMillis() >= nextBreak && System.currentTimeMillis() <= nextBreakEnd && Game.isLoggedIn()) {
                Bot.setLogin(false);
                while (Game.isLoggedIn())
                    Game.logout();
            }
        }
        //We dont have a break set...shit, lets do that nigga!
        if (lastBreak == -1) {
            generateBreak();
            TextLogger.log("Next break in: " + Timer.format(nextBreak - System.currentTimeMillis()));
            Bot.setLogin(true);
            return 100;
        } else if (System.currentTimeMillis() >= nextBreakEnd) {
            //our break ended, time to make a new one!
            generateBreak();
            TextLogger.log("Next break in: " + Timer.format(nextBreak - System.currentTimeMillis()));
            Bot.setLogin(true);
        }
        return 10;  //To change body of implemented methods use File | Settings | File Templates.
    }

    protected final void generateBreak() {
        lastBreak = System.currentTimeMillis();
        nextBreak = System.currentTimeMillis() + (frequency);
        nextBreakEnd = System.currentTimeMillis() + (frequency) + breakTime;
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.LOW;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void paint(Graphics g) {
        if (System.currentTimeMillis() >= nextBreak && System.currentTimeMillis() <= nextBreakEnd) {
            g.drawString("Breaking for: " + Timer.format(nextBreakEnd - System.currentTimeMillis()), 100, 300);
        }
    }
}
