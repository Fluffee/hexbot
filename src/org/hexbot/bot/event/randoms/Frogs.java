package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Npcs;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Npc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 8/23/13
 * Time: 11:26 AM
 * Copyright under GPL liscense by author.
 */
public class Frogs extends RandomEvent {
    int princess = 0;

    @Override
    public boolean validate() {
        ArrayList<Npc> frogs = new ArrayList<>();
        for (Npc npc : Npcs.getLoaded()) {
            if (npc != null && npc.getName() != null && npc.getName().toLowerCase().contains("frog"))
                frogs.add(npc);
        }
        if (frogs.size() == 0)
            return false;
        HashMap<Integer, Integer> ids = new HashMap<>();
        for (Npc npc : frogs) {
            if (ids.containsKey(npc.getId())) {
                ids.put(npc.getId(), ids.get(npc.getId()) + 1);
            } else {
                ids.put(npc.getId(), 1);
            }
        }
        for (Map.Entry<Integer, Integer> entry : ids.entrySet()) {
            if (entry.getValue() == 1) {
                princess = entry.getKey();
                return true;
            }
        }
        return false;
    }

    @Override
    public int loop() {
        if (princess > 0) {
            Npc prin = Npcs.getNearest(princess);
            if (prin != null) {
                prin.click();
				Time.sleep(1000, 3000);
            }
            while (Widgets.canClickContinue())
                Widgets.clickContinue();
        }
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.HIGH;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
