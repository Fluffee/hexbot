package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Npcs;
import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.api.wrapper.Widget;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 5/30/13
 * Time: 2:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class Certer extends RandomEvent {
    private Npc talker;

    private final String[] randomNames = {"Niles", "Miles", "Giles"};


    @Override
    public boolean validate() {
        if (Game.isLoggedIn()) {
            talker = Npcs.getNearest(randomNames);
            if (talker != null && talker.getInteracting() != null && talker.getInteracting().equals(Players.getLocal())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int loop() {
        if (talker != null) {
            if (Widgets.canClickContinue()) {
                Widgets.clickContinue();
                Time.sleep(200);
                return -1;
            }

            final Widget widget = new Widget(184);
            if (widget.validate()) {
                switch (widget.getChild(7).getModelId()) {
                    case 2807:
                        pressInterface("bowl");
                        break;
                    case 8828:
                        pressInterface("axe");
                        break;
                    case 8829:
                        pressInterface("fish");
                        break;
                    case 8832:
                        pressInterface("shield");
                        break;
                    case 8833:
                        pressInterface("helmet");
                        break;
                    case 8834:
                        pressInterface("ring");
                        break;
                    case 8835:
                        pressInterface("shears");
                        break;
                    case 8836:
                        pressInterface("sword");
                        break;
                    case 8837:
                        pressInterface("spade");
                        break;
                }
            }
        }
        return 0;
    }


    public void pressInterface(String contains) {
        if (Widgets.getChild(184, 1).getText().contains(contains)) {
            Widgets.getChild(184, 8).click();
        } else if (Widgets.getChild(184, 2).getText().contains(contains)) {
            Widgets.getChild(184, 9).click();
        } else if (Widgets.getChild(184, 3).getText().contains(contains)) {
            Widgets.getChild(184, 10).click();
        }
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.HIGH;
    }
}
