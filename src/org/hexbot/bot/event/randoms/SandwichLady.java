package org.hexbot.bot.event.randoms;

import org.hexbot.api.methods.Npcs;
import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Random;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.Npc;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/8/13
 * Time: 1:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class SandwichLady extends RandomEvent {

    private final int NPC_ID = 3117;
    private Npc npc;

    private enum Sandwiches {

        SQUARE(10731, "square"),
        ROLL(10727, "roll"),
        CHOCOLATE(10728, "chocolate"),
        BAGUETTE(10726, "baguette"),
        TRIANGLE(10732, "triangle"),
        KEBAB(10729, "kebab"),
        PIE(10730, "pie");
        private final int modelId;
        private final String name;

        Sandwiches(int m, String n) {
            this.modelId = m;
            this.name = n;
        }

        public int getChildId() {
            return modelId;
        }

        public String getChildMessage() {
            return name;
        }

        public static Sandwiches getChildObject(String nm) {
            for (Sandwiches e : Sandwiches.values()) {
                if (nm.equals(e.getChildMessage())) {
                    return e;
                }
            }
            return null;
        }
    }

    @Override
    public boolean validate() {
        Npc npc1 = Npcs.getNearest("Sandwich Lady");
        if (npc1 != null) {
            if (npc1.getInteracting() != null && npc1.getInteracting().equals(Players.getLocal())) {
                npc = npc1;
                return true;
            }
        }
        if (npc1 != null && npc1.getText() != null && npc1.getText().contains(Players.getLocal().getName())) {
            npc = npc1;
            return true;
        }
        return false;
    }

    @Override
    public int loop() {
        if (Widgets.getChild(297, 0) == null && Players.getLocal().getInteracting() == null) {
            npc.interact("Talk-to");
            Time.sleep(1500, 2000);
        } else if (Widgets.getChild(297, 0) == null && Players.getLocal().getInteracting() != null) {
            Widgets.clickContinue();
            Time.sleep(1500, 2000);
        } else {
            String whatSandwich = Widgets.getChild(297, 8).getText();
            Component sandwich = getChildSandwichComponent(Sandwiches.getChildObject(whatSandwich).getChildId());
            if (sandwich != null) {
                sandwich.click();
                Time.sleep(1500, 2000);
            }
        }
        if (Npcs.getNearest("Sandwich Lady") == null) {
            return 50;
        }
        if (Widgets.canClickContinue()) {
            Widgets.clickContinue();
            Time.sleep(1000, 1500);
        }
        return Random.nextInt(150, 200);
    }

    @Override
    public RandomEventPriority getPriority() {
        return RandomEventPriority.MEDIUM;
    }


    public Component getChildSandwichComponent(int mid) {
        Component sandwich;
        for (int i = 1; i < 8; i++) {
            sandwich = Widgets.getChild(297, i);
            if (sandwich != null) {
                if (sandwich.getModelId() == mid) {
                    return sandwich;
                }
            }
        }
        return null;
    }


}
