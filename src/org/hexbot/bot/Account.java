package org.hexbot.bot;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 12/21/12
 * Time: 1:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class Account {
        String user;
        String pass;
        int pin;

        public Account(String user, String pass, int pin) {
                this.user = user;
                this.pass = pass;
                this.pin = pin;
        }

	public Account(final String user, final String pass) {
		this.user = user;
		this.pass = pass;
	}

        public String getUser() {
                return user;
        }

        public String getPass() {
                return pass;
        }

        public int getPin() {
                return pin;
        }

	@Override
	public int hashCode() {
		return user.hashCode() & pass.hashCode();
	}

}
