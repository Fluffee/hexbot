package org.hexbot.bot;

import org.hexbot.bot.event.RandomManager;
import org.hexbot.impl.Client;
import org.hexbot.script.ScriptHandler;

import java.applet.Applet;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/3/13
 * Time: 1:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class Bot {
    public static Client client;
    public static Applet applet;
    public static ScriptHandler scriptHandler;
    public static String forumName = null;
    public static boolean draw = true;
    public static AccountManager accountManager;


    public static ScriptHandler getScriptHandler() {
        if (scriptHandler == null)
            scriptHandler = new ScriptHandler();
        return scriptHandler;
    }

    public static void setLogin(boolean on) {
        RandomManager.canLogin = on;
    }

    public boolean isLoginEnabled() {
        return RandomManager.canLogin;
    }
}
