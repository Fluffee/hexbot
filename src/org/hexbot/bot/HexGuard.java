package org.hexbot.bot;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 5/21/13
 * Time: 9:29 PM
 * To change this template use File | Settings | File Templates.
 */

import org.hexbot.gui.ScriptPanel;
import org.hexbot.gui.Selector;
import org.hexbot.gui.logging.TextLogger;

import java.io.File;
import java.net.InetAddress;
import java.security.Permission;

/**
 * @author Paris
 */
public class HexGuard extends SecurityManager {
    @Override
    public void checkAccess(final Thread t) {
        super.checkAccess(t);
    }

    @Override
    public void checkCreateClassLoader() {
        if (!isCallingClass(javax.swing.UIDefaults.class, Selector.class, ScriptPanel.class, java.io.ObjectOutputStream.class, java.io.ObjectInputStream.class,
                java.lang.reflect.Proxy.class)) {
            TextLogger.log("Creating class loader denied");
            throw new SecurityException();
        }
        super.checkCreateClassLoader();
    }

    @Override
    public void checkDelete(final String file) {
        checkFilePath(file, false);
        super.checkDelete(file);
    }


    @Override
    public void checkListen(final int port) {
        if (port != 0) {
            throw new SecurityException();
        }
        super.checkListen(port);
    }

    public void checkMemberAccess(Class<?> clazz, int which) {
        TextLogger.log("Reflection is now allowed!");
       // throw new SecurityException();
    }

    @Override
    public void checkMulticast(final InetAddress maddr) {
        throw new SecurityException();
    }

    @Override
    public void checkMulticast(final InetAddress maddr, final byte ttl) {
        throw new SecurityException();
    }

    @Override
    public void checkPermission(final Permission perm) {
        if (perm instanceof RuntimePermission) {
            if (perm.getName().equals("setSecurityManager")) {
                throw new SecurityException();
            }
        }
    }

    @Override
    public void checkPermission(final Permission perm, final Object context) {
        checkPermission(perm);
    }

    @Override
    public void checkPrintJobAccess() {
        throw new SecurityException();
    }

    @Override
    public void checkRead(final String file) {
        checkFilePath(file, true);
        super.checkRead(file);
    }

    @Override
    public void checkRead(final String file, final Object context) {
        checkRead(file);
        super.checkRead(file, context);
    }


    @Override
    public void checkSystemClipboardAccess() {
        throw new SecurityException();
    }

    @Override
    public void checkWrite(final String file) {
        checkFilePath(file, false);
        super.checkWrite(file);
    }

    private void checkFilePath(final String pathRaw, final boolean readOnly) {
        final String path = new File(pathRaw).getAbsolutePath();
        final String tmp = System.getProperty("java.io.tmpdir");

        // allow read permissions to all files
        if (readOnly) {
            return;
        }
        // allow write access to temp directory
        if ((path + File.separator).startsWith(tmp)) {
            return;
        }

        TextLogger.log((readOnly ? "Read" : "Write") + " denied: " + path + " on " + Thread.currentThread().getName() + "/" + Thread.currentThread().getThreadGroup().getName());
        throw new SecurityException();
    }

    private boolean isCallingClass(final Class<?>... classes) {
        for (final Class<?> context : getClassContext()) {
            for (final Class<?> clazz : classes) {
                if (clazz.isAssignableFrom(context)) {
                    return true;
                }
            }
        }
        return false;
    }


    public static void assertNonScript() {
        final SecurityManager sm = System.getSecurityManager();
        if (sm == null || !(sm instanceof HexGuard)) {
            throw new SecurityException();
        }
    }
}
