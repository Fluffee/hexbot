package org.hexbot.bot.rsi;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import org.hexbot.api.wrapper.input.Keyboard;
import org.hexbot.api.wrapper.input.Mouse;
import org.hexbot.bot.BotCanvas;
import org.hexbot.util.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * @author Swipe
 */
public class Modifier {


    public static void process(ClassNode node) {
        subCanvas(node);
        subKeyboard(node);
        subMouse(node);
        injectMessage(node);
        injectDefinitions(node);
    }

    private static void injectDefinitions(ClassNode node) {
        if (node.name.equals("client")) {
            Hook hook = Storage.getHook("getItemDef()");
            MethodNode mn = new MethodNode(Opcodes.ACC_PUBLIC, "getItemDefinition", "(I)Lorg/hexbot/impl/IItemDef;", "", null);
            InsnList il = new InsnList();
            il.add(new VarInsnNode(Opcodes.ILOAD, 1));
            il.add(new MethodInsnNode(Opcodes.INVOKESTATIC, hook.owner, hook.field, "(I)L" + Storage.getClass("IItemDef") + ";"));
            il.add(new InsnNode(Opcodes.ARETURN));
            mn.instructions = il;
            node.methods.add(mn);
            return;
        }
    }

    private static void injectMessage(ClassNode node) {
        for (MethodNode mn : (List<MethodNode>) node.methods) {
            if ((mn.desc.equals("(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V") || mn.desc.equals("(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V")) &&
                    java.lang.reflect.Modifier.isFinal(mn.access)) {
              //  Debug.println("message in method: " + node.name + "." + mn.name);
                InsnList list = new InsnList();
                list.add(new VarInsnNode(Opcodes.ILOAD, 0));
                list.add(new VarInsnNode(Opcodes.ALOAD, 1));
                list.add(new VarInsnNode(Opcodes.ALOAD, 2));
                list.add(new VarInsnNode(Opcodes.ALOAD, 3));
                list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "org/hexbot/api/events/MessageEvent", "onMessage", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V"));
                for (AbstractInsnNode ain : mn.instructions.toArray()) {
                    list.add(ain);
                }
                mn.instructions = list;
                return;
            }
        }
        if (node.name.equals("client")) {
            for (MethodNode mn : (List<MethodNode>) node.methods) {
                if (mn.desc.equals("(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V") &&
                        java.lang.reflect.Modifier.isFinal(mn.access)) {
                  //  Debug.println("message in method: " + node.name + "." + mn.name);
                    InsnList list = new InsnList();
                    list.add(new VarInsnNode(Opcodes.ILOAD, 0));
                    list.add(new VarInsnNode(Opcodes.ALOAD, 1));
                    list.add(new VarInsnNode(Opcodes.ALOAD, 2));
                    list.add(new VarInsnNode(Opcodes.ALOAD, 3));
                    list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "org/hexbot/api/events/MessageEvent", "onMessage", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V"));
                    for (AbstractInsnNode ain : mn.instructions.toArray()) {
                        list.add(ain);
                    }
                    mn.instructions = list;
                    return;
                }
            }
        }
    }

    public static void subCanvas(ClassNode node) {
        final String superclass = BotCanvas.class.getCanonicalName().replace('.', '/');
        if (node.superName.equals("java/awt/Canvas")) {
            // Debug.println("Injected: " + superclass + " -> " + node.name);
            subclass(node, superclass, "java/awt/Canvas");
            for (final MethodNode method : (List<MethodNode>) node.methods) {
                if (!method.name.equals("<init>")) {
                    continue;
                }
                for (final AbstractInsnNode insnNode : method.instructions.toArray()) {
                    //change the constructor to ours
                    if (insnNode instanceof MethodInsnNode) {
                        if (((MethodInsnNode) insnNode).owner.contains("Canvas")) {
                            ((MethodInsnNode) insnNode).owner = superclass;
                        }
                    }
                }
            }
        }
        //Check for any other possible subclasses, may not be needed
        subclass(node, superclass, "java/awt/Canvas");
        String field = null;
        String clazz = null;
        for (Implement i : Storage.implementList) {
            if (i.getName().contains("Widget")) {
                clazz = i.getParent();
                break;
            }
        }
        for (Hook hook : Storage.hooks) {
            if (hook.getName().equals("getY") && hook.owner.equals(clazz)) {
                field = hook.field;
                break;
            }
        }
        new Widgets(node, clazz, field).visit();
        new Models(Storage.getClass("IModel"), Storage.getClass("IAnimable"), node).visit();

    }

    public static void subKeyboard(ClassNode node) {
        final String superclass = Keyboard.class.getCanonicalName().replace('.', '/');
        for (String inter : (List<String>) node.interfaces) {
            if (inter.contains("KeyListener")) {
                // Debug.println("Injected: " + superclass + " -> " + node.name);
                node.superName = superclass.replaceAll("\\.", "/");
                for (MethodNode mn : (List<MethodNode>) node.methods) {
                    if (mn.name.contains("key") || mn.name.contains("focus")) {
                        mn.name = "_" + mn.name;
                        //  System.out.println("renamed: " + mn.name);
                    }
                }
                for (final MethodNode method : (List<MethodNode>) node.methods) {
                    if (!method.name.equals("<init>")) {
                        continue;
                    }
                    for (final AbstractInsnNode ain : method.instructions.toArray()) {
                        if (ain.getOpcode() == Opcodes.INVOKESPECIAL) {
                            ((MethodInsnNode) ain).owner = superclass;
                            break;
                        }
                    }
                }
            }
        }
        subclass(node, superclass, "java/awt/event/KeyListener");
    }

    public static void subMouse(ClassNode node) {
        final String superclass = Mouse.class.getCanonicalName().replace('.', '/');
        for (String inter : (List<String>) node.interfaces) {
            if (inter.contains("MouseListener")) {
                node.superName = superclass.replaceAll("\\.", "/");
                // Debug.println("Injected: " + superclass + " -> " + node.name);
                node.superName = superclass;
                for (MethodNode mn : (List<MethodNode>) node.methods) {
                    if (mn.name.contains("mouse")) {
                        mn.name = "_" + mn.name;
                        //System.out.println("renamed: " + mn.name);
                    }
                }
                for (final MethodNode method : (List<MethodNode>) node.methods) {
                    if (!method.name.equals("<init>")) {
                        continue;
                    }
                    for (final AbstractInsnNode ain : method.instructions.toArray()) {
                        if (ain.getOpcode() == Opcodes.INVOKESPECIAL) {
                            ((MethodInsnNode) ain).owner = superclass;
                            break;
                        }
                    }
                }
            }

        }
    }

    /**
     * Replaces the superclass of the provided class node.
     *
     * @param node       The node to swap the super of.
     * @param superclass The new super class
     * @param subclass   The subclass to ensure that the node is valid.
     */
    public static void subclass(ClassNode node, String superclass, String subclass) {
        if (node.superName.equals(subclass)) {
            node.superName = superclass.replaceAll("\\.", "/");
        }
    }

    static class Models {
        ClassNode node;
        private String model;
        private String anim;

        public Models(String modelClass, String animClass, ClassNode classNode) {
            model = modelClass;
            node = classNode;
            anim = animClass;
            if (node.name.equals(animClass)) {
                node.fields.add(new FieldNode(Opcodes.ACC_PUBLIC, "model", "Ljava/lang/Object;", "Ljava/lang/Object;", null));
            }
        }

        public void visit() {
            if (node.name.equals(anim)) {
                for (MethodNode mn : new Vector<MethodNode>(node.methods)) {
                    int store = 10;
                    if (!mn.desc.contains("IIIIIIII)V"))
                        continue;
                    for (AbstractInsnNode ain : mn.instructions.toArray()) {
                        if (ain.getOpcode() == Opcodes.ASTORE) {
                            store = ((VarInsnNode) ain).var;
                        }
                        if (ain.getOpcode() == Opcodes.RETURN) {
                            //Debug.println("models in method: " + mn.name);
                            InsnList list = new InsnList();
                            for (AbstractInsnNode node : mn.instructions.toArray()) {
                                if (node.equals(ain)) {
                                    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
                                    list.add(new VarInsnNode(Opcodes.ALOAD, store));
                                    list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "org/hexbot/api/wrapper/Animable", "updateModel", "(Ljava/lang/Object;Ljava/lang/Object;)V"));
                                    list.add(ain);
                                } else {
                                    list.add(node);
                                }
                            }

                            MethodNode methodNode = new MethodNode(mn.access, mn.name, mn.desc, mn.signature, null);
                            methodNode.instructions = list;
                            node.methods.remove(mn);
                            node.methods.add(methodNode);
                            break;
                        }
                    }
                }
                InsnList set = new InsnList();
                set.add(new VarInsnNode(Opcodes.ALOAD, 0));
                set.add(new VarInsnNode(Opcodes.ALOAD, 1));
                set.add(new FieldInsnNode(Opcodes.PUTFIELD, anim, "model", "Ljava/lang/Object;"));
                set.add(new InsnNode(Opcodes.RETURN));
                MethodNode methodNode1 = new MethodNode(Opcodes.ACC_PUBLIC, "setModel", "(Ljava/lang/Object;)V", null, null);
                methodNode1.instructions = set;
                node.methods.add(methodNode1);
            }
        }
    }

    static class Widgets {
        ClassNode node;
        private String widgetClass;
        private String widgetY;
        private int var;
        private ArrayList<String> names = new ArrayList<>();

        public Widgets(ClassNode cn, String widgetClass, String boundsIndex) {
            this.widgetClass = widgetClass;
            this.widgetY = boundsIndex;
            node = cn;
            if (node.name.equals(widgetClass)) {
                node.fields.add(new FieldNode(Opcodes.ACC_PUBLIC, "masterX", "I", "I", -1));
                node.fields.add(new FieldNode(Opcodes.ACC_PUBLIC, "masterY", "I", "I", -1));
            }
        }

        public void visit() {
            if (node.name.equals("client"))
                return;
            for (MethodNode mn : new Vector<MethodNode>(node.methods)) {
                if (!mn.desc.equals("([L" + widgetClass + ";IIIIIII)V") && !mn.desc.equals("([L" + widgetClass + ";IIIIIIIB)V") && !mn.desc.equals("([L" + widgetClass + ";IIIIIIII)V"))
                    continue;
                for (AbstractInsnNode ain : mn.instructions.toArray()) {
                    if (ain instanceof FieldInsnNode) {
                        FieldInsnNode fin = (FieldInsnNode) ain;
                        AbstractInsnNode last = ASMUtil.getNext(fin, Opcodes.ISTORE);
                        if (last == null) {
                            break;
                        } else if (fin.owner.equals(widgetClass) && fin.name.equals(widgetY) &&
                                fin.getOpcode() == Opcodes.GETFIELD) {
                         //   Debug.println("master in method: " + node.name + "." + mn.name);
                            VarInsnNode t = null;
                            VarInsnNode t1 = null;
                            t = (VarInsnNode) ASMUtil.getPrevious(fin, Opcodes.ALOAD);
                            t1 = (VarInsnNode) ASMUtil.getNext(fin, Opcodes.ISTORE);
                            if (t == null || t1 == null || names.contains(mn.name))
                                continue;
                            names.add(mn.name);
                            int load = t.var;
                            int var = t1.var;
                            System.out.println(load + "." + var);
                            InsnList list = new InsnList();
                            for (AbstractInsnNode node : mn.instructions.toArray()) {
                                if (node.equals(last)) {
                                    list.add(last);
                                    list.add(new VarInsnNode(Opcodes.ALOAD, load));
                                    list.add(new VarInsnNode(Opcodes.ILOAD, var-1));
                                    list.add(new FieldInsnNode(Opcodes.PUTFIELD, widgetClass, "masterX", "I"));
                                    list.add(new VarInsnNode(Opcodes.ALOAD, load));
                                    list.add(new VarInsnNode(Opcodes.ILOAD, var ));
                                    list.add(new FieldInsnNode(Opcodes.PUTFIELD, widgetClass, "masterY", "I"));
                                    //list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "org/hexbot/api/methods/Widgets", "setPosition", "(Ljava/lang/Object;II)V"));
                                } else {
                                    list.add(node);
                                }
                            }
                            MethodNode methodNode = new MethodNode(mn.access, mn.name, mn.desc, mn.signature, null);
                            methodNode.instructions = list;
                            node.methods.remove(mn);
                            node.methods.add(methodNode);
                            break;
                        }
                    }
                }
            }
        }
    }
}
