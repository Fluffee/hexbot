package org.hexbot.bot.rsi;

import com.google.common.io.ByteStreams;
import com.google.common.io.CountingInputStream;
import org.hexbot.bot.Bot;
import org.hexbot.impl.Client;
import org.hexbot.util.Hook;
import org.hexbot.util.Implement;
import org.hexbot.util.Processor;
import org.hexbot.util.Storage;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

import java.applet.Applet;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

public final class ClientStore {

    private double downloaded;
    public static CustomClassLoader loader;
    private String currentClass;

    public static Class get(String s) {
        try {
            Class c = loader.loadClass(s);
            if (c == null) {
                return Bot.applet.getClass().getClassLoader().loadClass(s);
            }
            return c;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    public static FieldNode get(String node, String field) {
        ClassNode cn = loader.data.get(node + ".class");
        for (FieldNode fn : (List<FieldNode>) cn.fields) {
            if (fn.name.equals(field))
                return fn;
        }
        return null;
    }

    public void load(final URL url) throws IOException {
        final HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        uc.setRequestProperty("User-Agent", "hexbot");
        uc.setConnectTimeout(30000);
        uc.setReadTimeout(30000);
        downloaded = 0D;
        loader = new CustomClassLoader();
        final int size = uc.getContentLength();
        final CountingInputStream cin = new CountingInputStream(uc.getInputStream());
        final JarInputStream is = new JarInputStream(cin) {
            //We want to do something custom when downloading :P
            public int read(final byte[] b, final int off, final int len) throws IOException {
                downloaded = (double) cin.getCount() / (double) size;
                return super.read(b, off, len);
            }
        };
        JarEntry entry;

        while ((entry = is.getNextJarEntry()) != null) {
            currentClass = entry.getName();
            byte[] data = ByteStreams.toByteArray(is);
            if (currentClass.endsWith(".class")) {
                ClassReader reader = new ClassReader(data);
                ClassNode node = new ClassNode();
                reader.accept(node, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
                loader.data.put(currentClass, node);
            }
            loader.classes.put(currentClass, data);
        }
        process();
        // dumpJar("yeye");
    }

    public void process() {
        for (ClassNode node : loader.data.values()) {
            Modifier.process(node);
        }
        int k = 0;
        for (ClassNode node : loader.data.values()) {
            for (Hook hook : Storage.hooks) {
                if (hook.toInject.equals(node.name) && !hook.type.contains("method")) {
                    if (hook.multiplier == -1) {
                        Processor.addFieldGetter(node, loader.data.get(hook.owner + ".class"), get(hook.owner, hook.field), hook.name, hook.type, hook.isStatic);
                    } else {
                        Processor.addMultiplyGetter(node, loader.data.get(hook.owner + ".class"), get(hook.owner, hook.field), hook.name, hook.type, hook.multiplier, hook.isStatic);
                    }
                }
            }
            for (Implement im : Storage.implementList) {
                if (im.getParent().equals(node.name)) {
                    Processor.addInterfacesTo(node, im.getName());
                }
            }
        }
        loader.flush();
    }

    public String getClass(String name) {
        for (Implement impl : Storage.implementList) {
            if (impl.getName().equals("org.hexbot.impl." + name))
                return impl.getParent();
        }
        return null;
    }

    public void dumpJar(String name) {
        try {
            File file = new File("./" + name + ".jar");
            FileOutputStream stream = new FileOutputStream(file);
            JarOutputStream out = new JarOutputStream(stream);
            for (ClassNode cg : loader.data.values()) {
                JarEntry je = new JarEntry(cg.name.replace('.', '/') + ".class");
                out.putNextEntry(je);
                ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
                cg.accept(writer);
                out.write(writer.toByteArray());
            }
            out.close();
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Applet createApplet() {
        try {
            if (Bot.applet != null) throw new InstantiationException("Applet already exists!");
            Object obj = loader.findClass("client").newInstance();
            Bot.client = (Client) obj;
            Bot.applet = (Applet) obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Bot.applet;
    }

    public String current() {
        return currentClass;
    }

    public double downloaded() {
        return downloaded;
    }

}
