package org.hexbot.bot.rsi;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public final class CustomClassLoader extends ClassLoader {

    //Package access
    final Map<String, ClassNode> data = new HashMap<>();
    final Map<String, byte[]> classes = new HashMap<>();

    protected Class<?> findClass(String name) {
        try {
            byte[] d = classes.get(name + ".class");
            if (d != null)
                return defineClass(name, d, 0, d.length);
            return super.findClass(name);
        } catch (Exception e) {
          //  e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    public InputStream getResourceAsStream(final String name) {
        return new ByteArrayInputStream(classes.get(name));
    }

    public void flush() {
        for (Map.Entry<String, ClassNode> entry : data.entrySet()) {
            ClassNode node = entry.getValue();
            ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
            node.accept(writer);
            classes.put(entry.getKey(), writer.toByteArray());
        }
    }

}
