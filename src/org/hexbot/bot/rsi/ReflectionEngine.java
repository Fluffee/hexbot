package org.hexbot.bot.rsi;

import org.hexbot.util.Hook;
import org.hexbot.util.Storage;

import java.lang.reflect.Field;

public final class ReflectionEngine {

    public static Field getField(final String clazz, String fieldName) {
        try {
            final Field f = ClientStore.get(clazz).getDeclaredField(fieldName);
            if (!f.isAccessible())
                f.setAccessible(true);
            return f;
        } catch (final NoSuchFieldException e) {
            throw new IllegalArgumentException("No such field: " + fieldName + " in " + clazz);
        }
    }


    public static Object getObjectField(Object instance, String clazz, String fieldName) {
        try {
            Hook hook = Storage.getHook(clazz, fieldName);
            if (hook.getMultiplier() == -1)
                return getField(hook.getOwner(), hook.getField()).get(instance);
            else
                return hook.getMultiplier() * (int) getField(hook.getOwner(), hook.getField()).get(instance);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getInt(Object instance, String clazz, String fieldName) {
        try {
            return (int) getField(clazz, fieldName).get(instance);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static Object getStaticField(Hook hook) {
        try {
            if (hook.getMultiplier() == -1)
                return getField(hook.getOwner(), hook.getField()).get(null);
            else
                return hook.getMultiplier() * (int) getField(hook.getOwner(), hook.getField()).get(null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

}
