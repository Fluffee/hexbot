package org.hexbot;

import org.hexbot.bot.event.RandomManager;
import org.hexbot.gui.First;
import org.hexbot.gui.Login;
import org.hexbot.gui.MainFrame;
import org.hexbot.util.IOHelper;
import org.hexbot.util.Storage;
import org.hexbot.util.VersionChecker;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.pushingpixels.substance.api.skin.GraphiteAquaSkin;
import org.pushingpixels.substance.api.skin.SubstanceGraphiteAquaLookAndFeel;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.Map;

/**
 * The main class for the Boot client.
 */
public final class Boot {

    public static boolean local = true;
    public static MainFrame gui;
    public static First first;

    public static void main(final String[] args) {

        try {
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            try {
                                JFrame.setDefaultLookAndFeelDecorated(true);
                                JPopupMenu.setDefaultLightWeightPopupEnabled(false);
                                ToolTipManager.sharedInstance().setLightWeightPopupEnabled(false);
                                SubstanceLookAndFeel.setSkin(new GraphiteAquaSkin());
                                UIManager.setLookAndFeel(new SubstanceGraphiteAquaLookAndFeel());
//                                if (!VersionChecker.isLatest()) {
//                                    JOptionPane.showMessageDialog(null, "You do not have the latest version of hexbot, please download the latest version.\n By selecting 'Ok' a new version will automatically download", "Out of date!", 0);
//                                    VersionChecker.downloadUpdate();
//                                    System.exit(1);
//                                }
//                                VersionChecker.affixRandomEvents();
//                                RandomManager.load();
                                String sre;
//                                if ((sre = IOHelper.getMessage()) == null || sre.toLowerCase().contains("bad creds")) {
//                                    new Login().setVisible(true);
//                                } else {
                                    new Storage().load();
                                    gui = new MainFrame();
                                    // System.setSecurityManager(new HexGuard());
//                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    );
                }
            }

            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
