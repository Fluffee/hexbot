package org.hexbot.api.util;


public class Random {

    private static final java.util.Random random = new java.util.Random();

    public static boolean nextBoolean() {
        return random.nextBoolean();
    }

    public static int nextInt(int min, int max) {
        min = Math.min(min, max);
        max = Math.max(min, max);
        return min + (max == min ? 0 : random.nextInt(max - min));
    }

    public static double nextDouble(final double min, final double max) {
        return min + random.nextDouble() * (max - min);
    }

    public static long nextLong(final long min, final long max) {
        return min + random.nextLong() * (max - min);
    }

    public static double nextDouble() {
        return random.nextDouble();
    }


}
