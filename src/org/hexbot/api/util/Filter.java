package org.hexbot.api.util;

/**
 * A simple class to filter things
 *
 * @author Parameter
 *
 * @param <F>
 */
public interface Filter<F>
{
    public boolean accept(F f);
}
