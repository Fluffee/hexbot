package org.hexbot.api.listeners;

import java.awt.*;
import java.util.EventListener;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/28/13
 * Time: 6:52 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Paintable extends EventListener {

    public void paint(Graphics g);

}
