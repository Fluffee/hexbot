package org.hexbot.api.listeners;

import org.hexbot.api.events.MessageEvent;

import java.util.EventListener;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 5/6/13
 * Time: 9:42 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MessageListener extends EventListener {
    public void onMessage(MessageEvent event);
}
