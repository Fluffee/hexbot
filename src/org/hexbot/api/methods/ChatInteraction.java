package org.hexbot.api.methods;

import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.api.wrapper.Widget;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 6/1/13
 * Time: 7:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class ChatInteraction {

    private final Npc interacting;

    public ChatInteraction(Npc interacting) {
        this.interacting = interacting;
    }

    public boolean canContinue() {
        return getInteractingWidget() != null && Widgets.canClickContinue();
    }

    public void clickContinue() {
        Widgets.clickContinue();
    }

    public boolean startConversation() {
        if(interacting == null) {
            return false;
        }

        screen: {
            if(interacting.isOnScreen()) {
                if(interacting.interact("Talk")) {
                    Time.sleep(500, 1000);
                    return getInteractingWidget() != null;
                }
            } else {
                Camera.turnTo(interacting);
                break screen;
            }
        }
        return getInteractingWidget() != null;
    }

    public Widget getInteractingWidget() {
        for (Widget widget : Widgets.getLoaded()) {
            if (widget.validate() && widget.getChildren() != null && widget.getChildren().length > 0) {
                for (Component child : widget.getChildren()) {
                    if (child.isValid()) {
                        if (child.getText() != null && interacting != null
                                && interacting.getName() != null && child.getText().contains(interacting.getName())) {
                               return widget;
                        }
                    }
                }
            }
        }
        return null;
    }


}
