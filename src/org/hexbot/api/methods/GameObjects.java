package org.hexbot.api.methods;

import org.hexbot.api.util.Filter;
import org.hexbot.api.wrapper.*;
import org.hexbot.bot.Bot;
import org.hexbot.impl.IGround;
import org.hexbot.impl.ISceneObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 7:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class GameObjects {

    public static List<GameObject> getLoaded() {
        final List<GameObject> list = new LinkedList<GameObject>();
        IGround[][][] i = Bot.client.getWorldController().getGroundArray();
        int plane = Game.getPlane();
        for (int x = 0; x < 104; x++) {
            for (int y = 0; y < 104; y++) {
                IGround ground = i[plane][x][y];
                if (ground != null) {
                    if (ground.getFloorDecoration() != null)
                        list.add(new FloorDecoration(ground.getFloorDecoration(), plane));
                    if (ground.getWallObject() != null)
                        list.add(new WallObject(ground.getWallObject(), plane));
                    ISceneObject[] object = ground.getSceneObjects();
                    for (ISceneObject sceneObject : object) {
                        if (sceneObject != null)
                            list.add(new SceneObject(sceneObject, plane));
                    }
                }
            }
        }
        return list;
    }

    public static GameObject getNearest(final String name) {
        return getNearest(new Filter<GameObject>() {

            @Override
            public boolean accept(GameObject gameObject) {
                return gameObject != null && gameObject.getDefinition() != null && gameObject.getName().equals(name);
            }
        });
    }

    public static GameObject getNearest(Filter<GameObject> filter) {
        GameObject nearest = null;
        for (GameObject g : getLoaded()) {
            if (g != null && filter.accept(g)) {
                if (nearest == null || Calculations.distanceTo(g) < Calculations.distanceTo(nearest))
                    nearest = g;
            }
        }
        return nearest;
    }

    public static GameObject getNearest(final String... names) {
        GameObject temp = null;
        for (String i : names) {
            if ((temp = getNearest(i)) != null)
                return temp;
        }
        return null;
    }

    public static GameObject getNearest(final int... id) {
        GameObject temp = null;
        for (int i : id) {
            if ((temp = getNearest(i)) != null)
                return temp;
        }
        return null;
    }

    public static GameObject getNearest(final int id) {
        if (Players.getLocal() == null)
            return null;
        final Tile playerPosition = Players.getLocal().getLocation();
        GameObject nearest = null;
        double distance = Double.MAX_VALUE;
        for (final GameObject object : getLoaded()) {
            if (object.getId() == id) {
                final double cDistance = Calculations.distanceBetween(playerPosition, object.getLocation());
                if (nearest == null || cDistance < distance) {
                    nearest = object;
                    distance = cDistance;
                }
            }
        }
        return nearest;
    }

    public static GameObject[] getAt(Tile tile) {
        ArrayList<GameObject> objs = new ArrayList<GameObject>();
        for (GameObject obj : GameObjects.getLoaded()) {
            if (obj.getLocation().equals(tile)) {
                objs.add(obj);
            }
        }
        return objs.toArray(new GameObject[objs.size()]);
    }


}
