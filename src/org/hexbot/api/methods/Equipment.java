package org.hexbot.api.methods;

import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.Item;
import org.hexbot.api.wrapper.Tab;

import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/19/13
 * Time: 4:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class Equipment {

    public static final int INTERFACE_ID = 387;


    public static boolean equip(Item item) {
        if (item == null || !Inventory.contains(item.getId()))
            return false;
        return Inventory.getItem(item.getId()).interact("Wield");
    }


    public static boolean unequip(Item item) {
        if (item == null || !isEquipped(item.getId())) return false;

        for (Slot slot : Slot.values()) {
            if (getEquippedItemID(slot) == item.getId()) {
                Component widget = getSlotWidget(slot);
                widget.click();
                return true;
            }
        }

        return false;
    }


    public static boolean unequip(final int id) {
        return unequip(new Item(id, 1));
    }


    public static Component[] getSlotWidgets() {
        setTab();
        Component[] ica = new Component[11];
        int index = 0;
        for (Slot slot : Slot.values()) {
            int id = slot.id();
            ica[index] = Widgets.getChild(INTERFACE_ID, id);
            index++;
        }
        return ica;
    }


    public static boolean isEquipped(String... itemNames) {
        setTab();
        for (Component slot : getSlotWidgets()) {
            if (slot == null)
                continue;
            for (String s : itemNames)
                if (slot.containsAction(s)) {
                    return true;
                }
        }
        return false;
    }


    public static boolean isEquipped(int... itemIDs) {
        setTab();
        for (Slot slot : Slot.values()) {
            if (slot == null)
                continue;
            for (int i : itemIDs)
                if (slot.getItemId() == i) {
                    return true;
                }
        }
        return false;
    }


    public static Integer[] getSlotItemIDs() {
        setTab();
        Vector<Integer> v = new Vector<Integer>();
        int k = 0;
        for (Component slot : getSlotWidgets()) {
            if (slot == null)
                continue;
            int i = Widgets.getChild(INTERFACE_ID, 28).getSlotIds()[k];
            if (i != -1) {
                v.add(i);
                k++;
            }
        }

        return v.toArray(new Integer[]{});
    }

    public static boolean isEmpty(Slot slot) {
        return getEquippedItemID(slot) == -1;
    }


    public static int getEquippedItemID(Slot slot) {
        return slot.getItemId();
    }

    public static void setTab() {
        Tab.open(Tab.EQUIPMENT);
    }

    public static Component getSlotWidget(Slot s) {
        return Widgets.getChild(INTERFACE_ID, s.id());
    }


    public static enum Slot {
        HEAD(12, 0), CAPE(13, 1), NECK(14, 2), WEAPON(16, 3), CHEST(17, 4), SHIELD(18, 5), LEGS(
                26, 6), HANDS(21, 7), FEET(20, 8), RING(22, 9), AMMO(15, 10);

        private int id;
        private int index;

        Slot(int id, int index) {
            this.id = id;
            this.index = index;
        }

        public int id() {
            return id;
        }

        public int index() {
            return index;
        }

        public int getItemId() {
            setTab();
            return Widgets.getChild(INTERFACE_ID, 28).getSlotIds()[index()];
        }

    }

}
