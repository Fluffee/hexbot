package org.hexbot.api.methods;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.InventoryItem;
import org.hexbot.api.wrapper.Item;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/28/13
 * Time: 12:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class Inventory {


	public static InventoryItem[] getAll() {
		final ArrayList<InventoryItem> items = new ArrayList<InventoryItem>();
		if (!Game.isLoggedIn()) {
			return null;
		}
		final Component[] temp = Widgets.getChildren(149);
		if (temp == null || temp.length < 0) {
			return null;
		}
		final Component itemContainer = temp[0];
		if (itemContainer != null) {
			final int[] ids = itemContainer.getSlotIds();
			final int[] stacks = itemContainer.getSlotSizes();
			if (ids != null && stacks != null) {
				for (byte i = 0; i < 28; i++) {
					if (ids[i] > 0) {
						items.add(new InventoryItem(ids[i], stacks[i], i));
					}
				}
			}
		}
		return items.toArray(new InventoryItem[items.size()]);
	}

	public static boolean contains(int... id) {
		return getCount(id) > 0;
	}

	public static boolean contains(int id) {
		return getCount(id) > 0;
	}

	public static boolean contains(String name) {
		return getCount(name) > 0;
	}

	public static boolean contains(String... name) {
		return getCount(name) > 0;
	}

	public static boolean dropAllExcept(final int... ids) {
		final InventoryItem[] items = Inventory.getAll();

		if (items == null) {
			return false;
		}

		items:
		for (final InventoryItem item : items) {
			if (item == null || item.getActions() == null) {
				continue;
			}

			for (final int id : ids) {
				if (id == item.getId()) {
					continue items;
				}
			}

			boolean canDrop = false;

			for (final String a : item.getActions()) {
				if (a != null && a.toLowerCase().startsWith("drop")) {
					canDrop = true;
				}
			}

			if (canDrop) {
				continue;
			}

			item.interact("Drop");
			Time.sleep(120, 170);
		}
		return true;
	}

	public static void dropAll() {
		final InventoryItem[] inv = getAll();
		for (final InventoryItem i : inv) {
			if (i != null) {
				if (i.interact("Drop")) {
					Time.sleep(150, 250);
				}
			}
		}
	}

	public static void dropWithMouseKeys(final int[] items) {
		final InventoryItem[] inv = getAll();
		for (final InventoryItem i : inv) {
			for (final int j : items) {
				if (i != null && i.getId() == j) {
					Mouse.hop(i.getPoint());
					Time.sleep(40, 70);
					if (Mouse.getLocation().equals(i.getPoint())) {
						if (i.interact("Drop")) {
							Time.sleep(250, 400);
						}
					}
				}
			}
		}
	}

	public static void dropAllWithMouseKeys() {
		final InventoryItem[] inv = getAll();
		for (final InventoryItem i : inv) {
			if (i != null) {
				Mouse.hop(i.getPoint());
				Time.sleep(40, 70);
				if (Mouse.getLocation().equals(i.getPoint())) {
					if (i.interact("Drop")) {
						Time.sleep(250, 400);
					}
				}
			}
		}
	}


	public static void dropAllOf(final int i) {
		final InventoryItem[] inv = Inventory.getAll();
		for (InventoryItem item : inv) {
			if (item != null) {
				if (item.getId() == i) {
					if (item.interact("Drop")) {
						Time.sleep(150, 250);
					}
				}
			}
		}
	}

	public static void dropWithMouseKeys(final String[] names) {
		final InventoryItem[] inv = getAll();
		for (final InventoryItem i : inv) {
			for (final String n : names) {
				if (i != null && i.getName().equals(n)) {
					Mouse.hop(i.getPoint());
					Time.sleep(40, 70);
					if (Mouse.getLocation().equals(i.getPoint())) {
						if (i.interact("Drop")) {
							Time.sleep(250, 400);
						}
					}
				}
			}
		}
	}

	public static InventoryItem getItem(final int id) {
		for (final InventoryItem item : getAll()) {
			if (item != null) {
				if (item.getId() == id) {
					return item;
				}
			}
		}
		return null;
	}

	public static InventoryItem getItem(final String name) {
		for (final InventoryItem item : getAll()) {
			if (item != null) {
				if (item.getName().contains(name)) {
					return item;
				}
			}
		}
		return null;
	}

	public static InventoryItem getItemByAction(final String action) {
		for (final InventoryItem item : getAll()) {
			if (item != null) {
				if (item.getActions() != null) {
					for (String str : item.getActions()) {
						if (str.toLowerCase().equals(action.toLowerCase())) {
							return item;
						}
					}
				}
			}
		}
		return null;
	}

	public static int getCount() {
		int total = 0;
		if (Game.getGameState() != 25) {
			for (final Item item : getAll()) {
				if (item != null) {
					if (item.getId() > 0) {
						total++;
					}
				}
			}
		}
		return total;
	}

	public static int getCount(int... id) {
		int total = 0;
		if (Game.getGameState() != 25) {
			for (int i : id) {
				if (i != -1) {
					total += getCount(i);
				}
			}
		}
		return total;
	}

	public static int getCount(String... name) {
		int total = 0;
		if (Game.getGameState() != 25) {
			for (String i : name) {
				if (i != null) {
					total += getCount(i);
				}
			}
		}
		return total;
	}

	public static int getCount(int id) {
		int total = 0;
		if (Game.getGameState() != 25) {
			for (final Item item : getAll()) {
				if (item != null) {
					if (item.getId() == id) {
						total++;
					}
				}
			}
		}
		return total;
	}

	public static int getCount(final String name) {
		int total = 0;
		if (Game.getGameState() != 25) {
			for (final Item item : getAll()) {
				if (item != null) {
					if (item.getName().contains(name)) {
						total++;
					}
				}
			}
		}
		return total;
	}


	public static boolean isFull() {
		return getCount() == 28;
	}

	public static boolean isEmpty() {
		return getCount() == 0;
	}

}
