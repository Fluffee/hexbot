package org.hexbot.api.methods;

import org.hexbot.api.util.Filter;
import org.hexbot.api.wrapper.Player;
import org.hexbot.bot.Bot;
import org.hexbot.impl.IPlayer;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/30/13
 * Time: 10:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class Players {

    public static Player getLocal() {
        IPlayer player = Bot.client.getLocalPlayer();
        return player == null ? null : new Player(Bot.client.getLocalPlayer());
    }

    public static Player[] getLoaded() {
        ArrayList<Player> players = new ArrayList<Player>();
        IPlayer[] iPlayers = Bot.client.getPlayers();
        for (IPlayer ip : iPlayers) {
            if (ip != null)
                players.add(new Player(ip));
        }
        return players.toArray(new Player[1]);
    }

    public static Player getNearest(Filter<Player> filter) {
        Player temp = null;
        for(Player n : getLoaded()) {
            if(filter.accept(n)) {
                if(temp == null || Calculations.distanceTo(n) < Calculations.distanceTo(temp))
                    temp = n;
            }
        }
        return temp;
    }


    public static Player getNearest(String... names) {
        Player temp = null;
        for (String i : names) {
            if ((temp = getNearest(i)) != null)
                return temp;
        }
        return null;
    }



    public static Player getNearest(String name) {
        Player nearest = null;
        double distance = Double.MAX_VALUE;
        for (Player Player : Players.getLoaded()) {
            if (Player == null || Player.getName() == null)
                continue;
            if (Player.getName().equals(name)) {
                double tempDist = -1D;
                if ((tempDist = Calculations.distanceTo(Player.getLocation())) < distance) {
                    nearest = Player;
                    distance = tempDist;
                }
            }
        }
        return nearest;
    }
}
