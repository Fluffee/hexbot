package org.hexbot.api.methods;

import java.util.ArrayList;
import java.util.List;

import org.hexbot.api.util.Filter;
import org.hexbot.api.wrapper.GroundItem;
import org.hexbot.impl.IItem;
import org.hexbot.impl.INode;
import org.hexbot.impl.INodeList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 7:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class GroundItems {

    public static GroundItem[] getAll() {
        List<GroundItem> list = new ArrayList<GroundItem>();
        int plane=Game.getPlane();
        INodeList[][] nodeList = Game.getItemArray()[plane];
        for (int i = 0; i < 104; i++) {
            for (int j = 0; j < 104; j++) {
                INodeList nl = nodeList[i][j];
                if (nl == null)
                    continue;
                INode holder = nl.getHead();
                INode curNode = holder.getNext();
                while (curNode != null && curNode != holder && curNode != nl.getHead()) {
                    INode node = curNode;
                    list.add(new GroundItem(node, i + Game.getBaseX(), j + Game.getBaseY(),plane));
                    curNode = curNode.getNext();
                }
            }
        }
        return list.toArray(new GroundItem[list.size()]);
    }

    public static boolean isItem(Object o) {
        return o instanceof IItem;
    }



    public static GroundItem get(int id) {
        for (GroundItem gi : getAll()) {
            if (gi.getId() == id)
                return gi;
        }
        return null;
    }


    public static GroundItem getNearest(Filter<GroundItem> filter) {
        GroundItem nearest = null;
        for(GroundItem g : getAll()) {
            if(filter.accept(g)) {
                if(nearest == null || Calculations.distanceTo(g) < Calculations.distanceTo(nearest))
                    nearest = g;
            }
        }
        return nearest;
    }
    

    public static GroundItem getNearest(int... ids) {
        GroundItem temp = null;
        for (int i : ids) {
            if ((temp = getNearest(i)) != null)
                return temp;
        }
        return null;
    }

    public static GroundItem getNearest(String... names) {
        GroundItem temp = null;
        for (String i : names) {
            if ((temp = getNearest(i)) != null)
                return temp;
        }
        return null;
    }
    
    public static GroundItem getNearest(final String name) {
        return getNearest(new Filter<GroundItem>() {

            @Override
            public boolean accept(GroundItem groundItem) {
                return groundItem != null && groundItem.getDefinition() != null && groundItem.getName().equals(name);
            }
        });
    }

    public static GroundItem getNearest(int id) {
        GroundItem nearest = null;
        double distance = Double.MAX_VALUE;
        for (GroundItem groundItem : getAll()) {
            if (groundItem == null)
                continue;
            if (groundItem.getId() == id) {
                double tempDist = -1D;
                if ((tempDist = Calculations.distanceTo(groundItem.getLocation())) < distance) {
                    nearest = groundItem;
                    distance = tempDist;
                }
            }
        }
        return nearest;
    }

}
