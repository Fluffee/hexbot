package org.hexbot.api.methods;

import org.hexbot.api.util.Filter;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.bot.Bot;
import org.hexbot.impl.INpc;

import java.util.ArrayList;

public class Npcs {

    public static Npc[] getLoaded() {
        ArrayList<Npc> npcs = new ArrayList<Npc>();
        INpc[] iNpcs = Bot.client.getNpcs();
        for (INpc in : iNpcs) {
            if (in != null)
                npcs.add(new Npc(in));
        }
        return npcs.toArray(new Npc[iNpcs.length - 1]);
    }

    public static Npc get(String name) {
        for (Npc npc : Npcs.getLoaded()) {
            if (npc == null || npc.getName() == null)
                continue;
            if (npc.getName().equals(name))
                return npc;
        }
        return null;
    }

    public static Npc get(int id) {
        for (Npc npc : Npcs.getLoaded()) {
            if (npc == null || npc.getName() == null)
                continue;
            if (npc.getId() == id)
                return npc;
        }
        return null;
    }
    
    public static Npc getNpcByActions(String[] actions) {
		for (Npc npc : Npcs.getLoaded()) {
			if (npc != null) {
				ArrayList<String> npcActions = new ArrayList<String>();
				for (String action : npc.getDefinition().getActions()) {
					npcActions.add(action);
					if (npcActions.contains(actions[0]) && npcActions.contains(actions[1])) {
						return Npcs.getNearest(npc.getId());
					}
				}
			}
		}
		return null;
	}
	
   public static Npc getInteractingWithPlayer() {
        for(Npc n : Npcs.getLoaded()){
            if(n != null && n.getInteracting() == Players.getLocal()) {
                    return n;
            }
        }
        return null;
    }

    public static Npc getNearest(Filter<Npc> filter) {
        Npc temp = null;
        for (Npc n : getLoaded()) {
            if (filter.accept(n)) {
                if (temp == null || Calculations.distanceTo(n) < Calculations.distanceTo(temp))
                    temp = n;
            }
        }
        return temp;
    }

    public static Npc getNearest(int... id) {
        Npc temp = null;
        for (int i : id) {
            if ((temp = getNearest(i)) != null)
                return temp;
        }
        return null;
    }

    public static Npc getNearest(String... id) {
        Npc temp = null;
        for (String i : id) {
            if ((temp = getNearest(i)) != null)
                return temp;
        }
        return null;
    }

    public static Npc getNearest(int id) {
        Npc nearest = null;
        double distance = Double.MAX_VALUE;
        for (Npc npc : Npcs.getLoaded()) {
            if (npc == null || npc.getName() == null)
                continue;
            if (npc.getId() == id) {
                double tempDist = -1D;
                if ((tempDist = Calculations.distanceTo(npc.getLocation())) < distance) {
                    nearest = npc;
                    distance = tempDist;
                }
            }
        }
        return nearest;
    }

    public static Npc getNearest(String name) {
        Npc nearest = null;
        double distance = Double.MAX_VALUE;
        for (Npc npc : Npcs.getLoaded()) {
            if (npc == null || npc.getName() == null)
                continue;
            if (npc.getName().equals(name)) {
                double tempDist = -1D;
                if ((tempDist = Calculations.distanceTo(npc.getLocation())) < distance) {
                    nearest = npc;
                    distance = tempDist;
                }
            }
        }
        return nearest;
    }
}
