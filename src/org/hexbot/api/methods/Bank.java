package org.hexbot.api.methods;

import org.hexbot.api.input.Keyboard;
import org.hexbot.api.input.Mouse;
import org.hexbot.api.util.Time;
import org.hexbot.api.util.Timer;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/8/13
 * Time: 12:34 AM
 * To change this template use File | Settings | File Templates.
 */

public class Bank {

    public static final int[] BANKERS = new int[]{44, 45, 56, 57, 494, 495, 498, 499, 909, 958, 1036, 2271, 2354, 2355, 3824,
            5488, 5901, 4456, 4457, 4458, 4459, 5912, 5913, 6362, 6532, 6533, 6534, 6535, 7605, 8948, 9710, 14367};
    public static final int[] BANK_BOOTHS = new int[]{782, 2213, 2995, 5276, 6084, 10517, 11402, 11758, 12759, 14367,
            19230, 20325, 24914, 25808, 26972, 29085, 52589, 34752, 35647, 36786, 2012, 2015, 2019};
    public static final int[] BANK_CHESTS = new int[]{693, 4483, 12308, 20607, 21301, 27663, 42192};

    public static final String BANK_BOOTH = "Bank Booth";
    public static final String BANKER = "Banker";
    public static final String BANK_CHEST = "Bank Chest";

    public static boolean open() {
        if (isOpen()) {
            return true;
        }

        final Npc npc = Npcs.getNearest(BANKER);
        final GameObject booth = GameObjects.getNearest(BANK_BOOTH);
        final GameObject chest = GameObjects.getNearest(BANK_CHEST);

        GameObject temp;
        if (booth == null) {
            temp = chest;
        } else {
            temp = booth;
        }

        if (temp != null) {
            if (temp.isOnScreen()) {
                return temp.interact("Open") || temp.interact("Use-quickly");
            } else {
                Camera.turnTo(temp);
            }
        } else if (npc != null) {
            if (npc.isOnScreen()) {
                return npc.interact("Bank");
            } else {
                Camera.turnTo(npc);
            }
        }
        return false;
    }


    public static boolean isOpen() {
        return (Widgets.getChild(12, 4) != null) && Widgets.getChild(12,4).isOnScreen();
    }
    
    public static void withdraw(String name,int amount){
		BankItem item = null;
		
		for(BankItem b : Bank.getItems()){
			if(b != null && b.getName().toLowerCase().contentEquals(name.toLowerCase())){
				item = b;
			}
		}
		
		if(item != null){
			switch(amount){
			 case 1:
				 item.interact("Withdraw 1");
	                break;
	            case 5:
	            	item.interact("Withdraw 5");
	                break;
	            case 10:
	            	item.interact("Withdraw 10");
	                break;
	            default:
	                if (amount < item.getStackSize() && amount != 0) {
	                	item.interact("Withdraw X");
	                    Time.sleep(800, 1000);
	                    Keyboard.sendKeys(new StringBuilder().insert(0, "").append(amount).toString());
	                } else {
	                	item.interact("Withdraw All");
	        		 }
	            	}
		}
	}

 public static boolean withdraw(int id, int amount) {
        BankItem var2 = getItem(id);
        if (var2 == null) {
            System.out.println("Well fuck...");
            return false;
        }
        switch (amount) {
            case 1:
                var2.interact("Withdraw 1");
                break;
            case 5:
                var2.interact("Withdraw 5");
                break;
            case 10:
                var2.interact("Withdraw 10");
                break;
            default:
                if (amount < var2.getStackSize() && amount != 0) {
                    var2.interact("Withdraw X");
                    Time.sleep(800, 1000);
                    Keyboard.sendKeys(new StringBuilder().insert(0, "").append(amount).toString());
                } else {
                    var2.interact("Withdraw All");
                }
        }
        return true;
    }

    public static boolean close() {
        Component var0;
        boolean b = !isOpen() ? false : ((var0 = Widgets.getChild(12, 102)) != null ? var0.interact("Close") : false);
        for (int i = 0; isOpen() && i < 10; i++)
            Time.sleep(20);
        return !isOpen();
    }

    public static BankItem[] getItems() {
        final ArrayList<BankItem> items = new ArrayList<BankItem>();
        final Component itemContainer = Widgets.getChild(12, 89);
        if (itemContainer == null || !itemContainer.isValid()) {
            System.out.println("WTF...");
            return null;
        }
        if (itemContainer != null) {
            final int[] ids = itemContainer.getSlotIds();
            final int[] stacks = itemContainer.getSlotSizes();
            if (ids != null && stacks != null) {
                for (int i = 0; i < ids.length; i++) {
                    if (ids[i] > 0) {
                        items.add(new BankItem(ids[i] - 1, stacks[i], i));
                    }
                }
            }
        }
        return items.toArray(new BankItem[items.size()]);
    }

    public static BankItem getItem(int var0) {
        for (BankItem item : getItems()) {
            if (item.getId() == var0)
                return item;
        }
        return null;
    }

    public static BankItem getItem(String name){
        for (BankItem item : getItems()) {
            if(item != null && item.getName().contentEquals(name)){
                return item;
            }
        }
        return null;
    }

    public static int getCount() {
        return getItems().length;
    }

    public static boolean deposit(int id, int amount) {
	        InventoryItem var2 = Inventory.getItem(id);
	        if (var2 == null)
	            return false;
	        switch (amount) {
	            case 0:
	                var2.interact("Store All");
	            case 1:
	                var2.interact("Store 1");
	                break;
	            case 5:
	                var2.interact("Store 5");
	                break;
	            case 10:
	                var2.interact("Store 10");
	                break;
	            default:
	                if (amount < var2.getStackSize() && amount != 0) {
	                    var2.interact("Store X");
	                    Time.sleep(800, 1000);
	                    Keyboard.sendKeys((new StringBuilder()).insert(0, "").append(amount).toString());
	                } else {
	                    var2.interact("Store All");
	                }
	        }
	        return true;
	    }

    public static boolean deposit(String name, int amount) {
        InventoryItem var2 = Inventory.getItem(name);
        if (var2 == null)
            return false;
        switch (amount) {
            case 0:
                var2.interact("Store All");
            case 1:
                var2.interact("Store 1");
                break;
            case 5:
                var2.interact("Store 5");
                break;
            case 10:
                var2.interact("Store 10");
                break;
            default:
                if (amount < var2.getStackSize() && amount != 0) {
                    var2.interact("Store X");
                    Time.sleep(800, 1000);
                    Keyboard.sendKeys((new StringBuilder()).insert(0, "").append(amount).toString());
                } else {
                    var2.interact("Store All");
                }
        }
        return true;
    }

    public static boolean depositAllExcept(int... except) {
        main:
        for (InventoryItem item : Inventory.getAll()) {
            for (int i : except) {
                if (i == item.getId()) {
                    continue main;
                }
            }
            deposit(item.getId(), 0);
			Time.sleep(500, 1000);
        }
        for (InventoryItem item : Inventory.getAll()) {
            if (!Arrays.asList(except).contains(item.getId())) {
                return false;
            }
        }
        return true;
    }

    public static void depositAll() {
        if(Widgets.getChild(12, 22)!= null){
            Widgets.getChild(12, 22).click();
            Timer t = new Timer(2000);
            while(t.isRunning() && Inventory.getCount() >0){
                Time.sleep(100);
            }
        }
    }

    public static void depostitEquipment(){
        if(Widgets.getChild(12, 24)!= null){
            Widgets.getChild(12, 24).click();
            Time.sleep(500, 750);
        }
    }

    public static boolean contains(String name){
        if(Widgets.getChild(12,7) != null){
            for(Component c : Widgets.getChild(12,7).getChildren()){
                System.out.println(c.getComponentName());
                if(c != null && c.getComponentName().replace("<col=ff9040>","").contentEquals(name)){
                    return true;
                }
            }
        }
        return false;
    }

    public static Point getMidpoint(int slot) {
        Component child = Widgets.getChild(12, 89);
        Rectangle area = child.getBounds();
        int x = (slot % 8) * 44 + area.x + 17;
        int y = (((slot / 8) * 40 + area.y + 13));
        return new Point(x, y);
    }

    static Tile lumbridgeCastle = new Tile(3208, 3220, 2);
    static Tile draynorBank = new Tile(3093, 3243, 0);
    static Tile faladorBank = new Tile(2945, 3369, 0);
    static Tile faladorBank2 = new Tile(3013, 3355, 0);
    static Tile varrockWestBank = new Tile(3185, 3434, 0);
    static Tile varrockEastBank = new Tile(3253, 3420, 0);
    static Tile catherbyBank = new Tile(2808, 3440, 0);
    static Tile alkharidBank = new Tile(3269, 3167, 0);
    static Tile canifisBank = new Tile(3510, 3479, 0);
    static Tile edgevilleBank = new Tile(3093, 3492, 0);
    static Tile ardougneBank = new Tile(2654, 3283, 0);
    static Tile ardougneBank2 = new Tile(2615, 3332, 0);
    static Tile camelotBank = new Tile(2724, 3490, 0);
    static Tile yanilleBank = new Tile(2610, 3092, 0);
    static Tile voidKnightBank = new Tile(2666, 2650, 0);
    static Tile fishingGuildBank = new Tile(2584, 3420, 0);
    static Tile portKhazardBank = new Tile(2662, 3158, 0);
    static Tile duelArenaBank = new Tile(3382, 3269, 0);
    static Tile shiloVillageBank = new Tile(2851, 2954, 0);
    static Tile[] bankTiles = new Tile[]{lumbridgeCastle, draynorBank, faladorBank, varrockEastBank, varrockWestBank, faladorBank2, catherbyBank, alkharidBank, canifisBank,
            edgevilleBank, ardougneBank, ardougneBank2, camelotBank, yanilleBank, voidKnightBank, fishingGuildBank, portKhazardBank, duelArenaBank, shiloVillageBank};


    public static Tile getNearestBankTile() {
        Tile closestTile = null;
        double closestDistance = Double.MAX_VALUE;
        for (Tile tile : bankTiles) {
            double distance = tile.getDistance();
            if (distance < closestDistance) {
                closestTile = tile;
                closestDistance = distance;
            }
        }
        return closestTile;
    }

    public static void walkNearestBank() {
        Tile tile = getNearestBankTile();
        if (tile != null) {
            Walking.walkWeb(tile);
        }
    }


    public static class BankItem extends Item {
        int index;

        public BankItem(int id, int stackSize, int i) {
            super(id, stackSize);
            index = i;
        }

        public Point getPoint() {
            return getMidpoint(index);
        }

        public void interact(String s) {
            Mouse.move(getPoint());
            Mouse.click(false);
            while (!Menu.openMenu())
                Time.sleep(10);
            Menu.click(s);
        }
    }

}

