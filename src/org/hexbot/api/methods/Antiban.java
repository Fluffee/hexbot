package org.hexbot.api.methods;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.util.Random;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.*;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Fryslan
 * Date: 24-8-13
 * Time: 21:46
 */
public class Antiban {

	private static int[] tabs = {Tab.STATS, Tab.QUEST, Tab.PRAYER, Tab.OPTIONS, Tab.CLAN, Tab.COMBAT, Tab.EMOTES, Tab.EQUIPMENT,
			Tab.FRIENDS_LIST, Tab.STATS, Tab.MUSIC, Tab.MAGIC};

	public static Component SKILL_ATTACK = Widgets.getChild(320, 123);
	public static Component SKILL_CONSTITUTION = Widgets.getChild(320, 124);
	public static Component SKILL_MINING = Widgets.getChild(320, 125);
	public static Component SKILL_STRENGTH = Widgets.getChild(320, 126);
	public static Component SKILL_AGILITY = Widgets.getChild(320, 127);
	public static Component SKILL_SMITHING = Widgets.getChild(320, 128);
	public static Component SKILL_DEFENCE = Widgets.getChild(320, 129);
	public static Component SKILL_HERBLORE = Widgets.getChild(320, 130);
	public static Component SKILL_FISHING = Widgets.getChild(320, 131);
	public static Component SKILL_RANGED = Widgets.getChild(320, 132);
	public static Component SKILL_THIEVING = Widgets.getChild(320, 133);
	public static Component SKILL_COOKING = Widgets.getChild(320, 134);
	public static Component SKILL_PRAYER = Widgets.getChild(320, 135);
	public static Component SKILL_CRAFTING = Widgets.getChild(320, 136);
	public static Component SKILL_FIREMAKING = Widgets.getChild(320, 137);
	public static Component SKILL_MAGIC = Widgets.getChild(320, 142);
	public static Component SKILL_FLETCHING = Widgets.getChild(320, 143);
	public static Component SKILL_WOODCUTTING = Widgets.getChild(320, 144);
	public static Component SKILL_RUNECRAFTING = Widgets.getChild(320, 145);
	public static Component SKILL_SLAYER = Widgets.getChild(320, 146);
	public static Component SKILL_FARMING = Widgets.getChild(320, 147);
	public static Component SKILL_CONSTRUCTION = Widgets.getChild(320, 148);
	public static Component SKILL_HUNTER = Widgets.getChild(320, 149);


	private static Component[] skills = {SKILL_ATTACK, SKILL_CONSTITUTION, SKILL_MINING, SKILL_STRENGTH, SKILL_AGILITY, SKILL_SMITHING,
			SKILL_DEFENCE, SKILL_HERBLORE, SKILL_FISHING, SKILL_RANGED, SKILL_THIEVING, SKILL_COOKING, SKILL_PRAYER,
			SKILL_CRAFTING, SKILL_FIREMAKING, SKILL_MAGIC, SKILL_FLETCHING, SKILL_WOODCUTTING, SKILL_RUNECRAFTING,
			SKILL_SLAYER, SKILL_FARMING, SKILL_CONSTRUCTION, SKILL_HUNTER};

	public static void openRandomTab() {
		int currentTab = Tab.getOpenTabID();
		Tab.open(Random.nextInt(tabs[0], tabs.length - 1));
		Time.sleep(750, 1500);
		Tab.open(currentTab);
	}

	public static void hoverRandomSkill() {
		if (!Tab.isOpen(Tab.STATS)) {
			Tab.open(Tab.STATS);
		} else {
			int r = Random.nextInt(0, skills.length - 1);
			Mouse.move(skills[r].getPoint());
			Time.sleep(2000, 4500);
		}
	}

	public static void hoverSkill(Component skill) {
		if (!Tab.isOpen(Tab.STATS)) {
			Tab.open(Tab.STATS);
		} else {
			Mouse.move(skill.getPoint());
			Time.sleep(2000, 4500);
		}
	}

	public static void hoverTotalLevel() {
		if (!Tab.isOpen(Tab.STATS)) {
			Tab.open(Tab.STATS);
		} else {
			Component c = Widgets.getChild(320, 140);
			if (c != null) {
			Mouse.move(c.getPoint());
			Time.sleep(2000, 4500);
			}
		}
	}

	public static void moveMouseRandomly() {
		Point p = new Point(Mouse.getLocation().x + Random.nextInt(-500, +500), Mouse.getLocation().y + Random.nextInt(-500, +500));
		Mouse.move(p);
	}

	public static void moveMouseSlightly() {
		Point p = new Point(Mouse.getLocation().x + Random.nextInt(-10, +10), Mouse.getLocation().y + Random.nextInt(-10, +10));
		Mouse.move(p);
	}

	public static void moveMouseOffScreen() {
		int r = Random.nextInt(0, 4);
		int x = Mouse.getLocation().x;
		int y = Mouse.getLocation().y;
		switch (r) {
			case 0:
				Mouse.move(Mouse.getLocation().x - (x + Random.nextInt(-3, -5)), Mouse.getLocation().y + Game.getBaseY() - x + Random.nextInt(+3, +5));
				Time.sleep(250, 1000);
				break;
			case 1:
				Mouse.move(Mouse.getLocation().x + Game.getBaseX() - x + Random.nextInt(+3, +5), Mouse.getLocation().y - (y + Random.nextInt(-3, -5)));
				Time.sleep(250, 1000);
				break;
			case 2:
				Mouse.move(Mouse.getLocation().x + Game.getBaseX() - x + Random.nextInt(+3, +5), Mouse.getLocation().y + Game.getBaseY() - x + Random.nextInt(+3, +5));
				Time.sleep(250, 1000);
				break;
			case 3:
				Mouse.move(Mouse.getLocation().x - (x + Random.nextInt(-3, -5)), Mouse.getLocation().y - (y + Random.nextInt(-3, -5)));
				Time.sleep(250, 1000);
				break;
			default:
				Mouse.move(Mouse.getLocation().x - (x + Random.nextInt(-3, -5)), Mouse.getLocation().y - (y + Random.nextInt(-3, -5)));
				Time.sleep(250, 1000);
				break;
		}
	}

	public static void moveCameraRandomly() {
		Camera.setCameraRotation(Random.nextInt(0, 360));
	}

	public static void moveCameraSlightly() {
		Camera.setCameraRotation(Random.nextInt(0, 25));
	}

	public static void setRandomMouseSpeed() {
		int r = Random.nextInt(0, 3);
		switch (r) {
			case 0:
				Mouse.setSpeed(Mouse.Speed.FAST);
				break;
			case 1:
				Mouse.setSpeed(Mouse.Speed.SLOW);
				break;
			case 2:
				Mouse.setSpeed(Mouse.Speed.NORMAL);
				break;
			default:
				Mouse.setSpeed(Mouse.Speed.FAST);
				break;
		}
	}

	public static void examineRandomObject() {
		for (GameObject g : GameObjects.getLoaded()) {
			if (g != null && g.isOnScreen()) {
				g.interact("Examine");
			}
		}
	}

	public static void examineRandomNpc() {
		for (Npc g : Npcs.getLoaded()) {
			if (g != null && g.isOnScreen()) {
				g.interact("Examine");
			}
		}
	}

	public static void examineRandomPlayer() {
		for (Player g : Players.getLoaded()) {
			if (g != null && g.isOnScreen()) {
				g.interact("Examine");
			}
		}
	}

	public static void hoverRandomObject() {
		for (GameObject g : GameObjects.getLoaded()) {
			if (g != null && g.isOnScreen()) {
				Mouse.move(g.getScreenLocation());
				Time.sleep(750, 2000);
			}
		}
	}

	public static void hoverRandomNpc() {
		for (Npc g : Npcs.getLoaded()) {
			if (g != null && g.isOnScreen()) {
				Mouse.move(g.getScreenLocation());
				Time.sleep(750, 2000);
			}
		}
	}

	public static void hoverRandomPlayer() {
		for (Player g : Players.getLoaded()) {
			if (g != null && g.isOnScreen()) {
				Mouse.move(g.getScreenLocation());
				Time.sleep(750, 2000);
			}
		}
	}

	public static void hoverRandomGroundItem() {
		for (GroundItem g : GroundItems.getAll()) {
			if (g != null && g.isOnScreen()) {
				Mouse.move(g.getLocation().getScreenLocation());
				Time.sleep(750, 2000);
			}
		}
	}

	public static void changeMusicVolume() {
		if (Tab.isOpen(Tab.OPTIONS)) {
			int r = Random.nextInt(0, 4);
			Component[] c = Widgets.getChildren(261);
			switch (r) {
				case 0:
					c[7].click();
					break;
				case 1:
					c[8].click();
					break;
				case 2:
					c[9].click();
					break;
				case 3:
					c[10].click();
					break;
				default:
					c[7].click();
					break;
			}
		} else {
			Tab.open(Tab.OPTIONS);
		}
	}
}
