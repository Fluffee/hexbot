package org.hexbot.api.methods;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.util.Random;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Entity;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.api.wrapper.Player;
import org.hexbot.api.wrapper.Tab;
import org.hexbot.bot.Bot;
import org.hexbot.impl.ICollisionMap;
import org.hexbot.impl.IFriend;
import org.hexbot.impl.INodeList;
import org.hexbot.impl.IWorldController;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/28/13
 * Time: 11:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class Game {

	public static int[][] worlds = new int[][] {
			{301, 302, 303, 304, 305, 306, 308, 309, 310, 311, 312, 313, 314, 316, 317, 318},
			{319, 320, 321, 322, 325, 326, 327, 328, 329, 330, 333, 334, 335, 336, 337, 338},
			{341, 342, 343, 344, 345, 346, 349, 350, 351, 352, 353, 354, 357, 358, 359, 360},
			{361, 362, 365, 366, 367, 368, 369, 370, 373, 374, 375, 376, 377, 378}};

    public static Entity[] getLoadedEntities() {
        Npc[] npcs = Npcs.getLoaded();
        Player[] players = Players.getLoaded();

        ArrayList<Entity> entities = new ArrayList<Entity>();
        for (Npc npc : npcs) {
            entities.add(npc);
        }

        for (Player player : players) {
            entities.add(player);
        }

        return entities.toArray(new Entity[entities.size()]);
    }

    public static int[][] getFlags() {
        return Bot.client.getCollisionMap()[getPlane()].getFlags();
    }

    public static boolean isRunning() {
        return Settings.get(173) == 1;
    }

//    public static boolean isItemSelected() {
//        return Menu.getMenuItems().get(0).contains("Use");
//    }

//    public boolean toggleRunning() {
//        if (isRunning()) {
//            setRunning(false);
//            return false;
//        } else {
//            setRunning(true);
//            return true;
//        }
//    }


    /**
     * @return The player's current energy
     */
    public static int getEnergy() {
        return Bot.client.getRunEnergy();
    }

    public static int getHpPercent() {
        return (getHp() / getMaxHp()) * 100;
    }

    public static int getHp() {
        return Skills.CONSTITUTION.getCurrentLevel();
    }

    public static int getMaxHp() {
        return Skills.CONSTITUTION.getRealLevel();
    }

    public static boolean isLoggedIn() {
        return getGameState() > 25;
    }

    public static void hopWorld(int world) {
        Bot.setLogin(false);
        if (isLoggedIn()) {
            logout();
        }
        //if (atWelcomeScreen()) {
            Mouse.click(Random.nextInt(35, 75), Random.nextInt(469, 495));
            Time.sleep(2200, 2500);
       // }
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 16; y++) {
                if (worlds[x][y] == world) {
                    Mouse.click(getPoint(x, y));
                    Time.sleep(2200, 2500);
                    Bot.setLogin(true);
                }
            }
        }
    }

	public static void hopToRandomWorld() {
		for (final int[] worldz : worlds) {
			int world = worldz[new java.util.Random().nextInt(worldz.length)]; //grabs a random world value from the array
			if (world != -1) { //Fail safe
			Game.hopWorld(world);
			}
		}
	}

    protected static Point getPoint(int x, int y) {
        return new Point(250 + ((x % 3) * 91), 83 + ((y % 16) * 24));
    }

    public static void logout() {
        if (!Tab.isOpen(Tab.LOGOUT)) {
            Tab.open(Tab.LOGOUT);
            Time.sleep(2300, 2800);
        }
        org.hexbot.api.wrapper.Component button = Widgets.getChild(182, 6);
        if (button != null && button.isValid()) {
            button.click();
            Time.sleep(1300, 2800);
        }
    }

    public static boolean atLoginScreen() {
        return getGameState() == 10;
    }

    public static boolean atWelcomeScreen() {
        return getGameState() > 10 && getLoginIndex() < 30;
    }

    public static boolean atNewUser() {
        return getLoginIndex() == 1;
    }

    public static boolean atExistingUser() {
        return getLoginIndex() == 2;
    }

    public static boolean atCreateUser() {
        return getLoginIndex() == 3;
    }

    public static int getLoginIndex() {
        return Bot.client.getLoginIndex();
    }

    public static int getGameState() {
        return Bot.client.getGameState();
    }

    public IFriend[] getFriendsList() {
        return Bot.client.getFriends();
    }

    public static int getMapScale() {
        return Bot.client.getMapScale();
    }

    public static int getMapOffset() {
        return Bot.client.getMapOffset();
    }

    public static int getCompassAngle() {
        return Bot.client.getCompassAngle();
    }

    public static int getPlane() {
        return Bot.client.getPlane();
    }

    public static int getBaseX() {
        return Bot.client.getBaseX();
    }

    public static int getBaseY() {
        return Bot.client.getBaseY();
    }

    public static int getCameraX() {
        return Bot.client.CameraX();
    }

    public static int getCameraY() {
        return Bot.client.CameraY();
    }

    public static int getCameraZ() {
        return Bot.client.CameraZ();
    }

    public static int getPitch() {
        return Bot.client.getPitch();
    }

    public static int getYaw() {
        return Bot.client.getYaw();
    }

    public static int[] getSkillLevelArray() {
        return Bot.client.getSkillLevelArray();
    }

    public static int[] getRealSkillLevelArray() {
        return Bot.client.getRealSkillLevelArray();
    }

    public static int[] getSkillExpArray() {
        return Bot.client.getSkillExpArray();
    }

    public static int getMenuX() {
        return Bot.client.getMenuX();
    }

    public static int getMenuY() {
        return Bot.client.getMenuY();
    }

    public static int getMenuWidth() {
        return Bot.client.getMenuWidth();
    }

    public static int getMenuHeight() {
        return Bot.client.getMenuHeight();
    }

    public static boolean isMenuOpen() {
        return Bot.client.isMenuOpen();
    }

    public static int getMenuCount() {
        return Bot.client.getMenuCount();
    }

    public static String[] getMenuActions() {
        return Bot.client.getMenuActions();
    }

    public static String[] getMenuOptions() {
        return Bot.client.getMenuOptions();
    }

    public static byte[][][] getTileBytes() {
        return Bot.client.getTileBytes();
    }

    public static int[][][] getTileHeights() {
        return Bot.client.getTileHeights();
    }

    public static Object getMouse() {
        return Bot.client.getMouse();
    }

    public static Object getKeyboard() {
        return Bot.client.getKeyboard();
    }

    public static INodeList[][][] getItemArray() {
        return Bot.client.getItemArray();
    }

    public static IWorldController getWorldController() {
        return Bot.client.getWorldController();
    }

    public static int[] getSettings() {
        return Bot.client.getSettings();
    }

	public static int getSpecialAttackEnergy() {
		return Settings.get(300) / 10;
	}

	public static boolean isPlayerPoisoned() {
		return Settings.get(102) != 0;
	}


	public ICollisionMap[] getCollisionMaps() {
        return Bot.client.getCollisionMap();
    }

    public void captureScreen(String fileName) throws Exception {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle screenRectangle = new Rectangle(Bot.applet.getLocationOnScreen().x,Bot.applet.getLocationOnScreen().y,Bot.applet.getWidth(),Bot.applet.getHeight());
        Robot robot = new Robot();
        BufferedImage image = robot.createScreenCapture(screenRectangle);
        ImageIO.write(image, "png", new File(fileName + ".png"));
    }

    public void captureScreen(String fileName, int xPos, int yPos, int Width, int Height) throws Exception {
        Rectangle screenRectangle = new Rectangle(Bot.applet.getLocationOnScreen().x+xPos,Bot.applet.getLocationOnScreen().y+yPos,Width,Height);
        Robot robot = new Robot();
        BufferedImage image = robot.createScreenCapture(screenRectangle);
        ImageIO.write(image, "png", new File(fileName+".png"));
    }
}
