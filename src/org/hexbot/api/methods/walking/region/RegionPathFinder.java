package org.hexbot.api.methods.walking.region;

import org.hexbot.api.methods.Calculations;
import org.hexbot.api.methods.GameObjects;
import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.walking.PathFinder;
import org.hexbot.api.wrapper.Tile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;


public class RegionPathFinder extends PathFinder<Tile>
{

	private Set<Tile> settledNodes = new HashSet<Tile>();
	private final Map<Tile, Double> shortestDistance = new HashMap<Tile, Double>();
	private final Map<Tile, Tile> predecessors = new HashMap<Tile, Tile>();
	private final PriorityQueue<Tile> unsettledNodes = new PriorityQueue<Tile>(10, new TileComparator());

	protected void init(Tile start) {
		settledNodes.clear();
		unsettledNodes.clear();

		shortestDistance.clear();
		predecessors.clear();

		setShortestDistance(start, 0);
		unsettledNodes.add(start);
	}

	public Tile[] findPath(Tile from, Tile to) {
		// if (!inRegion(from) || !inRegion(to))
		// return null;
		init(from);

		Tile u;

		while ((u = unsettledNodes.poll()) != null) {
			System.out.println("Testing:" + u.getX() + "," + u.getY() + " Flag: 0x" + String.format("%X", u.getFlag()));
			if (u == to)
				break;
			settledNodes.add(u);
			relaxNeighbors(u);
		}

		List<Tile> path = new ArrayList<Tile>();
		for (Tile node = to; node != null; node = getPredecessor(node)) {
			path.add(node);
			System.out.println("Path:" + node.getX() + "," + node.getY());
		}

		Collections.reverse(path);

		return path.toArray(new Tile[path.size()]);
	}

	private void relaxNeighbors(Tile u) {
		Tile[] t = getLinkedTiles(u);
		for (Tile v : t) {
			if (isSettled(v))
				continue;

			double shortDist = getShortestDistance(u) + Calculations.distanceBetween(u, v);
			if (shortDist < getShortestDistance(v)) {
				setShortestDistance(v, shortDist);
				setPredecessor(v, u);
			}

		}
	}

	@SuppressWarnings("unused")
	private boolean inRegion(Tile t) {
		int regionX = t.getX() / 64;
		int regionY = t.getY() / 64;
		Tile t1 = Players.getLocal().getLocation();
		return t1.getX() / 64 == regionX && t1.getY() / 64 == regionY;
	}

	private Tile[] getLinkedTiles(Tile u) {
		List<Tile> ret = new ArrayList<Tile>();
		ret.add(new Tile(u.getX(), u.getY() - 1));
		ret.add(new Tile(u.getX(), u.getY() + 1));
		ret.add(new Tile(u.getX() - 1, u.getY()));
		ret.add(new Tile(u.getX() - 1, u.getY() - 1));
		ret.add(new Tile(u.getX() - 1, u.getY() + 1));
		ret.add(new Tile(u.getX() + 1, u.getY()));
		ret.add(new Tile(u.getX() + 1, u.getY() - 1));
		ret.add(new Tile(u.getX() + 1, u.getY() + 1));
		for (int i = 0; i < ret.size(); i++) {
			Tile t = ret.get(i);
			if (t == null || isSettled(t) || GameObjects.getAt(t) != null)
				ret.remove(t);
		}
		return ret.toArray(new Tile[ret.size()]);
	}

	private Tile getPredecessor(Tile node) {
		return predecessors.get(node);
	}

	private void setPredecessor(Tile node1, Tile node2) {
		predecessors.put(node1, node2);
	}

	private boolean isSettled(Tile node) {
		for (Tile t : settledNodes)
			if (t.getX() == node.getX() && t.getY() == node.getY())
				return true;
		return settledNodes.contains(node);
	}

	private void setShortestDistance(Tile node, double distance) {
		unsettledNodes.remove(node);
		shortestDistance.put(node, distance);
		unsettledNodes.add(node);
	}

	public double getShortestDistance(Tile node) {
		Double d = shortestDistance.get(node);
		return d == null ? Double.MAX_VALUE : d;
	}

	@SuppressWarnings("unused")
	private Tile[] optimizePath(Tile[] path) {
		if (path.length < 2)
			return path;
		List<Tile> tiles = new ArrayList<Tile>();
		Tile lastTile = path[0];
		double lastSlope = 0;
		boolean slopeSet = false;
		for (int i = 1; i < path.length; i++) {
			Tile t = path[i];
			if (!slopeSet) {
				lastSlope = Calculations.slope(lastTile, t);
			} else if (lastSlope != Calculations.slope(lastTile, t)) {
				tiles.add(t);
				slopeSet = false;
			}
			lastTile = t;
		}
		return tiles.toArray(new Tile[tiles.size()]);
	}

	private class TileComparator implements Comparator<Tile>
	{

		@Override
		public int compare(Tile t1, Tile t2) {
			double result = Calculations.distanceTo(t1) - Calculations.distanceTo(t2);
			return (int) (result == 0d ? 0d : result);
		}

	}

}
