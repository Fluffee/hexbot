package org.hexbot.api.methods.walking.web;

import org.hexbot.api.methods.Calculations;
import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.walking.web.nodes.CommandNode;
import org.hexbot.api.wrapper.Tile;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.IOException;


public final class Web {

	protected static WebNode[] nodes;

	public static void load(){
		try {
			nodes = WebFile.read(Web.class.getResourceAsStream("/org/hexbot/api/methods/walking/web/nodes.web"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static {
		load();
	}
	

	/**
	 * Gets total nodes
	 * 
	 * @return total nodes
	 */
	public static int getTotalNodeCount() {
		return nodes.length;
	}

	/**
	 * Get closest node tile based on tile to search for
	 * 
	 * @param search
	 * @return node tile
	 */
	public static Tile getTileInWeb(Tile search) {
		return getClosestNode(search).getTile();
	}

    public static WebPath getPath(Tile to) {
        return Game.isLoggedIn() ? getPath(Players.getLocal().getLocation(), to) : null;
    }

	/**
	 * Generates a path based on start and end tile
	 * 
	 * @param from
	 * @param to
	 * @return WebPath
	 */
        public static WebPath getPath(Tile from, Tile to) {
            WebNode startNode = getClosestNode(from, to);
            WebNode endNode = getClosestNode(to);
            WebPathFinder pathFinder = new WebPathFinder();
            WebNode[] path = pathFinder.findPath(startNode, endNode);
            return new WebPath(path);
        }

	protected static WebNode getClosestNode(Tile t) {
		return getClosestNode(t, t);
	}

	protected static WebNode getClosestNode(Tile t, Tile end) {
		double dist = Integer.MAX_VALUE;
		double dist1 = Integer.MAX_VALUE;
		WebNode ret = null;
		for (WebNode node : nodes) {
			if ((dist1 = Calculations.distanceBetween(t, node.getTile())) < dist) {
				dist = dist1;
				ret = node;
			}

			if (node instanceof CommandNode) {
				if ((dist1 = Calculations.distanceBetween(node.getTile(), end)) < dist) {
					dist = dist1;
					ret = node;
				}
			}
		}
		return ret;
	}


	public static void draw(Graphics g, boolean in) {
		g.setColor(Color.RED);
		for (WebNode node : nodes) {
			Tile t = node.getTile();
			drawLinkedNodes(g, node);
			Point p = Calculations.tileToMinimap(t);
			if (p.x > 0 && p.x < 765 && p.y > 0 && p.y < 503) {
				g.fillRect(p.x - 2, p.y - 2, 4, 4);
			}
		}
	}


	public static void draw(Graphics g) {
		draw(g, false);
	}

	private static void drawLinkedNodes(Graphics g, WebNode node) {
		Integer[] links = node.getLinkedNodes();

        Point p1 = Calculations.tileToMinimap(node.getTile());
		if (p1.x > 0 && p1.x < 765 && p1.y > 0 && p1.y < 503) {
			for (int i = 0; i < links.length; i++) {
				Point p2 = Calculations.tileToMinimap(
						nodes[links[i]].getTile());
				if (p1.x == -1 || p1.y == -1 || p2.x == -1 || p2.y == -1)
					continue;
				g.drawLine(p1.x, p1.y, p2.x, p2.y);
			}
		}
	}

}
