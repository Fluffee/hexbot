package org.hexbot.api.methods.walking.web;

import org.hexbot.api.methods.walking.PathFinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;


public class WebPathFinder extends PathFinder<WebNode> {

	private Set<WebNode> settledNodes = new HashSet<>();
	private final Map<WebNode, Integer> shortestDistance = new HashMap<>();
	private final Map<WebNode, WebNode> predecessors = new HashMap<>();
	private final PriorityQueue<WebNode> unsettledNodes = new PriorityQueue<>(
			10, new WebNodeComparator());

	protected WebPathFinder() {

	}

	protected void init(WebNode start) {
		settledNodes.clear();
		unsettledNodes.clear();

		shortestDistance.clear();
		predecessors.clear();

		setShortestDistance(start, 0);
		unsettledNodes.add(start);
	}

	public WebNode[] findPath(WebNode from, WebNode to) {
		init(from);

		WebNode u;

		while ((u = unsettledNodes.poll()) != null) {

			if (u == to)
				break;
			settledNodes.add(u);
			relaxNeighbors(u);
		}

		List<WebNode> path = new ArrayList<WebNode>();
		for (WebNode node = to; node != null; node = getPredecessor(node)) {
			path.add(node);
		}

		Collections.reverse(path);

		return path.toArray(new WebNode[path.size()]);
	}

	private void relaxNeighbors(WebNode u) {
		for (int i : u.getLinkedNodes()) {
			WebNode v = Web.nodes[i];

			if (isSettled(v))
				continue;

			int shortDist = getShortestDistance(u) + u.getDistanceTo(v);
			if (shortDist < getShortestDistance(v)) {
				setShortestDistance(v, shortDist);
				setPredecessor(v, u);
			}

		}
	}

	public WebNode getPredecessor(WebNode node) {
		return predecessors.get(node);
	}

	public void setPredecessor(WebNode node1, WebNode node2) {
		predecessors.put(node1, node2);
	}

	private boolean isSettled(WebNode node) {
		return settledNodes.contains(node);
	}

	private void setShortestDistance(WebNode node, int distance) {
		unsettledNodes.remove(node);
		shortestDistance.put(node, distance);
		unsettledNodes.add(node);
	}

	public int getShortestDistance(WebNode node) {
		Integer d = shortestDistance.get(node);
		return d == null ? Integer.MAX_VALUE : d;
	}

	private class WebNodeComparator implements Comparator<WebNode> {

		@Override
		public int compare(WebNode node1, WebNode node2) {
			int result = getShortestDistance(node1)
					- getShortestDistance(node2);
			return result == 0 ? 1 : result;
		}

	}

}
