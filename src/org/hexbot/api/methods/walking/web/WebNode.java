package org.hexbot.api.methods.walking.web;

import org.hexbot.api.wrapper.Tile;

import java.util.ArrayList;


public abstract class WebNode {

	protected int x, y, plane;

	private ArrayList<Integer> linkedNodes = new ArrayList<Integer>();

	protected WebNode(int x, int y, int plane) {
		this.x = x;
		this.y = y;
		this.plane = plane;
	}

	public abstract boolean interact();

	// Override this for spells and shit to equal 0
	public int getDistanceTo(WebNode node) {
		return (int) Math.hypot(x - node.x, y - node.y);
	}

	public final Integer[] getLinkedNodes() {
		return linkedNodes.toArray(new Integer[linkedNodes.size()]);
	}

	public final int getX() {
		return x;
	}

	public final int getY() {
		return y;
	}

	public final int getPlane() {
		return plane;
	}

	public final Tile getTile() {
		return new Tile(x, y, plane);
	}

	public final void addLinkedNode(Integer i) {
		linkedNodes.add(i);
	}

	public final void removeLinkedNode(Integer i) {
		linkedNodes.remove((Object) i);
	}

}
