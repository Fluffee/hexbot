package org.hexbot.api.methods.walking.web;


import org.hexbot.api.methods.walking.Path;

public class WebPath extends Path {

	WebNode[] path;

	protected WebPath(WebNode[] nodes) {
		super(nodes);
		path = nodes;
	}

	/**
	 * Gets total nodes of this path
	 * @return total nodes length of this path
	 */
	public int getTotalNodes() {
		return path.length;
	}

	/**
	 * Calculates distance of path
	 * @return distance of path
	 */
	public int getTotalDistance() {
		int dist = 0;
		for (int i = 1; i < path.length; i++)
			dist += path[i].getDistanceTo(path[i - 1]);
		return dist;
	}

}
