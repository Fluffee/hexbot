package org.hexbot.api.methods.walking.web;

import org.hexbot.api.methods.walking.web.nodes.TileNode;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * 
 * @author Matthew
 */
public class WebFile {

	public static void save(File outFile, WebNode[] nodes) throws IOException {
		FileOutputStream fout = new FileOutputStream(outFile);
		try (DataOutputStream out = new DataOutputStream(fout)) {
			out.write("WEB".getBytes()); // Header
			out.writeShort(1); // File Version
			out.writeInt(nodes.length); // Number of Nodes in list
			for (int i = 0; i < nodes.length; i++) {
				WebNode node = nodes[i];
				if ((node instanceof TileNode)) {
					out.writeByte(0); // NodeType
					out.writeShort(node.getX());
					out.writeShort(node.getY());
					out.writeByte(node.getPlane());
					Integer[] links = node.getLinkedNodes();
					out.writeByte(links.length);
					for (int j = 0; j < links.length; j++) {
						out.writeInt(links[j]);
					}
				} else {
					throw new IOException("Unsupported Node type:"
							+ node.getClass());
				}
			}
		}
	}

	public static WebNode[] read(File in) throws IOException {
		return read(new FileInputStream(in));
	}

	public static WebNode[] read(InputStream inStream) throws IOException {
		try (DataInputStream in = new DataInputStream(inStream)) {
			byte[] header = new byte[3];
			in.read(header, 0, 3);
			if (!new String(header).equals("WEB")) {
				throw new IOException("Invalid web header!");
			}
			short version = in.readShort();
			if (version > 1) {
				in.close();
				throw new IOException("Unsupported version!");
			}
			int nodeCount = in.readInt();
			WebNode[] nodes = new WebNode[nodeCount];
			for (int i = 0; i < nodeCount; i++) {
				int type = in.readByte();
				switch (type) {
				case 0: // Tile
					int x = in.readShort();
					int y = in.readShort();
					int plane = in.readByte();
					nodes[i] = new TileNode(x, y, plane);
					int linkCount = in.readByte();
					for (int j = 0; j < linkCount; j++) {
						nodes[i].addLinkedNode((int) in.readInt());
					}
					break;
				default:
					in.close();
					throw new IOException("Unsupported node type:" + type);
				}
			}
			return nodes;
		}
	}
}
