package org.hexbot.api.methods.walking.web.nodes;


import org.hexbot.api.methods.walking.web.WebNode;

public class CommandNode extends WebNode {
	
	//private String command;

	public CommandNode(int x, int y, int plane,String command) {
		super(x, y, plane); // the coords of where you'd end up after command
		//this.command = command;
	}
	
	public int getDistanceTo(WebNode node){
		return 0; // no walking needed
	}

	@Override
	public boolean interact() {
		/*Keyboard.sendKeys("::");
		Keyboard.sendKeys(command);
		Keyboard.clickKey(KeyEvent.VK_ENTER);*/
		return true;
	}

}
