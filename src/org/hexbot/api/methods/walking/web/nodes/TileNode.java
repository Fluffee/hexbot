package org.hexbot.api.methods.walking.web.nodes;


import org.hexbot.api.methods.walking.web.WebNode;
import org.hexbot.api.wrapper.Tile;

public class TileNode extends WebNode
{

	public TileNode(int x, int y, int plane)
	{
		super(x, y, plane);
	}

	@Override
	public boolean interact() {
		Tile tile = getTile();
		if (tile.isOnScreen()) {
			//Point p = Calculations.tileToScreen(tile);
			//Mouse.click(p.x, p.y, false);
		} else if (tile.isOnMap()) {
			//Point p = Calculations.tileToMinimap(tile);
			//Mouse.click(p.x, p.y, false);
		}
		return false;
	}

}
