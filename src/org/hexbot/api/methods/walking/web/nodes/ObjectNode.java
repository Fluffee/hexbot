package org.hexbot.api.methods.walking.web.nodes;


import org.hexbot.api.methods.walking.web.WebNode;

public class ObjectNode extends WebNode {

	//private int id;
	//private String action;

	public ObjectNode(int x, int y, int plane, int id, String action) {
		super(x, y, plane);
		//this.id = id;
		//this.action = action;
	}

	/**
	 * Interacts the object
	 * @return <b>true</b> if interacted successfully
	 */
	@Override
	public boolean interact() {
		/*if (!getTile().isOnScreen())
			return false;
		GameObject[] objects = GameObjects.getObjects(getTile(), 1);
		if (objects == null || objects.length == 0)
			return false;
		for (GameObject object : objects) {
			if (object == null)
				continue;
			if (object.getID() == id) {
				object.interact(action);
			}
		}*/
		return false;
	}

}
