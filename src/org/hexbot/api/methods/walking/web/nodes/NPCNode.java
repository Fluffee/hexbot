package org.hexbot.api.methods.walking.web.nodes;


import org.hexbot.api.methods.walking.web.WebNode;

public class NPCNode extends WebNode {
	
	private int id;
	private String action;

	public NPCNode(int x, int y, int plane,int id,String action) {
		super(x, y, plane);
		this.id = id;
		this.action = action;
	}

	@Override
	public boolean interact() {
		/*if(!getTile().isOnScreen())
			return false;
		NPC[] npcs = NPCs.getNPCs();
		for(NPC npc: npcs){
			if(npc.getLocation().equals(getTile()) && npc.getDef().getID() == id){
				Point p = npc.getLocationOnScreen();
				Mouse.move(p.x, p.y);
				Menu.interact(action);
				return true;
			}
		}*/
		return false;
	}

}
