package org.hexbot.api.methods.walking;

import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.Walking;
import org.hexbot.api.methods.walking.web.WebNode;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Tile;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;


/**
 * @author Parameter
 */
public class Path {
    private Tile[] tileArray = null;

    public Path(Tile[] arrayOfTiles) {
        this.tileArray = arrayOfTiles;
    }

    protected Path(WebNode[] path) {
        Tile[] tiles = new Tile[path.length];
        for (int i = 0; i < path.length; i++) {
            tiles[i] = path[i].getTile();
        }
        this.tileArray = tiles;
    }

    /**
     * Gets tile array of this path
     *
     * @return array of tiles
     */
    public Tile[] getPath() {
        return tileArray;
    }

    private static Point worldToMinimap(Tile t) {
        Tile player = Players.getLocal().getLocation();
        Point a = player.getMinimapLocation();
        Point b = new Tile(player.getX(), player.getY() + 16).getMinimapLocation();
        Point c = new Tile(player.getX() + 16, player.getY()).getMinimapLocation();

        double bDistX = (b.x - a.x) / 16.0;
        double bDistY = (b.y - a.y) / 16.0;

        double cDistX = (c.x - a.x) / 16.0;
        double cDistY = (c.y - a.y) / 16.0;

        int xDist = t.getX() - player.getX();
        int yDist = t.getY() - player.getY();

        return new Point(a.x
                + (int) Math.round(cDistX * xDist + bDistX * yDist), a.y
                + (int) Math.round(bDistY * yDist + cDistY * xDist));
    }

    /**
     * Walks down the path
     */
    public void traverse() {
        Tile next = getNextTile();
        if (next != null) {
            Walking.walkTileMM(next);
            Time.sleep(700);
        }
    }

    /**
     * Gets next tile to walk to
     *
     * @return tile
     */
    public Tile getNextTile() {
        for (int x = tileArray.length - 1; x >= 0; x--) {
            if (tileArray[x] != null && tileArray[x].isOnMap()) {
                return tileArray[x];
            }
        }
        return null;
    }

    /**
     * Determines if this path can be walked down
     *
     * @return <b>true</b> if path is valid
     */
    public boolean isValid() {
        return getNextTile() != null;
    }


    public void draw(Graphics g) {
        for (int x = 0; x < tileArray.length - 1; x++) {
            Point p = worldToMinimap(tileArray[x]);
            g.setColor(Color.red);
            g.fillRect(p.x - 2, p.y - 2, 4, 4);
            Point p1 = worldToMinimap(tileArray[x + 1]);
            g.setColor(Color.red);
            g.fillRect(p1.x - 2, p1.y - 2, 4, 4);
            g.drawLine(p.x, p.y, p1.x, p1.y);
        }
    }

    /**
     * Determines if player has reached end of path
     *
     * @return <b>true</b> if player has reached end of path
     */
    public boolean hasReached() {
        return tileArray[tileArray.length - 1].getDistance() < 5;
    }

    /**
     * Reverses the current path
     *
     * @return The reversed path
     */
    public Path reverse() {
        Tile[] tiles = new Tile[tileArray.length];
        for (int i = 0; i < tiles.length; i++)
            tiles[i] = tileArray[tileArray.length - i - 1];
        return new Path(tiles);
    }

}
