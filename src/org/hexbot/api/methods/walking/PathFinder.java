package org.hexbot.api.methods.walking;

public abstract class PathFinder<N> {
	
	public abstract N[] findPath(N start,N end);

}
