package org.hexbot.api.methods.walking;

import org.hexbot.api.wrapper.Tile;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 6/7/13
 * Time: 4:38 PM
 */
public enum Location {

    LUMBRIDGE(null);

    private final Tile tile;

    private Location(final Tile tile) {
        this.tile = tile;
    }
}
