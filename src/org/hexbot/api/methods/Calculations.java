package org.hexbot.api.methods;

import org.hexbot.api.wrapper.Locatable;
import org.hexbot.api.wrapper.Tile;

import java.awt.*;

public class Calculations {

    public static final int[] SIN_TABLE = new int[2048];
    public static final int[] COS_TABLE = new int[2048];
    public static final Rectangle GAME_SCREEN;

    static {
        for (int i = 0; i < 2048; ++i) {
            SIN_TABLE[i] = (int) (65536.0D * Math.sin(i * 0.0030679615D));
            COS_TABLE[i] = (int) (65536.0D * Math.cos(i * 0.0030679615D));
        }
        GAME_SCREEN = new Rectangle(0, 0, 765, 503);
    }

    public static Rectangle getGameScreen() {
        return GAME_SCREEN;
    }

    public static boolean onScreen(Point p) {
        return GAME_SCREEN.contains(p);
    }

    public static boolean onGameScreen(Point p) {
        return new Rectangle(0,0, 516, 337).contains(p);
    }

    public static Point tileToScreen(int x, int y, double dX, double dY, int height) {
        return worldToScreen((x - Game.getBaseX() + dX) * 128.0D, (y - Game.getBaseY() + dY) * 128.0D, height);
    }

    public static Point tileToScreen(Tile t, double dX, double dY, int height) {
        return tileToScreen(t.getX(), t.getY(), dX, dY, height);
    }

    public static Point tileToScreen(Tile t, int h) {
        return tileToScreen(t.getX(), t.getY(), h);
    }

    public static Point tileToScreen(Tile t) {
        return tileToScreen(t.getX(), t.getY(), t.getPlane());
    }

    public static Point tileToScreen(int x, int y, int height) {
        return tileToScreen(x, y, 0.5D, 0.5D, height);
    }

    public static Point worldToScreen(double X, double Y, int height) {
      //  X += 5D;
       // Y -= 11D;
        try {
            if ((X < 128.0D) || (Y < 128.0D) || (X > 13056.0D) || (Y > 13056.0D)) {
                return new Point(-1, -1);
            }
            int Z = tileHeight((int) X, (int) Y) - height;
            X -= Game.getCameraX();
            Z -= Game.getCameraZ();
            int curvexsin = SIN_TABLE[Game.getYaw()];
            int curvexcos = COS_TABLE[Game.getYaw()];
            Y -= Game.getCameraY();
            int curveysin = SIN_TABLE[Game.getPitch()];
            int curveycos = COS_TABLE[Game.getPitch()];
            int calculation = curvexsin * (int) Y + (int) X * curvexcos >> 16;
            Y = -(curvexsin * (int) X) + (int) Y * curvexcos >> 16;
            X = calculation;
            calculation = curveycos * Z - curveysin * (int) Y >> 16;
            Y = curveysin * Z + (int) Y * curveycos >> 16;
            Z = calculation;
            int ScreenX = ((int) X << 9) / (int) Y + 256;
            int ScreenY = (Z << 9) / (int) Y + 167;

            return new Point(ScreenX, ScreenY);
        } catch (Exception e) {
        }
        return new Point(-1, -1);
    }

    public static int tileHeight(final Tile t) {
        return tileHeight(t.getX(), t.getY());
    }

    public static int tileHeight(int x, int y) {
        int[][][] ground = Game.getTileHeights();
        int zidx = Game.getPlane();
        int x1 = x >> 7;
        int y1 = y >> 7;
        int x2 = x & 0x7F;
        int y2 = y & 0x7F;
        if ((x1 < 0) || (y1 < 0) || (x1 > 103) || (y1 > 103)) {
            return 0;
        }
        if ((zidx < 3) && ((0x2 & Game.getTileBytes()[1][x1][y1]) == 2)) {
            ++zidx;
        }
        int i = ground[zidx][(x1 + 1)][y1] * x2 + ground[zidx][x1][y1] * (128 - x2) >> 7;
        int j = ground[zidx][(x1 + 1)][(y1 + 1)] * x2 + ground[zidx][x1][(y1 + 1)] * (128 - x2) >> 7;
        return j * y2 + (128 - y2) * i >> 7;
    }


    public static int angleToTile(final Tile t) {
        final Tile me = Players.getLocal().getLocation();
        final int angle = (int) Math.toDegrees(Math.atan2(t.getY() - me.getY(), t.getX() - me.getX()));
        return angle >= 0 ? angle : 360 + angle;
    }

    /**
     * Calculates the distance between two points.
     *
     * @param curr The first point.
     * @param dest The second point.
     * @return The distance between the two points, using the distance formula.
     * @see #distanceBetween(Tile, Tile)
     */
    public static double distanceBetween(final Point curr, final Point dest) {
        return Math.sqrt((curr.x - dest.x) * (curr.x - dest.x) + (curr.y - dest.y) * (curr.y - dest.y));
    }

    /**
     * Returns the diagonal distance (hypot) between two Tiles.
     *
     * @param curr The starting tile.
     * @param dest The destination tile.
     * @return The diagonal distance between the two <code>Tile</code>s.
     * @see #distanceBetween(java.awt.Point, java.awt.Point)
     */
    public static double distanceBetween(final Tile curr, final Tile dest) {
        if (curr == null || dest == null) {
            return Double.MAX_VALUE;
        }
        return Math.sqrt(Math.pow(curr.getX() - dest.getX(), 2) + Math.pow(curr.getY() - dest.getY(), 2));
    }

    /**
     * Returns the diagonal distance to a given Tile.
     *
     * @param t The destination tile.
     * @return Distance to <code>Tile</code>.
     */
    public static int distanceTo(Tile t) {
        return t == null ? -1 : (int) distanceBetween(Players.getLocal().getLocation(), t);
    }

    /**
     * Returns the diagonal distance to a given Tile.
     *
     * @param t The destination tile.
     * @return Distance to <code>Tile</code>.
     */
    public static int distanceTo(Locatable t) {
        return t == null ? -1 : (int) distanceBetween(Players.getLocal().getLocation(), t.getLocation());
    }

    /**
     * @param x location x
     * @param y locations y
     * @return point on minimap
     * @author Swipe
     */
    public static Point worldToMap(int x, int y) {
        int X = Players.getLocal().getLocation().getX();
        int Y = Players.getLocal().getLocation().getY();
        int delta_x = (x - X);
        int delta_y = (Y - y);
        //get degrees
        double angle = Camera.getAngle();
        angle = Math.toRadians(angle);
        Point center = new Point(643, 83);
        float px = (float) (Math.cos(angle) * (delta_x) - Math.sin(angle) * (delta_y));
        float py = (float) (Math.sin(angle) * (delta_x) + Math.cos(angle) * (delta_y));
        px *= 4.0;
        py *= 4.0;
        return new Point(Math.round(px) + center.x, Math.round(py) + center.y);
    }

    public static Point tileToMinimap(Tile t) {
        return worldToMap(t.getX(), t.getY());
    }

    public static double slope(Tile tile, Tile tile2) {
        double rise = tile.getX() - tile2.getY();
        if (rise < 0)
            rise = -rise;
        double run = tile.getY() - tile2.getY();
        if (run < 0)
            run = -run;
        return rise / run;
    }

}