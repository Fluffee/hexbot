package org.hexbot.api.methods;

import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.Tab;

import java.util.LinkedList;
import java.util.List;

public class Prayer {
    public static final int INTERFACE_PRAYER = 271;
    public static final int INTERFACE_PRAYER_ORB = 749;


    public interface Book {

        public int getComponentIndex();
        public int getSettings();
        public int getRequiredLevel();

    }

    public enum Normal implements Book {
        THICK_SKIN(0, 0x1, 1),
        BURST_OF_STRENGTH(1, 0x2, 4),
        CLARITY_OF_THOUGHT(2, 0x4, 7),
        SHARP_EYE(3, 0x40000, 8),
        MYSTIC_WILL(4, 0x80000, 9),
        ROCK_SKIN(5, 0x8, 10),
        SUPERHUMAN_STRENGTH(6, 0x10, 13),
        IMPROVED_REFLEXES(7, 0x20, 16),
        RAPID_RESTORE(8, 0x40, 19),
        RAPID_HEAL(9, 0x80, 22),
        PROTECT_ITEM_REGULAR(10, 0x100, 25),
        HAWK_EYE(11, 0x100000, 26),
        MYSTIC_LORE(12, 0x200000, 27),
        STEEL_SKIN(13, 0x200, 28),
        ULTIMATE_STRENGTH(14, 0x400, 31),
        INCREDIBLE_REFLEXES(15, 0x800, 34),
        PROTECT_FROM_SUMMONING(16, 0x1000000, 35),
        PROTECT_FROM_MAGIC(17, 0x1000, 37),
        PROTECT_FROM_MISSILES(18, 0x2000, 40),
        PROTECT_FROM_MELEE(19, 0x4000, 43),
        EAGLE_EYE(20, 0x400000, 44),
        MYSTIC_MIGHT(21, 0x800000, 45),
        RETRIBUTION(22, 0x8000, 46),
        REDEMPTION(23, 0x10000, 49),
        SMITE(24, 0x20000, 52),
        CHIVALRY(25, 0x2000000, 60),
        RAPID_RENEWAL(26, 0x8000000, 65),
        PIETY(27, 0x4000000, 70),
        RIGOUR(28, 0x10000000, 74),
        AUGURY(29, 0x20000000, 77);

        int comp, setting, level;

        private Normal(int comp, int setting, int level) {
            this.comp = comp;
            this.setting = setting;
            this.level = level;
        }

        public int getComponentIndex() {
            return comp;
        }

        public int getSettings() {
            return setting;
        }

        public int getRequiredLevel() {
            return level;
        }
    }

    public Book[] getSelectedPrayers() {
        final int bookSetting = 1395;
        final List<Book> activePrayers = new LinkedList<Book>();
        for (Book prayer : Normal.values()) {
            if ((Settings.get(bookSetting) & (prayer.getSettings())) == prayer.getSettings()) {
                activePrayers.add(prayer);
            }
        }
        return activePrayers.toArray(new Book[activePrayers.size()]);
    }

    public boolean isPrayerOn(final Book prayer) {
        for (Book pray : getSelectedPrayers()) {
            if (pray == prayer) {
                return true;
            }
        }
        return false;
    }

    private boolean isQuickPrayerSet(Book... prayers) {
        for (Book effect : prayers) {
            if (!isQuickPrayerSet(effect)) {
                return false;
            }
        }
        return true;
    }


    public int getPrayerLeft() {
        return Integer.parseInt(Widgets.getChild(INTERFACE_PRAYER_ORB, 4).getText());
    }

    public int getPrayerPercentLeft() {
        return 100 * getPrayerLeft() / Skills.PRAYER.getCurrentLevel();
    }

    public boolean setPrayer(Book pray, boolean active) {
        if (isPrayerOn(pray) == active) {
            return true;
        } else {
            if (Skills.PRAYER.getRealLevel() < pray.getRequiredLevel()) {
                return false;
            }
            Tab.open(Tab.PRAYER);
            if (Tab.isOpen(Tab.PRAYER)) {
                Component component = Widgets.getChild(INTERFACE_PRAYER, 7).getChildren()[pray.getComponentIndex()];
                if (component.isValid()) {
                    component.interact(active ? "Activate" : "Deactivate");
                }
            }
        }
        return isPrayerOn(pray) == active;
    }


    public boolean deactivateAll() {
        for (Book pray : getSelectedPrayers()) {
            setPrayer(pray, false);
        }
        return getSelectedPrayers().length < 1;
    }
}
