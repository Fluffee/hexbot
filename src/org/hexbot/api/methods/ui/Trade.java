package org.hexbot.api.methods.ui;

import org.hexbot.api.methods.Widgets;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.Item;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 8/22/13
 * Time: 4:22 PM
 * Copyright under GPL liscense by author.
 */
public class Trade {
    public final static int PARENT = 335;
    public final static int ALT_PARENT = 334;
    public final static int FIRST_CLOSE = 7;
    public final static int SECOND_CLOSE = 6;
    public final static int MY_ITEMS = 48;
    public final static int OTHER_ITEMS = 50;

    public static boolean isOpen() {
        return getWindowState() > 0;
    }

    public static int getWindowState() {
        Component t1 = Widgets.getChild(PARENT, FIRST_CLOSE);
        Component t2 = Widgets.getChild(ALT_PARENT, SECOND_CLOSE);
        if (t1 != null && t1.isOnScreen())
            return 1;
        if (t2 != null && t2.isOnScreen())
            return 2;
        return -1;
    }

    public static String getTrader() {
        for (Component c : Widgets.getLoaded()[335].getChildren()) {
            if (c != null && c.getText() != null && c.getText().contains("Trad")) {
                return c.getText().replace("Trading With:", "");
            }
        }
        return null;
    }

    public static void acceptFirst() {
        Component trade = Widgets.getChild(PARENT, 17);
        if (trade == null)
            return;
        trade.click();
    }

    public static void declineFirst() {
        Component trade = Widgets.getChild(PARENT, 18);
        if (trade == null)
            return;
        trade.click();
    }

    public static void acceptSecond() {
        Component trade = Widgets.getChild(ALT_PARENT, 20);
        if (trade == null)
            return;
        trade.click();
    }

    public static void declineSecond() {
        Component trade = Widgets.getChild(ALT_PARENT, 21);
        if (trade == null)
            return;
        trade.click();
    }

    public static TradeItem[] getTraderItems() {
        Component other = Widgets.getChild(PARENT, OTHER_ITEMS);
        if (other == null || other.getChildren() == null)
            return null;
        ArrayList<Item> items = new ArrayList<>();
        for (Component child : other.getChildren()) {
            items.add(new TradeItem(child.getTradeId(), child.getTradeStack(), child));
        }
        return items.toArray(new TradeItem[items.size()]);
    }

    public static TradeItem[] getMyItems() {
        Component other = Widgets.getChild(PARENT, MY_ITEMS);
        if (other == null || other.getSlotIds() == null)
            return null;
        ArrayList<TradeItem> items = new ArrayList<>();
        for (Component child : other.getChildren()) {
            items.add(new TradeItem(child.getTradeId(), child.getTradeStack(), child));
        }
        return items.toArray(new TradeItem[items.size()]);
    }

    public static class TradeItem extends Item {
        Component comp;

        public TradeItem(int id, int stackSize, Component c) {
            super(id, stackSize);
            comp = c;
        }

        public void click() {
            comp.click();
        }

        public void interact(String str) {
            comp.interact(str);
        }

        public Component getComponent() {
            return comp;
        }
    }
}
