package org.hexbot.api.methods.ui;

import org.hexbot.api.input.Keyboard;
import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Inventory;
import org.hexbot.api.methods.Widgets;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.InventoryItem;
import org.hexbot.api.wrapper.Item;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 8/22/13
 * Time: 5:26 PM
 * Copyright under GPL liscense by author.
 */
public class Shop {

    public static boolean isOpen() {
        return Widgets.getChild(300, 75) != null && Widgets.getChild(300, 75).isOnScreen();
    }

    public static void close() {
        Component close = Widgets.getChild(300, 91);
        if (close == null)
            return;
        close.click();
    }

    public static String getShopName() {
        if (isOpen())
            return Widgets.getChild(300, 76).getText();
        return null;
    }

    public static ShopItem getItem(int id) {
        for (ShopItem shopItem : getItems()) {
            if (shopItem.getId() == id)
                return shopItem;
        }
        return null;
    }

    public static ShopItem getItem(String name) {
        for (ShopItem shopItem : getItems()) {
            if (shopItem.getName().equals(name))
                return shopItem;
        }
        return null;
    }

    public static int getStackSize(ShopItem item){
            return item.getStackSize();
    }

    public static int getStackSize(int id){
        ShopItem item = getItem(id);
        return item.getStackSize();
    }

    public static int getStackSize(String name){
        ShopItem item = getItem(name);
        return item.getStackSize();
    }

    public static void sellItem(int id, int amount) {
        if (isOpen()) {
            InventoryItem item = Inventory.getItem(id);
            if (item != null) {
                switch (amount) {
                    case 1:
                        item.interact("Sell 1");
                        break;
                    case 5:
                        item.interact("Sell 5");
                        break;
                    case 10:
                        item.interact("Sell 10");
                        break;
                    default:
                        if (amount < item.getStackSize() && amount != 0) {
                            item.interact("Sell X");
                            Time.sleep(800, 1000);
                            Keyboard.sendKeys(new StringBuilder().insert(0, "").append(amount).toString());
                        } else {
                            item.interact("Sell All");
                        }
                }
            }
        }
    }


    public static void sellItem(String name, int amount) {
        if (isOpen()) {
            InventoryItem item = Inventory.getItem(name);
            if (item != null) {
                switch (amount) {
                    case 1:
                        item.interact("Sell 1");
                        break;
                    case 5:
                        item.interact("Sell 5");
                        break;
                    case 10:
                        item.interact("Sell 10");
                        break;
                    default:
                        if (amount < item.getStackSize() && amount != 0) {
                            item.interact("Sell X");
                            Time.sleep(800, 1000);
                            Keyboard.sendKeys(new StringBuilder().insert(0, "").append(amount).toString());
                        } else {
                            item.interact("Sell All");
                        }
                }
            }
        }
    }

    public static void buyItem(int id, int amount) {
        if (isOpen()) {
            ShopItem item = getItem(id);
            if (item != null) {
                switch (amount) {
                    case 1:
                        item.interact("Buy 1");
                        break;
                    case 5:
                        item.interact("Buy 5");
                        break;
                    case 10:
                        item.interact("Buy 10");
                        break;
                    default:
                        if (amount < item.getStackSize() && amount != 0) {
                            item.interact("Buy X");
                            Time.sleep(800, 1000);
                            Keyboard.sendKeys(new StringBuilder().insert(0, "").append(amount).toString());
                        } else {
                            item.interact("Buy All");
                        }
                }
            }
        }
    }

    public static void buyItem(String name, int amount) {
        if (isOpen()) {
            ShopItem item = getItem(name);
            if (item != null) {
                switch (amount) {
                    case 1:
                        item.interact("Buy 1");
                        break;
                    case 5:
                        item.interact("Buy 5");
                        break;
                    case 10:
                        item.interact("Buy 10");
                        break;
                    default:
                        if (amount < item.getStackSize() && amount != 0) {
                            item.interact("Buy X");
                            Time.sleep(800, 1000);
                            Keyboard.sendKeys(new StringBuilder().insert(0, "").append(amount).toString());
                        } else {
                            item.interact("Buy All");
                        }
                }
            }
        }
    }

    public static ShopItem[] getItems() {
        final ArrayList<ShopItem> items = new ArrayList<ShopItem>();
        if (!Game.isLoggedIn()) {
            return null;
        }
        final Component[] temp = Widgets.getChildren(300);
        if (temp == null || temp.length < 0)
            return null;
        final Component itemContainer = temp[75];
        if (itemContainer != null) {
            final int[] ids = itemContainer.getSlotIds();
            final int[] stacks = itemContainer.getSlotSizes();
            if (ids != null && stacks != null) {
                for (byte i = 0; i < ids.length; i++) {
                    if (ids[i] > 0) {
                        items.add(new ShopItem(ids[i], stacks[i], i));
                    }
                }
            }
        }
        return items.toArray(new ShopItem[items.size()]);
    }

    public static class ShopItem extends Item {
        int index;

        public ShopItem(int id, int stackSize, int index) {
            super(id, stackSize);
            this.index = index;
        }

        /**
         * Its not exact yet
         * @return
         */
        public Point getPoint() {
            int col = (index % 8);
            int row = (index / 8);
            int x = 90 + (col * 47);
            int y = 80 + (row * 41);
            return new Point(x, y);
        }

        public void click() {
            Mouse.move(getPoint());
            Mouse.click(true);
        }

        public boolean interact(String str) {
            Mouse.move(getPoint());
            Mouse.click(false);
            while (!org.hexbot.api.methods.Menu.isMenuOpen())
                Time.sleep(20);
            return org.hexbot.api.methods.Menu.click(str);
        }

    }
}
