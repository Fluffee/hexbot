package org.hexbot.api.methods;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.util.Filter;
import org.hexbot.api.util.Random;
import org.hexbot.api.util.Time;
import org.hexbot.api.util.Timer;
import org.hexbot.api.wrapper.InventoryItem;
import org.hexbot.api.wrapper.Npc;
import org.hexbot.api.wrapper.Tab;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 8/24/13
 * Time: 2:06 AM
 * Copyright under GPL liscense by author.
 */
public class Combat {

	public static Npc getPassiveNpc(final int id) {
		Npc npc = Npcs.getNearest(new Filter<Npc>() {
			@Override
			public boolean accept(Npc npc) {
				return npc.getId() == id && npc.getInteracting() == null;  //To change body of implemented methods use File | Settings | File Templates.
			}
		});
		return npc;
	}

	public static Npc getPassiveNpc(final String name) {
		Npc npc = Npcs.getNearest(new Filter<Npc>() {
			@Override
			public boolean accept(Npc npc) {
				return npc.getName() != null && npc.getName().equalsIgnoreCase(name) && npc.getInteracting() == null;  //To change body of implemented methods use File | Settings | File Templates.
			}
		});
		return npc;
	}

	public static boolean attackPassiveNpc(final String name) {
		Timer t = new Timer(Random.nextInt(2500, 4000));
		Npc npc = getPassiveNpc(name);
		if (npc != null && npc.isOnScreen()) {
			npc.interact("Attack");
			while (Players.getLocal().getInteracting() == null && t.isRunning())
				Time.sleep(30);
		}
		return (Players.getLocal().getInteracting() != null);
	}

	public static boolean attackPassiveNpc(final int id) {
		Timer t = new Timer(Random.nextInt(2500, 4000));
		Npc npc = getPassiveNpc(id);
		if (npc != null && npc.isOnScreen()) {
			npc.interact("Attack");
			while (Players.getLocal().getInteracting() == null && t.isRunning())
				Time.sleep(30);
		}
		return (Players.getLocal().getInteracting() != null);
	}

	public static void hoverNextNpc(int id) {
		Npc npc = getPassiveNpc(id);
		Mouse.move(npc.getScreenLocation());
	}

	public static void hoverNextNpc(String name) {
		Npc npc = getPassiveNpc(name);
		Mouse.move(npc.getScreenLocation());
	}

	public int getSpecialAmount() {
		return Game.getSpecialAttackEnergy();
	}

	public static void performSpecialAttack() {
		if (!Tab.isOpen(Tab.COMBAT)) {
			Tab.open(Tab.COMBAT);
		}
		Mouse.click(Random.nextInt(558, 705), Random.nextInt(410, 425));
		Time.sleep(600, 1000);
	}

	public static String getWeaponName() {
		org.hexbot.api.wrapper.Component[] PARENT = {Widgets.getChild(90, 0), Widgets.getChild(76, 0), Widgets.getChild(77, 0), Widgets.getChild(81, 0)};
		for (final org.hexbot.api.wrapper.Component comp : PARENT) {
			if (comp != null) {
				return comp.getText();
			}
		}
		return null;
	}


	public static void switchWeapon(int id) {
		if (!Tab.isOpen(Tab.INVENTORY)) {
			Tab.open(Tab.INVENTORY);
		}
		InventoryItem item = Inventory.getItem(id);
		if (item != null) {
			item.click();
			Time.sleep(500, 800);
		}
	}

	public static void switchWeapon(String name) {
		if (!Tab.isOpen(Tab.INVENTORY)) {
			Tab.open(Tab.INVENTORY);
		}
		InventoryItem item = Inventory.getItem(name);
		if (item != null) {
			item.click();
			Time.sleep(500, 800);
		}
	}

	public static void toggleAutoRetaliate(boolean on) {
		if (on && !isAutoRetaliateOn()) {
			if (Tab.isOpen(Tab.COMBAT)) {
				Mouse.click(new Point(Random.nextInt(573, 707), Random.nextInt(363, 393)));
				Time.sleep(1000, 1500);
			} else {
				Tab.open(Tab.COMBAT);
			}
		} else if (!on && isAutoRetaliateOn()) {
			if (Tab.isOpen(Tab.COMBAT)) {
				Mouse.click(new Point(Random.nextInt(573, 707), Random.nextInt(363, 393)));
				Time.sleep(1000, 1500);
			} else {
				Tab.open(Tab.COMBAT);
			}
		}
	}

  public static boolean getInteractingIsDead(){
      for(Npc n : Npcs.getLoaded()){
          if(n != null && n.getInteracting() != null && n.getInteracting().equals(Players.getLocal()) && n.getHp() == 0){
             return true;
          }
      }
      return false;
  }


	/**
	 * @author Bradsta (from here to bottom)
	 */
	public enum Stance {

		ACCURATE,
		AGGRESSIVE,
		CONTROLLED,
		DEFENSIVE;
	}

	public static Stance getCombatStance() {
		switch (Game.getSettings()[43]) {
			case 0:
				return Stance.ACCURATE;
			case 1:
				return Stance.AGGRESSIVE;
			case 2:
				return Stance.CONTROLLED;
			case 3:
				return Stance.DEFENSIVE;
		}
		return null;
	}

	public static void setCombatStance(Stance stance) {                 //ugly AF, gonna get the correct data soon.
		switch (stance) {
			case ACCURATE:
				Mouse.click(new Point(Random.nextInt(571, 631), Random.nextInt(253,
						290)));
				break;
			case AGGRESSIVE:
				Mouse.click(new Point(Random.nextInt(654, 716), Random.nextInt(253,
						290)));
				break;
			case CONTROLLED:
				Mouse.click(new Point(Random.nextInt(571, 631), Random.nextInt(308,
						340)));
				break;
			case DEFENSIVE:
				Mouse.click(new Point(Random.nextInt(654, 716), Random.nextInt(308,
						340)));
				break;
		}
	}

	public static boolean isAutoRetaliateOn() {
		return Settings.get(172) == 0;
	}
}
