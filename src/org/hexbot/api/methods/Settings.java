package org.hexbot.api.methods;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 5-5-13
 * Time: 14:31
 * To change this template use File | Settings | File Templates.
 */
public class Settings {

    public static int get(int setting) {
        if(getAll().length > setting) {
            return getAll()[setting];
        }
        return -1;
    }

    public static int[] getAll() {
        return Game.getSettings();
    }
}
