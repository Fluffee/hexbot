package org.hexbot.api.methods;


import org.hexbot.api.input.Keyboard;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Locatable;

import java.awt.event.KeyEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/28/13
 * Time: 6:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class Camera {
    public static int getAngle() {
        double mapAngle = Game.getYaw();
        mapAngle /= 2048;
        mapAngle *= 360;
        return (int) mapAngle;
    }

    public static int getPitch() {
        return Game.getPitch();
    }

    public static void setPitch(boolean maxAltitude) {
        char key = (char) (maxAltitude ? KeyEvent.VK_UP : KeyEvent.VK_DOWN);
        Keyboard.pressKey(key);
        Time.sleep(1200, 1400);
        Keyboard.releaseKey(key);
    }

    /**
     * Rotates the camera to a specific angle in the closest direction.
     *
     * @param degrees The angle to rotate to.
     */
    public static void setCameraRotation(int degrees) {
        char left = 37;
        char right = 39;
        char whichDir = left;
        int start = getAngle();

		/* Some of this shit could be simplified, but it's easier to wrap my mind around it this way*/
        if (start < 180)
            start += 360;
        if (degrees < 180)
            degrees += 360;

        if (degrees > start) {
            if (degrees - 180 < start)
                whichDir = right;
        } else if (start > degrees) {
            if (start - 180 >= degrees)
                whichDir = right;
        }
        degrees %= 360;

        Keyboard.pressKey(whichDir);
        int timeWaited = 0;

        while (getAngle() > degrees + 5 || getAngle() < degrees - 5) {
            Time.sleep(10);
            timeWaited += 10;
            if (timeWaited > 500) {
                int time = timeWaited - 500;

                if (time == 0) {
                    Keyboard.pressKey(whichDir);
                } else if (time % 40 == 0) {
                    Keyboard.pressKey(whichDir);
                }
            }
        }
        Keyboard.releaseKey(whichDir);
    }

    public static void turnTo(Locatable loc) {
        int angle = getAngleToCoordinates(loc.getLocation().getX(), loc.getLocation().getY());
        setCameraRotation(angle);
    }

    public static int getAngleToCoordinates(int x2, int y2) {
        int x1 = Players.getLocal().getLocation().getX();
        int y1 = Players.getLocal().getLocation().getY();
        int x = x1 - x2;
        int y = y1 - y2;
        double angle = Math.toDegrees(Math.atan2(x, y));
        if (x == 0 && y > 0)
            angle = 180;
        if (x < 0 && y == 0)
            angle = 90;
        if (x == 0 && y < 0)
            angle = 0;
        if (x < 0 && y == 0)
            angle = 270;
        if (x < 0 && y > 0)
            angle += 270;
        if (x > 0 && y > 0)
            angle += 90;
        if (x < 0 && y < 0)
            angle = Math.abs(angle) - 180;
        if (x > 0 && y < 0)
            angle = Math.abs(angle) + 270;
        if (angle < 0)
            angle = 360 + angle;
        if (angle >= 360)
            angle -= 360;
        return (int) angle;
    }


}
