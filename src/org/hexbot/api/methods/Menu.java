package org.hexbot.api.methods;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.util.Random;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Tile;

import java.awt.*;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/26/13
 * Time: 4:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class Menu {
    private static Pattern stripFormatting = Pattern.compile("\\<.+?\\>");

    private static boolean arrayContains(String[] items, String searchString) {
        searchString = searchString.toLowerCase();
        for (String s : items) {
            if (s.toLowerCase().contains(searchString)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Search a menu for multiple Strings and click the first occurrence that is found.
     *
     * @param items The Strings to search the menu for
     * @return Whether or not the option was clicked
     */
    public static boolean atMenu(String[] items) {
        ArrayList<String> menuItems = getMenuItems();
        for (String s : menuItems) {
            s = s.toLowerCase();
            if (arrayContains(items, s)) {
                return click(s);
            }
        }
        click("Cancel");
        return false;
    }

    /**
     * Returns the location of the menu. Returns null if not open.
     */
    public static Point getMenuLocation() {
        if (!isMenuOpen()) return null;
        int x = Game.getMenuX();
        int y = Game.getMenuY();
        return new Point(x, y);
    }

    /**
     * Removed the stuff in html brackets <, >
     */
    public static String stripFomatting(String input) {
        return stripFormatting.matcher(input).replaceAll("");
    }

    /**
     * Returns first half + second half. As displayed in rs.
     */
    public static ArrayList<String> getMenuItems() {
        String[] options = Game.getMenuOptions();
        String[] actions = Game.getMenuActions();
        int offset = getMenuCount();
        ArrayList<String> output = new ArrayList<String>();
        for (int i = offset - 1; i >= 0; i--) {
            String option = options[i];
            String action = actions[i];
            if (option != null && action != null) {
                String text = stripFomatting(action)
                        + ' ' + stripFomatting(option);
                output.add(text);
            }
        }
        return output;
    }

    /**
     * Returns the first half. "Walk here" "Follow"
     */
    public static ArrayList<String> getMenuActions() {
        String[] options = Game.getMenuActions();
        int offset = getMenuCount();
        ArrayList<String> output = new ArrayList<String>();
        for (int i = offset - 1; i >= 0; i--) {
            String option = options[i];
            if (option != null) {
                String text = stripFomatting(option);
                output.add(text);
            }
        }
        return output;
    }

    /**
     * Returns the second half. "<user name>".
     */
    public static ArrayList<String> getMenuOptions() {
        String[] options = Game.getMenuOptions();
        int offset = getMenuCount();
        ArrayList<String> output = new ArrayList<String>();
        for (int i = offset - 1; i >= 0; i--) {
            String option = options[i];
            if (option != null) {
                String text = stripFomatting(option);
                output.add(text);
            }
        }
        return output;
    }

    public static int getMenuCount() {
        return Game.getMenuCount();

    }

    /**
     * Returns the index (Start at 0) in the menu for a given action. -1 for invalid.
     */
    public static int getMenuIndex(String option) {
        option = option.toLowerCase();
        java.util.List<String> actions = getMenuItems();
        for (int i = 0; i < actions.size(); i++) {
            String action = actions.get(i);
            if (action.toLowerCase().contains(option)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns the index (Start at 0) in the menu for a given action. -1 for invalid.
     */
    public static int getMenuIndex(String a, String optionContains) {
        optionContains = optionContains.toLowerCase();
        java.util.List<String> actions = getMenuItems();
        for (int i = 0; i < actions.size(); i++) {
            String action = actions.get(i);
            if (action.toLowerCase().contains(optionContains) && getMenuOptions().get(i).contains(a)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Performs a given action. Returns true on success.
     * <p/>
     * Opens the menu if the action exists and the menu isn't open.
     * If it is open and the action does not exist clicks "Cancel".
     */
    public static boolean click(String option) {
        int idx = getMenuIndex(option);
        if (idx == -1) {
            if (isMenuOpen())
                click("Cancel");
            return false;
        } else if (idx == 0 && !isMenuOpen()) {
            Mouse.click(true);
            return true;
        } else {
            return clickWithMenu(idx);
        }
    }

    public static boolean interact(String option, Tile t) {
        final Point bottomLeft = t.getScreenLocation(), topLeft = t.derive(0, 1).getScreenLocation(),
                bottomRight = t.derive(1, 0).getScreenLocation(), topRight = t.derive(1, 1).getScreenLocation();
        final Polygon polygon = new Polygon();
        polygon.addPoint(bottomLeft.x, bottomLeft.y);
        polygon.addPoint(topLeft.x, topLeft.y);
        polygon.addPoint(topRight.x, topRight.y);
        polygon.addPoint(bottomRight.x, bottomRight.y);
        final Rectangle boundingBox = polygon.getBounds();
        for (byte tries = 0; tries < 5 && !optionsContains(option); ) {
            final Point distributed = new Point(boundingBox.x + Random.nextInt(0, boundingBox.width) + Random.nextInt(-20, 20), boundingBox.y + Random.nextInt(0, boundingBox.height) + Random.nextInt(-20, 20));
            if (!polygon.contains(distributed)) {
                continue;
            }
            Mouse.move(distributed);
            tries++;
        }
        boolean b = click(option);
        if (isMenuOpen())
            click("Cancel");
        return b;
    }

    public static boolean actionsContains(String str) {
        for (String s : getMenuActions()) {
            if (s.contains(str)) {
                return true;
            }
        }
        return false;
    }

    public static boolean optionsContains(String str) {
        return getMenuIndex(str) >= 0;
    }

    /**
     * Left clicks at the given index.
     */
    public static boolean clickWithMenu(int index) {
        if (isMenuOpen() || openMenu()) {
            Point menu = getMenuLocation();
            int xOff = 2 + Random.nextInt(30, 50);
            int yOff = Random.nextInt(23, 30) + (15 * index);
            Mouse.click(menu.x + xOff, menu.y + yOff);
            for (byte i = 0; i < 5 && Menu.isMenuOpen(); i++, Time.sleep(100)) ;
            return true;
        }
        if (isMenuOpen())
            click("Cancel");
        return false;
    }

    public static boolean openMenu() {
        if (!Menu.isMenuOpen()) {
            Mouse.click(false);
            for (byte i = 0; i < 5 && !Menu.isMenuOpen(); i++, Time.sleep(100)) ;
        }
        return Menu.isMenuOpen();
    }

    public static boolean isMenuOpen() {
        return Game.isMenuOpen();
    }
}