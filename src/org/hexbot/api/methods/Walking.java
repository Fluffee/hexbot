package org.hexbot.api.methods;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.walking.web.Web;
import org.hexbot.api.methods.walking.web.WebPath;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Locatable;
import org.hexbot.api.wrapper.Path;
import org.hexbot.api.wrapper.Tab;
import org.hexbot.api.wrapper.Tile;
import org.hexbot.bot.Bot;
import org.hexbot.script.ScriptHandler;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/4/13
 * Time: 8:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class Walking {

    public static void walkTileMM(Tile t) {
        Point p = Calculations.tileToMinimap(t);

        Mouse.click(p);
        long timeout = System.currentTimeMillis();
        while (Calculations.distanceTo(t) > 3 && System.currentTimeMillis() - timeout < 8000) {
            Time.sleep(50);
        }
    }

    public static void walkTileMM(final Locatable loc) {
        walkTileMM(loc.getLocation());
    }

    public static boolean reachedDestination(Tile[] path) {
        if (Calculations.distanceTo(path[path.length - 1]) < 3) {
            return true;
        }
        return false;
    }

    public static boolean reachedDestination(Tile tile) {
        if (Calculations.distanceTo(tile) < 3) {
            return true;
        }
        return false;
    }

     public static void walkPath(Tile[] path) {
        for(int i = path.length -1; i >= 0; i --){
            if(path[i].isOnMap() && !Walking.reachedDestination(path)){
                Walking.walkTileMM(path[i]);
                break;
            }
        }
    }
      /*
      This could mess up when a tile is missed ^ works i only needed to add a break so the
      path does loop from the back and as soon as a tile gets on the map it will walk to it.

      ^ IS TESTED AND WORKS!
       */
   /* public static void walkPath(Tile[] path) {
        for (int i = 0; i < path.length - 1; i++) {
            if (path[i].isOnMap() && !reachedDestination(path)) {
                walkTileMM(path[i]);
            }
        }
    }  */

    public static Tile[] findPath(Tile end) {
        return Path.findPath(Players.getLocal().getLocation(), end);
    }

    public static int pathLengthTo(Tile paramTile, boolean paramBoolean) {
        Tile localTile = Players.getLocal().getLocation();
        return pathLengthBetween(localTile, paramTile, paramBoolean);
    }

    public static int pathLengthBetween(Tile paramTile1, Tile paramTile2, boolean paramBoolean) {
        return dijkstraDist(paramTile1.getX() - Bot.client.getBaseX(), paramTile1.getY() - Bot.client.getBaseY(), paramTile2.getX() - Bot.client.getBaseX(), paramTile2.getY() - Bot.client.getBaseY(), paramBoolean);
    }

    public static boolean canReach(Tile paramTile, boolean paramBoolean) {
        return pathLengthTo(paramTile, paramBoolean) != -1;
    }

    private static int dijkstraDist(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean) {
        int[][] arrayOfInt1 = new int[104][104];
        int[][] arrayOfInt2 = new int[104][104];
        int[] arrayOfInt3 = new int[4000];
        int[] arrayOfInt4 = new int[4000];

        for (int i = 0; i < 104; i++) {
            for (int j = 0; j < 104; j++) {
                arrayOfInt1[i][j] = 0;
                arrayOfInt2[i][j] = 99999999;
            }
        }

        int i = paramInt1;
        int j = paramInt2;
        arrayOfInt1[paramInt1][paramInt2] = 99;
        arrayOfInt2[paramInt1][paramInt2] = 0;
        int k = 0;
        int m = 0;
        arrayOfInt3[k] = paramInt1;
        arrayOfInt4[(k++)] = paramInt2;
        int[][] arrayOfInt5 = Bot.client.getCollisionMap()[Game.getPlane()].getFlags();
        int n = arrayOfInt3.length;
        int i1 = 0;
        while (m != k) {
            i = arrayOfInt3[m];
            j = arrayOfInt4[m];

            if (paramBoolean) {
                if (((i == paramInt3) && (j == paramInt4 + 1)) || ((i == paramInt3) && (j == paramInt4 - 1)) || ((i == paramInt3 + 1) && (j == paramInt4)) || ((i == paramInt3 - 1) && (j == paramInt4))) {
                    i1 = 1;
                    break;
                }
            } else if ((i == paramInt3) && (j == paramInt4)) {
                i1 = 1;
            }

            m = (m + 1) % n;
            int i2 = arrayOfInt2[i][j] + 1;

            if ((j > 0) && (arrayOfInt1[i][(j - 1)] == 0) && ((arrayOfInt5[i][(j - 1)] & 0x1280102) == 0)) {
                arrayOfInt3[k] = i;
                arrayOfInt4[k] = (j - 1);
                k = (k + 1) % n;
                arrayOfInt1[i][(j - 1)] = 1;
                arrayOfInt2[i][(j - 1)] = i2;
            }

            if ((i > 0) && (arrayOfInt1[(i - 1)][j] == 0) && ((arrayOfInt5[(i - 1)][j] & 0x1280108) == 0)) {
                arrayOfInt3[k] = (i - 1);
                arrayOfInt4[k] = j;
                k = (k + 1) % n;
                arrayOfInt1[(i - 1)][j] = 2;
                arrayOfInt2[(i - 1)][j] = i2;
            }

            if ((j < 103) && (arrayOfInt1[i][(j + 1)] == 0) && ((arrayOfInt5[i][(j + 1)] & 0x1280120) == 0)) {
                arrayOfInt3[k] = i;
                arrayOfInt4[k] = (j + 1);
                k = (k + 1) % n;
                arrayOfInt1[i][(j + 1)] = 4;
                arrayOfInt2[i][(j + 1)] = i2;
            }

            if ((i < 103) && (arrayOfInt1[(i + 1)][j] == 0) && ((arrayOfInt5[(i + 1)][j] & 0x1280180) == 0)) {
                arrayOfInt3[k] = (i + 1);
                arrayOfInt4[k] = j;
                k = (k + 1) % n;
                arrayOfInt1[(i + 1)][j] = 8;
                arrayOfInt2[(i + 1)][j] = i2;
            }

            if ((i > 0) && (j > 0) && (arrayOfInt1[(i - 1)][(j - 1)] == 0) && ((arrayOfInt5[(i - 1)][(j - 1)] & 0x128010E) == 0) && ((arrayOfInt5[(i - 1)][j] & 0x1280108) == 0) && ((arrayOfInt5[i][(j - 1)] & 0x1280102) == 0)) {
                arrayOfInt3[k] = (i - 1);
                arrayOfInt4[k] = (j - 1);
                k = (k + 1) % n;
                arrayOfInt1[(i - 1)][(j - 1)] = 3;
                arrayOfInt2[(i - 1)][(j - 1)] = i2;
            }

            if ((i > 0) && (j < 103) && (arrayOfInt1[(i - 1)][(j + 1)] == 0) && ((arrayOfInt5[(i - 1)][(j + 1)] & 0x1280138) == 0) && ((arrayOfInt5[(i - 1)][j] & 0x1280108) == 0) && ((arrayOfInt5[i][(j + 1)] & 0x1280120) == 0)) {
                arrayOfInt3[k] = (i - 1);
                arrayOfInt4[k] = (j + 1);
                k = (k + 1) % n;
                arrayOfInt1[(i - 1)][(j + 1)] = 6;
                arrayOfInt2[(i - 1)][(j + 1)] = i2;
            }

            if ((i < 103) && (j > 0) && (arrayOfInt1[(i + 1)][(j - 1)] == 0) && ((arrayOfInt5[(i + 1)][(j - 1)] & 0x1280183) == 0) && ((arrayOfInt5[(i + 1)][j] & 0x1280180) == 0) && ((arrayOfInt5[i][(j - 1)] & 0x1280102) == 0)) {
                arrayOfInt3[k] = (i + 1);
                arrayOfInt4[k] = (j - 1);
                k = (k + 1) % n;
                arrayOfInt1[(i + 1)][(j - 1)] = 9;
                arrayOfInt2[(i + 1)][(j - 1)] = i2;
            }

            if ((i < 103) && (j < 103) && (arrayOfInt1[(i + 1)][(j + 1)] == 0) && ((arrayOfInt5[(i + 1)][(j + 1)] & 0x12801E0) == 0) && ((arrayOfInt5[(i + 1)][j] & 0x1280180) == 0) && ((arrayOfInt5[i][(j + 1)] & 0x1280120) == 0)) {
                arrayOfInt3[k] = (i + 1);
                arrayOfInt4[k] = (j + 1);
                k = (k + 1) % n;
                arrayOfInt1[(i + 1)][(j + 1)] = 12;
                arrayOfInt2[(i + 1)][(j + 1)] = i2;
            }
        }
        if (i1 != 0) {
            return arrayOfInt2[i][j];
        }
        return -1;
    }


    public static boolean walkWeb(Tile end) {
        WebPath path = Web.getPath(end);
        walk(path, end);
        return end.getDistance() < 3;
    }

    protected static void walk(WebPath path, Tile end) {
        Tile next = path.getNextTile();
        if (next == null || next.getDistance() < 4)
            return;
        while (next != null && next.getDistance() > 5) {
            if (Players.getLocal().isMoving()) {
                Time.sleep(100, 120);
            } else {
                Walking.walkTileMM(next);
                Time.sleep(400);
            }
        }
        walk(path, end);
    }

    /**
     * Used to generate a local path from a to b using A*
     *
     * @param end
     * @return true is succeeded
     */
    public static boolean walkTo(Tile end) {
        walkWeb(end);
        return Players.getLocal().getDistanceTo(end) < 5;
    }

    public static Tile[] reverse(Tile[] input) {
        Tile[] tile = new Tile[input.length];
        for (int i = input.length - 1; i >= 0; i--) {
            tile[i] = input[i];
        }
        return tile;
    }

    public static void traverse(Tile t) {
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < 30000) {
            if (Calculations.distanceTo(t) < 4 && ScriptHandler.isRunning())
                return;
            else {
                walkTileMM(getClosestOnMap(t));
            }
        }
    }

    public static void traverse(Tile t, long timeout) {
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < timeout && ScriptHandler.isRunning()) {
            if (Calculations.distanceTo(t) < 4)
                return;
            else {
                walkTileMM(getClosestOnMap(t));
            }
        }
    }

    public static Tile getClosestOnMap(Tile tile) {
        if (tile.isOnMap()) {
            return tile;
        }
        final Tile location = Players.getLocal().getLocation();
        tile = tile.derive(-location.getX(), -location.getY());
        final double angle = Math.atan2(tile.getY(), tile.getX());
        return new Tile(
                location.getX() + (int) (16d * Math.cos(angle)),
                location.getY() + (int) (16d * Math.sin(angle)),
                tile.getPlane()
        );
    }

    public static boolean isRunning() {
        return Game.getSettings()[173] == 1;
    }

    public static boolean setRun(boolean running) {
        if ((isRunning() && running) || (!isRunning() && !running)) return false;
        Tab.open(Tab.OPTIONS);
        org.hexbot.api.wrapper.Component widget = Widgets.getChild(261, 0);
        widget.click();
        return true;
    }

}
