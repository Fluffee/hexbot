package org.hexbot.api.methods;

import org.hexbot.api.util.Filter;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;
import org.hexbot.api.wrapper.Widget;
import org.hexbot.bot.Bot;
import org.hexbot.impl.IWidget;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/30/13
 * Time: 12:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class Widgets {

    public static Widget[] getLoaded() {
        ArrayList<Widget> widgets = new ArrayList<Widget>();
        int index = 0;
        IWidget[][] loaded = Bot.client.getWidgets();
        for (IWidget[] obj : loaded) {
            widgets.add(new Widget(index));
            index++;
        }
        return widgets.toArray(new Widget[0]);
    }

    public static Component getChild(int parent, int child) {
        Component[] comps = getChildren(parent);
        if (comps != null)
            for (Component comp : comps) {
                if (comp != null)
                    if (comp.getId() == child)
                        return comp;
            }
        return null;
    }

    public static Component[] getChildren(int parent) {
        if (getLoaded().length < parent)
            return null;
        return getLoaded()[parent].getChildren();
    }

    public static IWidget[][] getRaw() {
        return Bot.client.getWidgets();
    }

    private static void setPosition(Object o, int a, int i) {
        try {
            System.out.println(a + ", " + i);
            o.getClass().getField("masterX").set(o, a);
            o.getClass().getField("masterY").set(o, i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Component getContinue() {
        if (Widgets.getLoaded() == null) {
            return null;
        }
        for (Widget iface : Widgets.getLoaded()) {
            if (iface.getChildren() != null) {
                for (Component ic : iface.getChildren()) {
                    if (ic.getText().toLowerCase().contains("to continue"))
                        return ic;
                }
            }
        }
        return null;
    }

    public static boolean canClickContinue() {
        Component cont = getContinue();
        return cont != null && cont.isOnScreen() && cont.getX() > 0;
    }

    public static void clickContinue() {
        getContinue().click();
        Time.sleep(400, 800);
    }

    public static Component getWidgetFromString(String text) {
        for (Widget widget : Widgets.getLoaded()) {
            if (widget != null) {
                if (widget.getChildren() != null && widget.getChildren().length > 0) {
                    for (Component comp : widget.getChildren()) {
                        if (comp.getText() != null) {
                            if (comp.getText().equals(text))
                                return comp;
                        }
                    }
                }
            }
        }
        return null;
    }

    public static Component[] getValidated(Filter<Component> filter) {
        ArrayList<Component> things = new ArrayList<Component>();
        for (Widget widget : getLoaded()) {
            if (widget == null || widget.getChildren() == null)
                continue;
            for (Component child : widget.getChildren()) {
                if (filter.accept(child)) {
                    things.add(child);
                }
            }
        }
        return things.toArray(new Component[things.size()]);
    }
}
