package org.hexbot.api.wrapper;

import org.hexbot.api.methods.Widgets;
import org.hexbot.impl.IWidget;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/30/13
 * Time: 12:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class Widget {
    int index;

    public Widget(int index) {
        this.index = index;
    }

    public boolean validate() {
        return Widgets.getLoaded().length >= index && Widgets.getLoaded()[index] != null;
    }

    public Component[] getChildren() {
        ArrayList<Component> components = new ArrayList<Component>();
        IWidget[] children = Widgets.getRaw()[index];
        if (children == null) {
            return null;
        }
        ArrayList<Component> childArrayList = new ArrayList<Component>();
        for (short index = 0; index < children.length; index++) {
            final Component child = new Component(children[index]);
            if (child.isValid())
                childArrayList.add(child);
        }
        return childArrayList.toArray(new Component[childArrayList.size()]);
    }

    public Component getChild(int i) {
        Component[] children = getChildren();
        if (children != null && children.length > i)
            return getChildren()[i];
        return null;
    }

    public int getId() {
        return index;
    }
}

