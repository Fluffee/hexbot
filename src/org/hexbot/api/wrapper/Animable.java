package org.hexbot.api.wrapper;

import org.hexbot.api.wrapper.rs.RSModel;
import org.hexbot.impl.IAnimable;

import java.lang.ref.WeakReference;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/17/13
 * Time: 11:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class Animable {
    public final WeakReference<IAnimable> instance;


    public Animable(IAnimable inst) {
        instance = new WeakReference<IAnimable>(inst);
    }

    public int getModelHeight() {
        return get().getModelHeight();
    }

    public RSModel getModel() {
        return (RSModel) get().getModel();
    }

    public IAnimable get() {
        return instance.get();
    }

    public static void updateModel(Object a, Object b) {
        ((IAnimable) a).setModel(new RSModel((IAnimable) a, b, null));
    }
}
