package org.hexbot.api.wrapper.rs;

import org.hexbot.api.methods.Calculations;
import org.hexbot.api.wrapper.Tile;
import org.hexbot.impl.IAnimable;
import org.hexbot.impl.IEntity;
import org.hexbot.impl.IModel;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/18/13
 * Time: 1:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class RSModel {

    public int[] XVerticies;
    public int[] YVerticies;
    public int[] ZVerticies;
    public int[] XTriangles;
    public int[] YTriangles;
    public int[] ZTriangles;
    public int[] xbase;
    public int[] zbase;
    public Tile tile;

    public RSModel(IAnimable a, Object o, Tile t) {
        tile = t;
        IModel model = (IModel) o;
        if (model != null) {
            XVerticies = model.getXVerticies().clone();
            YVerticies = model.getYVerticies().clone();
            ZVerticies = model.getZVerticies().clone();
            XTriangles = model.getXTriangles().clone();
            YTriangles = model.getYTriangles().clone();
            ZTriangles = model.getZTriangles().clone();
        } else {
            XVerticies = new int[]{0};
            YVerticies = new int[]{0};
            ZVerticies = new int[]{0};
            XTriangles = new int[]{0};
            YTriangles = new int[]{0};
            ZTriangles = new int[]{0};
        }
        if (a instanceof IEntity && ((IEntity) a).getOrientation() != 0)
            rotate(((IEntity) a).getOrientation());

    }

    public Polygon[] getTriangles() {
        if (XTriangles == null)
            return null;
        ArrayList<Polygon> polygons = new ArrayList<Polygon>();

        int len = XTriangles.length;
        for (int i = 0; i < len; ++i) {
            Point p1 = Calculations.worldToScreen(tile.getX() + XVerticies[XTriangles[i]], tile.getY() + ZVerticies[XTriangles[i]], -YVerticies[XTriangles[i]]);
            Point p2 = Calculations.worldToScreen(tile.getX() + XVerticies[YTriangles[i]], tile.getY() + ZVerticies[YTriangles[i]], -YVerticies[YTriangles[i]]);
            Point p3 = Calculations.worldToScreen(tile.getX() + XVerticies[ZTriangles[i]], tile.getY() + ZVerticies[ZTriangles[i]], -YVerticies[ZTriangles[i]]);

            if (p1.x >= 0 && p2.x >= 0 && p2.x >= 0 && p3.x > 0 && p3.y > 0) {
                Polygon p = new Polygon(new int[]{p1.x, p2.x, p3.x}, new int[]{p1.y, p2.y, p3.y}, 3);
                polygons.add(p);
            }
        }
        return polygons.toArray(new Polygon[polygons.size()]);
    }

    public void draw(Graphics g) {
        Polygon[] poly = getTriangles();
        if (poly == null)
            return;
        for (Polygon polygon : getTriangles()) {
            g.drawPolygon(polygon);
        }
    }

    public Point getCentralPoint() {
        int avX = 0;
        int avY = 0;
        int len = XTriangles.length - 1;
        if (len < 1)
            return new Point(-1, -1);
        for (int i = 0; i < len; ++i) {
            Point p1 = Calculations.worldToScreen(tile.getX() + XVerticies[XTriangles[i]], tile.getY() + ZVerticies[XTriangles[i]], -YVerticies[XTriangles[i]]);
            Point p2 = Calculations.worldToScreen(tile.getX() + XVerticies[YTriangles[i]], tile.getY() + ZVerticies[YTriangles[i]], -YVerticies[YTriangles[i]]);
            Point p3 = Calculations.worldToScreen(tile.getX() + XVerticies[ZTriangles[i]], tile.getY() + ZVerticies[ZTriangles[i]], -YVerticies[ZTriangles[i]]);
            avX += (p1.x + p2.x + p3.x) / 3;
            avY += (p1.y + p2.y + p3.y) / 3;
        }
        return new Point(avX / len, avY / len);
    }

    public void rotate(int orien) {
        try {
            xbase = Arrays.copyOfRange(XVerticies, 0, XVerticies.length);
            zbase = Arrays.copyOfRange(ZVerticies, 0, ZVerticies.length);
            XVerticies = new int[xbase.length];
            ZVerticies = new int[zbase.length];
            int theta = orien & 0x3fff;
            int sin = Calculations.SIN_TABLE[theta];
            int cos = Calculations.COS_TABLE[theta];
            int length = XTriangles.length - 1;
            for (int i = 0; i < length; ++i) {
                XVerticies[i] = (xbase[i] * cos + zbase[i] * sin >> 15) >> 1;
                ZVerticies[i] = (zbase[i] * cos - xbase[i] * sin >> 15) >> 1;
            }
        } catch (Exception e) {

        }
    }

}
