package org.hexbot.api.wrapper.rs;

import org.hexbot.bot.rsi.ReflectionEngine;
import org.hexbot.util.Storage;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 7:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class Node {
    public Object instance;

    public Node(Object o) {
        instance = o;
    }

    public boolean isValid() {
        return instance != null;
    }

    public Node getNext() {
        return new Node(ReflectionEngine.getObjectField(instance, Storage.getClass("INode"), Storage.getField("INode", "getNext")));
    }

    public Node getPrevious() {
        return new Node(ReflectionEngine.getObjectField(instance, Storage.getClass("INode"), Storage.getField("INode", "getPrevious")));
    }

    public long getId() {
        return (long) ReflectionEngine.getObjectField(instance, Storage.getClass("Node"), Storage.getField("Node", "getNext"));
    }
}
