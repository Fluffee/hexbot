package org.hexbot.api.wrapper.rs;

import org.hexbot.bot.rsi.ReflectionEngine;
import org.hexbot.util.Storage;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 7:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class NodeList {
    Object instance;

    public NodeList(Object obj) {
        instance = obj;
    }

    public Node getCurrent() {
        return new Node(ReflectionEngine.getObjectField(instance, Storage.getClass("NodeList"), Storage.getField("NodeList", "getCurrent")));
    }

    public Node getHead() {
        return new Node(ReflectionEngine.getObjectField(instance, Storage.getClass("INodeList"), Storage.getField("INodeList", "getHead")));
    }
}
