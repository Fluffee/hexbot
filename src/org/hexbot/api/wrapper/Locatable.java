package org.hexbot.api.wrapper;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 7:05 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Locatable {
    public Tile getLocation();

    boolean interact(String s);

    public void click();

    public boolean isOnScreen();


}
