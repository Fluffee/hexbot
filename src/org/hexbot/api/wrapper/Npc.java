package org.hexbot.api.wrapper;

import org.hexbot.bot.Bot;
import org.hexbot.impl.INpc;
import org.hexbot.impl.INpcDef;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/30/13
 * Time: 10:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class Npc extends Entity {


    public Npc(INpc inst) {
        super(inst);
    }

    public INpc get() {
        return (INpc) instance.get();
    }

    public INpcDef getDefinition() {
        INpc i = get();
        if (i == null)
            return null;
        return i.getDefinition();
    }

    public String getName() {
        if (getDefinition() == null)
            return null;
        return getDefinition().getName();
    }

    public int getId() {
        return getDefinition().getId();
    }

    public int getLevel() {
        if (getDefinition() == null)
            return -1;
        return getDefinition().getLevel();
    }


    public int getIndex() {
        int x = 0;
        for (INpc np : Bot.client.getNpcs()) {
            if (np != null && np.equals(get()))
                return x;
            else
                x++;
        }
        return x;
    }

    public boolean equals(Object object) {
        if (object instanceof Npc) {
            return ((Npc) object).get().equals(get());
        }
        return object.equals(this);
    }
}
