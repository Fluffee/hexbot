package org.hexbot.api.wrapper;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.util.Time;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/4/13
 * Time: 10:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class InventoryItem extends Item {
    int index;

    public InventoryItem(int id, int stackSize, int i) {
        super(id, stackSize);
        index = i;
        this.id -= 1;
    }

    public Point getPoint() {
        int col = (index % 4);
        int row = (index / 4);
        int x = 580 + (col * 42);
        int y = 228 + (row * 36);
        return new Point(x, y);
    }
    
    public void click() {
    	Mouse.move(getPoint());
    	Mouse.click(true);
    }

    public boolean interact(String str) {
        Mouse.move(getPoint());
        Mouse.click(false);
        while (!org.hexbot.api.methods.Menu.isMenuOpen())
            Time.sleep(20);
        return org.hexbot.api.methods.Menu.click(str);
    }

}
