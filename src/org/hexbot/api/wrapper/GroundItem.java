package org.hexbot.api.wrapper;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.Calculations;
import org.hexbot.api.methods.Menu;
import org.hexbot.api.util.Random;
import org.hexbot.api.util.Time;
import org.hexbot.impl.IItem;
import org.hexbot.impl.INode;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 7:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class GroundItem extends Item implements Locatable {
    Tile location;
    IItem inst;
    int plane;

    public GroundItem(INode obj, Tile t, int plane) {
        super(-1, -1);
        inst = (IItem) obj;
        location = new Tile(t.getX(), t.getY(), plane);
        super.id = getId();
        super.stackSize = getStackSize();
        this.plane = plane;
    }

    public GroundItem(INode obj, int x, int y, int plane) {
        super(-1, -1);
        inst = (IItem) obj;
        location = new Tile(x, y, plane);
        super.id = getId();
        super.stackSize = getStackSize();
        this.plane = plane;
    }

    public int getId() {
        return inst.getId();
    }

    public int getStackSize() {
        return inst.getStackSize();
    }

    public Tile getLocation() {
        return location;
    }

    @Override
    public boolean interact(String s) {
        int k = -1;
        Mouse.move(getLocation().getScreenLocation());
        Mouse.click(false);
        Menu.clickWithMenu((k = Menu.getMenuIndex(getName())));
        Time.sleep(Random.nextInt(400, 1200));
        return k >= 0;
    }

    @Override
    public void click() {
        Mouse.click(getLocation().getScreenLocation());
    }

    @Override
    public boolean isOnScreen() {
        return Calculations.onGameScreen(getLocation().getScreenLocation());  //To change body of implemented methods use File | Settings | File Templates.
    }
}
