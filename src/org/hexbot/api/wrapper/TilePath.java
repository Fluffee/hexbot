package org.hexbot.api.wrapper;

import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.Walking;
import org.hexbot.api.util.Time;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 1-5-13
 * Time: 19:19
 * To change this template use File | Settings | File Templates.
 */
public class TilePath {

    private final Tile[] tiles;

    public TilePath(Tile[] tiles) {
        this.tiles = tiles;
    }

    public Tile getNext() {
        for (int i = tiles.length - 1; i >= 0; i--) {
            if (tiles[i] != null && tiles[i].isOnMap()) {
                return tiles[i];
            }
        }
        return null;
    }

    public boolean traverse() {
        Walking.walkTileMM(getNext());
        Time.sleep(1000, 2000);
        return Players.getLocal().isMoving();
    }

}
