package org.hexbot.api.wrapper.input;


import org.hexbot.bot.event.EventManager;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.EventListener;

public abstract class Keyboard extends Focus implements KeyListener {

    public abstract void _keyPressed(KeyEvent e);

    public abstract void _keyReleased(KeyEvent e);

    public abstract void _keyTyped(KeyEvent e);

    private KeyListener[] getListeners() {
        ArrayList<KeyListener> listeners = new ArrayList<KeyListener>();
        for (EventListener listener : EventManager.get()) {
            if (listener instanceof KeyListener) {
                listeners.add((KeyListener) listener);
            }
        }
        return listeners.toArray(new KeyListener[listeners.size()]);
    }

    public void keyPressed(final KeyEvent e) {
        for (KeyListener listener : getListeners()) {
            listener.keyPressed(e);
        }
        _keyPressed(e);
    }

    public void keyReleased(final KeyEvent e) {
        for (KeyListener listener : getListeners()) {
            listener.keyReleased(e);
        }
        _keyReleased(e);
    }

    public void keyTyped(final KeyEvent e) {
        for (KeyListener listener : getListeners()) {
            listener.keyTyped(e);
        }
        _keyTyped(e);
    }
}