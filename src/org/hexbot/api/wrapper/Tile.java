package org.hexbot.api.wrapper;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.Calculations;
import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Players;
import org.hexbot.api.methods.Walking;
import org.hexbot.api.util.Random;

import java.awt.*;

public class Tile implements Locatable {
    private int x, y, plane = 0;

    static interface Flag {
        static final int WALL_NORTHWEST = 0x1;
        static final int WALL_NORTH = 0x2;
        static final int WALL_NORTHEAST = 0x4;
        static final int WALL_EAST = 0x8;
        static final int WALL_SOUTHEAST = 0x10;
        static final int WALL_SOUTH = 0x20;
        static final int WALL_SOUTHWEST = 0x40;
        static final int WALL_WEST = 0x80;

        static final int OBJECT_TILE = 0x100;

        static final int WALL_BLOCK_NORTHWEST = 0x200;
        static final int WALL_BLOCK_NORTH = 0x400;
        static final int WALL_BLOCK_NORTHEAST = 0x800;
        static final int WALL_BLOCK_EAST = 0x1000;
        static final int WALL_BLOCK_SOUTHEAST = 0x2000;
        static final int WALL_BLOCK_SOUTH = 0x4000;
        static final int WALL_BLOCK_SOUTHWEST = 0x8000;
        static final int WALL_BLOCK_WEST = 0x10000;

        static final int OBJECT_BLOCK = 0x20000;
        static final int DECORATION_BLOCK = 0x40000;

        static final int WALL_ALLOW_RANGE_NORTHWEST = 0x400000;
        static final int WALL_ALLOW_RANGE_NORTH = 0x800000;
        static final int WALL_ALLOW_RANGE_NORTHEAST = 0x1000000;
        static final int WALL_ALLOW_RANGE_EAST = 0x2000000;
        static final int WALL_ALLOW_RANGE_SOUTHEAST = 0x4000000;
        static final int WALL_ALLOW_RANGE_SOUTH = 0x8000000;
        static final int WALL_ALLOW_RANGE_SOUTHWEST = 0x10000000;
        static final int WALL_ALLOW_RANGE_WEST = 0x20000000;

        static final int OBJECT_ALLOW_RANGE = 0x40000000;

        static final int BLOCKED = 0x1280100;
    }

    public Tile(final int x, final int y, final int plane) {
        this.x = x;
        this.y = y;
        this.plane = plane;
    }

//    public boolean canReach() {
//        if (getFlag() != 0)
//            return false;
//        Tile[] tiles = new Tile[]{derive(-1, 0), derive(1, 0), derive(0, -1), derive(0, 1)};
//        for (Tile tile : tiles) {
//            if (tile.getFlag() != 0)
//                return false;
//        }
//        return true;
//    }

    public Tile(final int x, final int y) {
        this.x = x;
        this.y = y;
        plane = 0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getPlane() {
        return plane;
    }

    public int getFlag() {
        return Game.getFlags()[x - Game.getBaseX()][y - Game.getBaseY()];
    }

    public Tile derive(int x, int y) {
        return new Tile(this.x + x, this.y + y);
    }

    public Polygon getBounds() {
        final Point bottomLeft = new Point((getScreenLocation().x + derive(-1, 1).getScreenLocation().x) / 2, (derive(-1, 1).getScreenLocation().y + getScreenLocation().y) / 2),
                topLeft = new Point((getScreenLocation().x + derive(-1, -1).getScreenLocation().x) / 2, (derive(-1, -1).getScreenLocation().y + getScreenLocation().y) / 2),
                bottomRight = new Point((getScreenLocation().x + derive(1, 1).getScreenLocation().x) / 2, (derive(1, 1).getScreenLocation().y + getScreenLocation().y) / 2),
                topRight = new Point((getScreenLocation().x + derive(1, -1).getScreenLocation().x) / 2, (derive(1, -1).getScreenLocation().y + getScreenLocation().y) / 2);
        final Polygon polygon = new Polygon();
        polygon.addPoint(bottomLeft.x, bottomLeft.y);
        polygon.addPoint(topLeft.x, topLeft.y);
        polygon.addPoint(topRight.x, topRight.y);
        polygon.addPoint(bottomRight.x, bottomRight.y);
        return polygon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tile tile = (Tile) o;

        if (plane != tile.plane) return false;
        if (x != tile.x) return false;
        if (y != tile.y) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + plane;
        return result;
    }

    public boolean canReach() {
        return Walking.canReach(this, true);
    }

    public void draw(final Graphics g) {
        g.drawPolygon(getBounds());
    }

    public boolean isOnMap() {
        return (Math.abs(Players.getLocal().getLocation().getX() - x) < 15) && (Math.abs(Players.getLocal().getLocation().getY() - y) < 15);
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + plane + ")";
    }

    public Point getMinimapLocation() {
        return Calculations.tileToMinimap(this);
    }

    public Point getScreenLocation() {
        return Calculations.tileToScreen(this);
    }

    @Override
    public Tile getLocation() {
        return this;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean interact(String s) {
        return org.hexbot.api.methods.Menu.interact(s, this);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void click() {
        Mouse.click(getScreenLocation());
    }

    @Override
    public boolean isOnScreen() {
        return Calculations.onGameScreen(getScreenLocation());  //To change body of implemented methods use File | Settings | File Templates.
    }

    public double getDistance() {
        return Calculations.distanceTo(this);
    }


    public double getDistanceTo(Tile tile) {
        return Calculations.distanceBetween(tile, this);
    }

    /**
     * @param locatable any locatable instance
     * @return if the tile contains the locatable's location. Does not work for multiple tiled loca tables.
     */
    public boolean contains(Locatable locatable) {
        return this.equals(locatable.getLocation());
    }

    public static Tile randomize(Tile tile, int distance){
        return new Tile(tile.getX()+ Random.nextInt(-distance, +distance) , tile.getY()+ Random.nextInt(-distance,+distance));
    }

    public static Tile randomize(Tile tile, int Xdistance, int Ydistance){
        return new Tile(tile.getX()+ Random.nextInt(-Xdistance,+Xdistance) , tile.getY()+ Random.nextInt(-Ydistance,+Ydistance));
    }

    public static Tile randomize(Tile tile, int minXdistance, int maxXdistance, int minYdistance, int maxYdistance){
        return new Tile(tile.getX()+ Random.nextInt(-minXdistance,+maxXdistance) , tile.getY()+ Random.nextInt(-minYdistance,+maxYdistance));
    }
}
