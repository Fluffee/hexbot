package org.hexbot.api.wrapper;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.Calculations;
import org.hexbot.api.util.Random;
import org.hexbot.impl.IWidget;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/30/13
 * Time: 12:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class Component {

    IWidget instance;

    public boolean isOnScreen() {
        return Calculations.onScreen(getPoint());
    }

    public Component(IWidget obj) {
        instance = obj;
    }

    public int getSize() {
        return getWidth() * getHeight();
    }

    public int getX() {
        return instance.getMasterX();
    }

    public int getBorderThickness() {
        return instance.getBorderThickness();
    }

    public int getY() {
        return instance.getMasterY();
    }

    public int getRelativeX() {
        return instance.getX();
    }

    public int getRelativeY() {
        return instance.getY();
    }

    public int getWidth() {
        return instance.getWidth();
    }

    public int getHeight() {
        return instance.getHeight();
    }

    public int[] getSlotIds() {
        return instance.getSlotIds();
    }

    public int[] getSlotSizes() {
        return instance.getStackSizes();
    }

    public int getTradeId() {
        return instance.getTradeId();
    }

    public int getTradeStack() {
        return instance.getTradeStack();
    }

    public Point getPoint() {
        return new Point(getX(), getY());
    }

    public Component[] getChildren() {
        ArrayList<Component> components = new ArrayList<Component>();
        IWidget[] list = instance.getComponents();
        if (list == null)
            return null;
        for (IWidget obj : list) {
            if (obj != null)
                components.add(new Component(obj));
        }
        return components.toArray(new Component[1]);
    }

    public int getId() {
        return instance.getId() & 0xFF;
    }

    public Component getParent() {
        IWidget parent = instance.getParent();
        if (parent == null)
            return null;
        return new Component(parent);
    }

    public String getText() {
        return instance.getText();
    }

    public Rectangle getBounds() {
        return new Rectangle(getX(), getY(), getWidth(), getHeight());
    }

    public int getModelId() {
        return instance.getModelId();
    }

    public int getTextureId() {
        return instance.getTextureId();
    }


    public String getComponentName() {
        return instance.getComponentName();
    }

    public String[] getActions() {
        return instance.getActions();
    }

    public boolean isValid() {
        return instance != null && isOnScreen();
    }

    public Dimension getScreenDimension() {
        return new Dimension(getWidth(), getHeight());
    }

    public void click() {
        Rectangle b = getBounds();
        int a = Random.nextInt(b.x, b.x + b.width);
        int c = Random.nextInt(b.y, b.y + b.height);
        Mouse.click(a, c);
    }

    public boolean interact(String s) {
        Rectangle b = getBounds();
        int a = Random.nextInt(b.x, b.x + b.width);
        int c = Random.nextInt(b.y, b.y + b.height);
        Mouse.move(a, c);
        if (org.hexbot.api.methods.Menu.getMenuItems().get(0).contains(s)) {
            Mouse.click(true);
        } else {
            Mouse.click(false);
            return org.hexbot.api.methods.Menu.click(s);
        }
        return true;
    }

    public int getParentId() {
        return instance.getParentId();
    }

    public boolean containsAction(String s) {
        for (String action : getActions()) {
            if (action.contains(s))
                return true;
        }
        return false;
    }
}
