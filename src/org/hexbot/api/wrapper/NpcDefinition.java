package org.hexbot.api.wrapper;

import org.hexbot.impl.INpcDef;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/30/13
 * Time: 10:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class NpcDefinition {
    public INpcDef instance;

    public NpcDefinition(INpcDef o) {
        instance = o;
    }

    public String getName() {
        return instance.getName();
    }

    public int getLevel() {
        return instance.getLevel();
    }

    public int getId() {
        return instance.getId();
    }

    public String[] getActions() {
        return instance.getActions();
    }
}
