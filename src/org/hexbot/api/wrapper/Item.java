package org.hexbot.api.wrapper;

import org.hexbot.bot.Bot;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 2/28/13
 * Time: 12:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class Item {

    int id;
    int stackSize;

    public Item(int id, int stackSize) {
        this.id = id;
        this.stackSize = stackSize;
    }

    public int getId() {
        return id;
    }

    public int getStackSize() {
        return stackSize;
    }

    public ItemDef getDefinition() {
        return new ItemDef(Bot.client.getItemDefinition(id));
    }

    public String getName() {
        ItemDef def = null;
        if ((def = getDefinition()) != null)
            return def.getName();
        return null;
    }

    public String[] getActions() {
        if (getDefinition() == null) return null;
        return getDefinition().getActions();
    }


}

