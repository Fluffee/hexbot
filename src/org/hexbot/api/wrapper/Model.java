package org.hexbot.api.wrapper;

import org.hexbot.api.methods.Calculations;
import org.hexbot.impl.IModel;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/4/13
 * Time: 12:39 AM
 * To change this template use File | Settings | File Templates.
 */
public class Model {
    public IModel instance;
    Tile tile;

    public Model(Object obj, Tile tile) {
        instance = (IModel) obj;
        this.tile = tile;
    }

    public int[] getXVerticies() {
        return instance.getXVerticies();
    }

    public int[] getYVerticies() {
        return instance.getYVerticies();
    }

    public int[] getZVerticies() {
        return instance.getZVerticies();
    }

    public int[] getXTriangles() {
        return instance.getXTriangles();
    }

    public int[] getYTriangles() {
        return instance.getYTriangles();
    }

    public int[] getZTriangles() {
        return instance.getZTriangles();
    }


    public Polygon[] getTriangles() {
        if (instance == null)
            return null;
        ArrayList<Polygon> polygons = new ArrayList<Polygon>();

        int len = getXTriangles().length;
        for (int i = 0; i < len; ++i) {
            Point p1 = Calculations.worldToScreen(tile.getX() + getXVerticies()[getXTriangles()[i]], tile.getY() + getZVerticies()[getXTriangles()[i]], -getYVerticies()[getXTriangles()[i]]);
            Point p2 = Calculations.worldToScreen(tile.getX() + getXVerticies()[getYTriangles()[i]], tile.getY() + getZVerticies()[getYTriangles()[i]], -getYVerticies()[getYTriangles()[i]]);
            Point p3 = Calculations.worldToScreen(tile.getX() + getXVerticies()[getZTriangles()[i]], tile.getY() + getZVerticies()[getZTriangles()[i]], -getYVerticies()[getZTriangles()[i]]);

            if (p1.x >= 0 && p2.x >= 0 && p2.x >= 0 && p3.x > 0 && p3.y > 0) {
                Polygon p = new Polygon(new int[]{p1.x, p2.x, p3.x}, new int[]{p1.y, p2.y, p3.y}, 3);
                polygons.add(p);
            }
        }
        return polygons.toArray(new Polygon[polygons.size()]);
    }

    public void draw(Graphics g) {
        Polygon[] poly = getTriangles();
        if (poly == null)
            return;
        for (Polygon polygon : getTriangles()) {
            g.drawPolygon(polygon);
        }
    }

    public Point getCentralPoint() {
        int avX = 0;
        int avY = 0;
        int len = getXTriangles().length - 1;
        if (len < 1)
            return new Point(-1, -1);
        for (int i = 0; i < len; ++i) {
            Point p1 = Calculations.worldToScreen(tile.getX() + getXVerticies()[getXTriangles()[i]], tile.getY() + getZVerticies()[getXTriangles()[i]], -getYVerticies()[getXTriangles()[i]]);
            Point p2 = Calculations.worldToScreen(tile.getX() + getXVerticies()[getYTriangles()[i]], tile.getY() + getZVerticies()[getXTriangles()[i]], -getXVerticies()[getXTriangles()[i]]);
            Point p3 = Calculations.worldToScreen(tile.getX() + getXVerticies()[getZTriangles()[i]], tile.getY() + getZVerticies()[getZTriangles()[i]], -getXVerticies()[getZTriangles()[i]]);
            avX += (p1.x + p2.x + p3.x) / 3;
            avY += (p1.y + p2.y + p3.y) / 3;
        }
        return new Point(avX / len, avY / len);
    }


}
