package org.hexbot.api.wrapper;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.*;
import org.hexbot.api.methods.Menu;
import org.hexbot.api.wrapper.rs.RSModel;
import org.hexbot.bot.Bot;
import org.hexbot.impl.IEntity;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/28/13
 * Time: 5:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class Entity extends Animable implements Locatable {

    public Entity(IEntity inst) {
        super(inst);
    }

    public boolean isInCombat() {
        return getInteractingIndex() > -1;
    }

    public int getLocalX() {
        return get().getX() >> 7;
    }

    public int getLocalY() {
        return get().getY() >> 7;
    }

    public int getSpeed() {
        return get().getSpeed();
    }

    public String getName() {
        return "N/A";
    }

    public boolean isMoving() {
        return getSpeed() > 0;
    }

    public int getX() {
        return getLocalX() + Game.getBaseX();
    }

    public int getY() {
        return getLocalY() + Game.getBaseY();
    }

    public int getInteractingIndex() {
        return get().getInteractingIndex();
    }

    public int getHp() {
        return get().getHp();
    }

    public int getMaxHp() {
        return get().getMaxHp();
    }

    public int getHpPercent() {
        return (getHp() / getMaxHp()) * 100;
    }

    public IEntity get() {
        return (IEntity) instance.get();
    }

    public Entity getInteracting() {
        int index = getInteractingIndex();
        if (index == -1) {
            return null;
        }
        if (index < 0x8000) {
            return new Npc(Bot.client.getNpcs()[index]);
        } else if (index - 0x8000 == Bot.client.getMyPlayerIndex()) {
            return Players.getLocal();
        } else {
            return new Player(Bot.client.getPlayers()[index - 0x8000]);
        }
    }

    public Tile getLocation() {
        return new Tile(getX(), getY());
    }
    /**
     * @author Viewer
     * @return
     */
    public boolean isOnMinimap() {
        return this != null ? Calculations.distanceTo(getLocation()) <= 15 : false;
    }
    @Override
    public boolean interact(String s) {
        Mouse.moveLocatable(this, true);
        if (Menu.getMenuItems().get(0).contains(s)) {
            Mouse.click(true);
        } else {
            return Menu.click(s);
        }
        return true;
    }

    public Tile getRelativeLocation() {
        return new Tile(getLocalX(), getLocalY());
    }

    public Point getScreenLocation() {
        RSModel m = null;
        if ((m = getModel()) != null)
            return m.getCentralPoint();
        return getLocation().getScreenLocation();
    }

    @Override
    public void click() {
        Mouse.moveLocatable(this, true);
        Mouse.click(true);
    }

    @Override
    public boolean isOnScreen() {
        return Calculations.onGameScreen(getScreenLocation());  //To change body of implemented methods use File | Settings | File Templates.
    }

    public RSModel getModel() {
        RSModel m = super.getModel();
        if (m == null)
            return null;
        m.tile = new Tile(getRelativeLocation().getX() * 128 + 64, getRelativeLocation().getY() * 128 + 64);
        return m;
    }

    public int getAnimation() {
        return get().getAnimation();
    }

    public String getText() {
        return get().getText();
    }

    public int getOrientation() {
        return get().getOrientation();
    }

    public int[] getQueueX() {
        return get().getQueueX();
    }

    public int[] getQueueY() {
        return get().getQueueY();
    }

    public double getDistance() {
        return Calculations.distanceTo(this);
    }

    public double getDistanceTo(Tile tile) {
        return Calculations.distanceBetween(tile, getLocation());
    }
}
