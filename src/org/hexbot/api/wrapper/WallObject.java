package org.hexbot.api.wrapper;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.Calculations;
import org.hexbot.api.methods.Game;
import org.hexbot.bot.rsi.ClientStore;
import org.hexbot.impl.IWallObject;
import org.hexbot.util.Hook;
import org.hexbot.util.Storage;

import java.awt.*;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/15/13
 * Time: 4:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class WallObject implements GameObject {
    public IWallObject internal;
    int plane;

    public WallObject(IWallObject o, int plane) {
        internal = o;
        this.plane = plane;
    }
    public int getPlane() {
        return plane;
    }
    protected int getUID() {
        return internal.getId();
    }

    public int getId() {
        return getUID() >> 14 & 32767;
    }

    public int getX() {
        return getUID() & 0x7f;
    }

    public int getY() {
        return getUID() >> 7 & 0x7f;
    }


    public Point getScreenLocation() {
        return getLocation().getScreenLocation();
    }

    @Override
    public Tile getLocation() {
        return new Tile(Game.getBaseX() + getX(), Game.getBaseY() + getY(),getPlane());  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean interact(String s) {
        return org.hexbot.api.methods.Menu.interact(s, getLocation());  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isOnMinimap() {
        return this != null ? Calculations.distanceTo(getLocation()) <= 15 : false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isOnScreen() {
        return Calculations.onGameScreen(getScreenLocation());
    }

    public ObjectDef getDefinition() {
        try {
            Hook hook = Storage.getHook("getObjectDef()");
            Class clazz = ClientStore.get(hook.owner);
            Method method = clazz.getDeclaredMethod(hook.field, new Class[]{int.class});
            if (method != null) {
                return new ObjectDef(method.invoke(null, getId()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public String getName() {
        ObjectDef def = null;
        if ((def = getDefinition()) != null)
            return def.getName();
        return null;
    }
    public String[] getActions() {
        ObjectDef def = null;
        if ((def = getDefinition()) != null)
            return def.getActions();
        return null;
    }

    @Override
    public void click() {
        Mouse.click(getScreenLocation());
    }
}
