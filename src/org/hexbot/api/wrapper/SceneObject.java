package org.hexbot.api.wrapper;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.Calculations;
import org.hexbot.api.methods.Game;
import org.hexbot.api.methods.Menu;
import org.hexbot.bot.rsi.ClientStore;
import org.hexbot.impl.IModel;
import org.hexbot.impl.ISceneObject;
import org.hexbot.util.Hook;
import org.hexbot.util.Storage;

import java.awt.*;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/2/13
 * Time: 6:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class SceneObject implements GameObject {

    public ISceneObject internal;
    int plane;

    public SceneObject(ISceneObject o, int plane) {
        internal = o;
        this.plane = plane;
    }

    public int getPlane() {
        return plane;
    }

    protected int getUID() {
        return internal.getId();
    }

    public int getWorldX() {
        return internal.getWorldX();
    }

    public int getWorldY() {
        return internal.getWorldY();
    }

    protected int getHeight() {
        return internal.getId();
    }

    /**
     * @return
     * @author Viewer
     */
    public boolean isOnMinimap() {
        return this != null ? Calculations.distanceTo(getLocation()) <= 15 : false;
    }

    public int getId() {
        return getUID() >> 14 & 32767;
    }

    public int getX() {
        return getUID() & 0x7f;
    }

    public int getY() {
        return getUID() >> 7 & 0x7f;
    }

    public Model getModel() {
        if (internal.getModel() != null && internal.getModel() instanceof IModel)
            return new Model(internal.getModel(), new Tile(getWorldX(), getWorldY()));
        return null;
    }

    public Point getScreenLocation() {
        Model m;
        if ((m = getModel()) != null)
            return m.getCentralPoint();
        else
            return getLocation().getScreenLocation();

    }

    @Override
    public Tile getLocation() {
        return new Tile(Game.getBaseX() + getX(), Game.getBaseY() + getY(), getPlane());  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean interact(String s) {
        Mouse.moveLocatable(this, true);
        if (Menu.getMenuItems().get(0).contains(s)) {
            Mouse.click(true);
        } else {
            return Menu.click(s);
        }
        return true;
    }

    public boolean isOnScreen() {
        return Calculations.onGameScreen(getScreenLocation());
    }

    public ObjectDef getDefinition() {
        try {
            Hook hook = Storage.getHook("getObjectDef()");
            Class clazz = ClientStore.get(hook.owner);
            Method method = clazz.getDeclaredMethod(hook.field, new Class[]{int.class});
            if (method != null) {
                return new ObjectDef(method.invoke(null, getId()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getName() {
        ObjectDef def = null;
        if ((def = getDefinition()) != null)
            return def.getName();
        return null;
    }
    public String[] getActions() {
        ObjectDef def = null;
        if ((def = getDefinition()) != null)
            return def.getActions();
        return null;
    }
    @Override
    public void click() {
        Mouse.moveLocatable(this, true);
        Mouse.click(true);
    }
}
