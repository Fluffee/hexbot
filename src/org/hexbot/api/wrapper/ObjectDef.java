package org.hexbot.api.wrapper;

import org.hexbot.impl.IObjectDef;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/4/13
 * Time: 11:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectDef {
    public Object instance;

    public ObjectDef(Object obj) {
        instance = obj;
    }

    public String getName() {
        if (instance == null)
            return null;
        return ((IObjectDef) instance).getName();
    }

    public String[] getActions() {
        if (instance == null)
            return null;
        return ((IObjectDef) instance).getActions();
    }
}
