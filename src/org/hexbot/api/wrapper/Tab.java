package org.hexbot.api.wrapper;

import org.hexbot.api.input.Mouse;
import org.hexbot.api.methods.Widgets;

import java.awt.Graphics;
import org.hexbot.api.util.Time;
import org.hexbot.api.wrapper.Component;

public class Tab {
	
	public final static int TAB_WIDGET = 548;
	
	public static final int CLAN = 30;
	public static final int FRIENDS_LIST = 31;
	public static final int IGNORE_LIST = 32;
	public static final int LOGOUT = 33;
	public static final int OPTIONS = 34;
	public static final int EMOTES = 35;
	public static final int MUSIC = 36;
	public static final int COMBAT = 47;
	public static final int STATS = 48;
	public static final int QUEST = 49;
	public static final int INVENTORY = 50;
	public static final int EQUIPMENT = 51;
	public static final int PRAYER = 52;
	public static final int MAGIC = 53;

	/*
	 * Uses the TAB_IDS int[] to find the open tab id.
	 */
	public static int getOpenTabID(){
		for(Component c : Widgets.getChildren(TAB_WIDGET)){
			if(c != null && c.getTextureId() > 1000 && c.getId()>29){
				return c.getId();
			}
		}
		return -1;
	}
	
	/*
	 * Open's a tab with the given ID.
	 */
	public static void open(int TabID){
		for(Component c : Widgets.getChildren(TAB_WIDGET)){
			if(getOpenTabID() != TabID && c != null && c.getId() == TabID){
				c.click();
				Time.sleep(500, 750);
			}
		}
	}
	
	/*
	 * Checks if an tab given by id is open.
	 */
	public static boolean isOpen(int TabID){
		return getOpenTabID() == TabID;
	}
	
	/*
	 * Draws The tab.
	 */
	public static void drawTab(Graphics g, int TabID){
		for(Component c : Widgets.getChildren(TAB_WIDGET)){
			if(c != null && c.getId() == TabID){
				g.drawRect(c.getX(), c.getY(), c.getWidth(), c.getHeight());
			}
		}
	}

}
