package org.hexbot.api.wrapper;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/15/13
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
public interface GameObject extends Locatable {
    public int getId();

    public int getX();

    public int getY();

    public int getPlane();

    public Point getScreenLocation();

    @Override
    public Tile getLocation();

    @Override
    public boolean interact(String s);

    public boolean isOnMinimap();

    public boolean isOnScreen();

    public ObjectDef getDefinition();

    public String getName();

    @Override
    public void click();
}
