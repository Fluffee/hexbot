package org.hexbot.api.wrapper;

import org.hexbot.api.methods.Players;
import org.hexbot.bot.Bot;
import org.hexbot.impl.IPlayer;
import org.hexbot.impl.IPlayerDef;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/28/13
 * Time: 6:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class Player extends Entity {


    public Player(IPlayer inst) {
        super(inst);
    }

    public IPlayer get() {
        return (IPlayer) instance.get();
    }

    public String getName() {
        final IPlayer player = (IPlayer) instance.get();
        return player == null ? "NULL" : player.getName();
    }

    public IPlayerDef getDefinition() {
        return get().getDefinition();
    }

    public int getSkullIcon() {
        return get().getSkullIcon();
    }

    public int getPrayerIcon() {
        return get().getPrayerIcon();
    }

    public int getIndex() {
        int index = 0;
        if (this.equals(Players.getLocal())) {
            return Bot.client.getMyPlayerIndex();
        }
        for (IPlayer ip : Bot.client.getPlayers()) {
            if (ip == null) {
                index++;
                continue;
            }
            if (ip.equals(this.get())) {
                return index;
            } else {
                index++;
            }
        }
        return -1;
    }
    public boolean equals(Object object) {
        if (object instanceof Player) {
            return ((Player) object).get().equals(get());
        }
        return object.equals(this);
    }
}
