package org.hexbot.api.wrapper;

import org.hexbot.impl.IItemDef;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 4/4/13
 * Time: 9:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class ItemDef {
    public IItemDef instance;

    public ItemDef(IItemDef obj) {
        instance = obj;
    }

    public String getName() {
        return instance.getName();
    }

    public String[] getActions() {
        return instance.getActions();
    }
}
