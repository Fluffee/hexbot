package org.hexbot.api.events;

import org.hexbot.api.listeners.MessageListener;
import org.hexbot.script.Script;
import org.hexbot.script.ScriptHandler;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 5/6/13
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class MessageEvent {

    public static final int MESSAGE_SERVER = 0;
    public static final int MESSAGE_CHAT = 2;
    public static final int MESSAGE_PRIVATE_IN = 3;
    public static final int MESSAGE_PRIVATE_INFO = 5;
    public static final int MESSAGE_PRIVATE_OUT = 6;
    public static final int MESSAGE_FRIENDS_CHAT = 9;
    public static final int MESSAGE_CLIENT = 11;
    public static final int MESSAGE_EXAMINE_NPC = 28;
    public static final int MESSAGE_EXAMINE_OBJECT = 29;
    public static final int MESSAGE_CLAN_CHAT = 41;
    public static final int MESSAGE_TRADE_REQ = 100;
    public static final int MESSAGE_ASSIST_REQ = 102;
    public static final int MESSAGE_TRADE_INFO = 103;
    public static final int MESSAGE_ASSIST_INFO = 104;
    public static final int MESSAGE_ACTION = 109;


    private final int senderId;
    private final String owner;
    private final String text;
    private final String debug;

    public MessageEvent(int senderId, String owner, String text, String debug) {
        this.senderId = senderId;
        this.owner = owner;
        this.text = text;
        this.debug = debug;
    }

    public int getSenderId() {
        return senderId;
    }

    public String getOwner() {
        return owner;
    }

    public String getText() {
        return text;
    }

    public String getDebug() {
        return debug;
    }

    /**
     * Used for intercepting messages via client
     *
     * @param parameter owner id
     * @param a         sender
     * @param b         text
     * @param c         debug or some other shit
     */
    public static void onMessage(int parameter, String a, String b, String c) {
        Script script = ScriptHandler.getScript();
        if (script != null) {
            if (script instanceof MessageListener) {
                ((MessageListener) script).onMessage(new MessageEvent(parameter, a, b, c));
            }
        }
    }
}
