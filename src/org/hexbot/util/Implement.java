package org.hexbot.util;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/25/13
 * Time: 11:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class Implement {
    String name;
    String parent;

    public Implement(String name, String parent) {
        this.name = name;
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public String getParent() {
        return parent;
    }
}
