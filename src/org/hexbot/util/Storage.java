package org.hexbot.util;


import org.hexbot.bot.rsi.ClientStore;

import java.io.InputStream;
import java.net.URL;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/25/13
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class Storage {
    public static final LinkedList<Hook> hooks = new LinkedList<Hook>();
    public static final LinkedList<Implement> implementList = new LinkedList<Implement>();
    public static final ClientStore clientStore = new ClientStore();
    public static final HashMap<String, Hook> hookMap = new HashMap<>();
    public static final HashMap<String, String> wrapperMap = new HashMap<>();


    public static String getField(String wrapper, String fieldName) {
        for (Hook hook : hooks) {
            if (hook.getName().toLowerCase().contains(fieldName.toLowerCase()) && wrapper.toLowerCase().contains(getWrapper(hook.getOwner()).toLowerCase())) {
                return hook.getField();
            }
        }
        return null;
    }

    public static Hook getHook(String fieldName) {
        for (Hook hook : hooks) {
            if (hook.getName().toLowerCase().equals(fieldName.toLowerCase())) {
                return hook;
            }
        }
        return null;
    }

    public static Hook getHook(String clazz, String fieldName) {
        if (hookMap.containsKey(clazz + "." + fieldName)) {
            return hookMap.get(clazz + "." + fieldName);
        }
        for (Hook hook : hooks) {
            if (hook.getField().toLowerCase().equals(fieldName.toLowerCase())) {
                if (hook.getOwner().toLowerCase().equals(clazz.toLowerCase()))
                    return hook;
            }
        }
        return null;
    }

    public static String getWrapper(String clazz) {
        for (Implement im : implementList) {
            if (im.getParent().toLowerCase().equals(clazz.toLowerCase())) {
                return im.getName();
            }
        }
        System.out.println("No wrapper found for: " + clazz);
        return "none";
    }

    public static String getClass(String wrapper) {
        if (wrapperMap.containsKey(wrapper)) {
            return wrapperMap.get(wrapper);
        }
        for (Implement im : implementList) {
            if (im.getName().toLowerCase().contains(wrapper.toLowerCase())) {
                return im.getParent();
            }
        }
        return "none";
    }

    /**
     * Will read into the json file and move all the hooks and impls into the lists.
     */
    public void load() {
        List<Block> blocks = read();
        for (Block block : blocks) {
            if (block.map.size() == 2) {
                wrapperMap.put(block.map.get("name"), block.map.get("parent"));
                implementList.add(new Implement(block.map.get("name"), block.map.get("parent")));
            } else {
                hookMap.put(block.map.get("field") + "." + block.map.get("class"), new Hook(block.map.get("field"), block.map.get("class"), block.map.get("name"),
                        block.map.get("desc"), block.map.get("toinject"),
                        Integer.parseInt(block.map.get("multiplier")), Boolean.parseBoolean(block.map.get("static"))));
                hooks.add(new Hook(block.map.get("field"), block.map.get("class"), block.map.get("name"), block.map.get("desc"), block.map.get("toinject"),
                        Integer.parseInt(block.map.get("multiplier")), Boolean.parseBoolean(block.map.get("static"))));
            }
        }
    }

    public List<Block> read() {
        ArrayList<Block> blocks = new ArrayList<>();
        try {
            int i = 0;
            final String file = "http://www.hexbot.org/bot/hooks.json";
            PowerScanner scanner = new PowerScanner(new URL(file).openStream());
            while (scanner.hasNextLine()) {
                String read = scanner.nextLine();
                if (read.contains("{")) {
                    Block block = new Block();
                    String temp = "";
                    while (!(temp = scanner.nextLine()).contains("}")) {
                        block.add(temp.split(":"));
                    }
                    blocks.add(block);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return blocks;
    }

    public class Block {
        HashMap<String, String> map = new HashMap<>();

        public void add(String s, String t) {
            map.put(s, t);
        }

        public void add(String s[]) {
            map.put(s[0].trim(), s[1].trim());
        }
    }

    //lul, we do this
    class PowerScanner {
        Scanner scanner;

        public PowerScanner(InputStream source) {
            scanner = new Scanner(source);
        }

        public boolean hasNextLine() {
            return scanner.hasNextLine();
        }

        public String nextLine() {
            String read = null;
            try {
                read = scanner.nextLine();
                read = Crypto.decrypt("updater", read);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return read;
        }
    }
}
