package org.hexbot.util;


import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

import java.util.Collections;

public class Processor implements Opcodes {

    public static void addInterfacesTo(ClassNode cn, String... interfaces) {
        Collections.addAll(cn.interfaces, interfaces);
    }


    public static void addMultiplyGetter(ClassNode cn, FieldNode fn, String name, String desc, int multiplier, boolean isStatic) {
        if (fn == null) {
            System.out.println(name + " was broked");
            return;
        }

        MethodNode mn = new MethodNode(ACC_PUBLIC, name, "()" + desc, null, null);
        mn.instructions.add(new VarInsnNode(ALOAD, 0));
        mn.instructions.add(new FieldInsnNode(isStatic ? GETSTATIC : GETFIELD, cn.name, fn.name, fn.desc));
        mn.instructions.add(new LdcInsnNode(multiplier));
        mn.instructions.add(new InsnNode(IMUL));
        mn.instructions.add(new InsnNode(IRETURN));
        cn.methods.add(mn);
    }

    public static void addMultiplyGetter(ClassNode ti, ClassNode cn, FieldNode fn, String name, String desc, int multiplier, boolean isStatic) {
        if (fn == null) {
            System.out.println(name + " was broked");
            return;
        }
        MethodNode mn = new MethodNode(ACC_PUBLIC, name, "()" + desc, null, null);
        if (!isStatic)
            mn.instructions.add(new VarInsnNode(ALOAD, 0));
        mn.instructions.add(new FieldInsnNode(isStatic ? GETSTATIC : GETFIELD, cn.name, fn.name, fn.desc));
        mn.instructions.add(new LdcInsnNode(multiplier));
        mn.instructions.add(new InsnNode(IMUL));
        mn.instructions.add(new InsnNode(IRETURN));
        ti.methods.add(mn);
    }

    public static void addFieldGetter(ClassNode ti, ClassNode cn, FieldNode fn, String name, String desc, boolean isStatic) {
        if (fn == null || cn == null || ti == null) {
            System.out.println(name + " was broked");
            return;
        }
        MethodNode mn = new MethodNode(ACC_PUBLIC, name, "()" + desc, null, null);
        if (!isStatic)
            mn.instructions.add(new VarInsnNode(ALOAD, 0));
        mn.instructions.add(new FieldInsnNode(isStatic ? GETSTATIC : GETFIELD, cn.name, fn.name, fn.desc));
        mn.instructions.add(new InsnNode(ASMUtil.getReturnFor(fn.desc)));
        mn.visitMaxs(1, 1);
        mn.visitEnd();
        ti.methods.add(mn);
    }

    public static void addFieldGetter(ClassNode cn, FieldNode fn, String name, String desc, boolean isStatic) {
        if (fn == null) {
            System.out.println(name + " was broked");
            return;
        }
        MethodNode mn = new MethodNode(ACC_PUBLIC, name, "()" + desc, null, null);
        mn.instructions.add(new VarInsnNode(ALOAD, 0));
        mn.instructions.add(new FieldInsnNode(isStatic ? GETSTATIC : GETFIELD, cn.name, fn.name, fn.desc));
        mn.instructions.add(new InsnNode(ASMUtil.getReturnFor(fn.desc)));
        mn.visitMaxs(1, 1);
        mn.visitEnd();
        cn.methods.add(mn);
    }


    public static void addFieldGetter(ClassNode inject, ClassNode parent, String ifaceDesc, String desc, String field, int access, String name) {
        MethodNode mn = new MethodNode(ACC_PUBLIC, name, "()" + ifaceDesc, null, null);
        mn.instructions.add(new VarInsnNode(ALOAD, 0));
        mn.instructions.add(new FieldInsnNode(ASMUtil.access(access, ACC_STATIC) ? GETSTATIC : GETFIELD, parent.name, field, desc));
        mn.instructions.add(new InsnNode(ASMUtil.getReturnFor(ifaceDesc)));
        mn.visitMaxs(1, 1);
        mn.visitEnd();
        inject.methods.add(mn);
    }

    public static void addFieldGetter(ClassNode cn, String ifaceDesc, String desc, String field, int access, String name) {
        MethodNode mn = new MethodNode(ACC_PUBLIC, name, "()" + ifaceDesc, null, null);
        mn.instructions.add(new VarInsnNode(ALOAD, 0));
        mn.instructions.add(new FieldInsnNode(ASMUtil.access(access, ACC_STATIC) ? GETSTATIC : GETFIELD, cn.name, field, desc));
        mn.instructions.add(new InsnNode(ASMUtil.getReturnFor(ifaceDesc)));
        mn.visitMaxs(1, 1);
        mn.visitEnd();
        cn.methods.add(mn);
    }
}