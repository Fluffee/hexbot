package org.hexbot.util;

import java.awt.*;

/**
 * Collection of constant default values.
 */
public final class Configuration {

	/**
	 * Client version
	 */
	public static final float VERSION = 0.01F;

	/**
	 * The minimum size that the client can be.
	 */
	public static final Dimension CLIENT_MINIMUM_SIZE = new Dimension(770, 660);

	/**
	 * The default world to initially load
	 */
	public static final byte DEFAULT_WORLD = 1;

}
