package org.hexbot.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;


/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 12/23/12
 * Time: 11:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class VersionChecker {

    private static final double VERSION_NUMBER = 1.29;
    private static final String VERSIONURL = "http://www.hexbot.org/bot/version.txt";
    private static final String R_VER = "http://www.hexbot.org/bot/r_ver.txt";
    private static double R_VERSION = -1;

    public static boolean isLatest() throws IOException {
        double latestVersion = 1;
        final BufferedReader br = new BufferedReader(new InputStreamReader(
                new URL(VERSIONURL).openStream()));
        String input;
        if ((input = br.readLine()) != null) {
            latestVersion = Double.parseDouble(input.trim());
        }
        return getLocalVersion() == latestVersion;
    }

    public static double getLocalVersion() {
        return VERSION_NUMBER;
    }

    public static void downloadUpdate() {
        try {
            IOHelper.download("http://www.hexbot.org/bot/Hexbot-" + (VERSION_NUMBER + .01D) + ".jar", System.getProperty("user.dir") + File.separator + "Hexbot-" + (VERSION_NUMBER + .01D) + ".jar", null);
            System.out.println("Downloaded update to " + System.getProperty("user.dir") + File.separator + "Hexbot-" + (VERSION_NUMBER + .01D) + ".jar");
            Runtime.getRuntime().exec("java -jar Hexbot-" + (VERSION_NUMBER + .01D) + ".jar");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static File getHexFolder() {
        return new File(System.getProperty("user.home") + File.separator + "Hexbot" + File.separator);
    }

    private static File getLocalRandomsVersionFile() {
        try {
            return new File(getHexFolder().getCanonicalPath() + File.separator + "r_ver.txt");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    public static void affixRandomEvents() throws IOException {
        if (!hasHexbotFolder()) {
            getHexFolder().mkdirs();
        }
        if (getLocalRandomsVersionFile().exists()) {
            if (!areRandomsCurrent()) {
                downloadRandomsUpdate();
                FileTools.write(getRandomsVersion() + "", getLocalRandomsVersionFile());
            }
        } else {
            downloadRandomsUpdate();
            FileTools.write(getRandomsVersion() + "", getLocalRandomsVersionFile());
        }
    }

    private static void downloadRandomsUpdate() {
        try {
            IOHelper.download("http://www.hexbot.org/bot/" + getRandomsVersion() + ".jar", getHexFolder().getCanonicalPath() + "/r.jar", null);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static boolean hasHexbotFolder() {
        return getHexFolder().exists();
    }

    public static double getLocalRandomsVersion() throws IOException {
        return Double.parseDouble(IOHelper.readFile(getHexFolder().getCanonicalPath() + File.separator + "r_ver.txt")[0]);
    }

    public static double getRandomsVersion() throws IOException {
        if (R_VERSION > 0)
            return R_VERSION;
        double latestVersion = 1;
        final BufferedReader br = new BufferedReader(new InputStreamReader(
                new URL(R_VER).openStream()));
        String input;
        if ((input = br.readLine()) != null) {
            latestVersion = Double.parseDouble(input.trim());
        }
        return (R_VERSION = latestVersion);
    }

    public static boolean areRandomsCurrent() {
        try {
            return getLocalRandomsVersion() == getRandomsVersion();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return false;
    }
}


