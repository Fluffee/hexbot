package org.hexbot.util;

import sun.reflect.Reflection;

import java.text.Format;
import java.text.SimpleDateFormat;

public final class Debug {

	private static final Format time = new SimpleDateFormat("[hh:mm:ss]");

	private static String lastClass = "";

	public static void println(final String message) {
		//Lol, this is a really stupid way to do it.
		final String callerClass = Reflection.getCallerClass(2).getSimpleName();
		System.out.printf("%s: %15s | %s%n", time.format(System.currentTimeMillis()),
				(callerClass.equals(lastClass) ? "" : callerClass), message);
		if (!callerClass.equals(lastClass))
			lastClass = callerClass;
	}

	public static void println(final String from, final String message) {
		//Lol, this is a really stupid way to do it.
		System.out.printf("%s: %15s | %s%n", time.format(System.currentTimeMillis()),
				("AppletPanel".equals(lastClass) ? "" : "AppletPanel"), message);
		if (!"AppletPanel".equals(lastClass))
			lastClass = "AppletPanel";
	}

}
