package org.hexbot.util;

import org.hexbot.bot.Bot;
import org.hexbot.script.Manifest;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class IOHelper {


    public static String md5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static final String USER_AGENT = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)";

    public static String CURRENT_USER_AGENT = USER_AGENT;

    public static String useragent(final String program) {
        return USER_AGENT + " " + program + "/1.0";
    }

    public static void setAgent() {
        System.setProperty("http.agent", CURRENT_USER_AGENT != null ? CURRENT_USER_AGENT : USER_AGENT);
    }

    public static File getAccountFile() {
        return new File(System.getProperty("java.io.tmpdir") + File.separator + "1hex8" + File.separator + "acc.dat");
    }

    public static boolean hasAccountFile() {
        return getAccountFile().exists();
    }

    public static String[] readFile(String file) {
        String buffer = "";
        try {
            final BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                buffer += line + "#";
            }
            br.close();
        } catch (Exception e) {

        }
        return buffer.split("#");
    }

    public static String getMessage() {
        String out = null;
        try {
            out = null;
            if (!getAccountFile().exists())
                return null;
            String read = null;
            String aes = "SMR567LOL22";

            read = readFile(getAccountFile().getCanonicalPath())[0];
            String user = read.split(",")[0];
            String md5 = read.split(",")[1];
            user = Crypto.decrypt(aes, user.replaceAll(" ", "%20"));
            md5 = Crypto.decrypt(aes, md5);
            Bot.forumName = user;
            final BufferedReader br = new BufferedReader(new InputStreamReader(
                    new URL("http://hexbot.org/sdn/index.php/bot/login?username=" + user + "&password=" + md5).openStream()));
            String input;
            if ((input = br.readLine()) != null) {
                out = input.trim();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }

    public static String getScriptsMessage() {
        String out = null;
        try {
            out = null;
            if (!getAccountFile().exists())
                return null;
            String read = null;
            String aes = "SMR567LOL22";
            read = readFile(getAccountFile().getCanonicalPath())[0];
            String user = read.split(",")[0];
            String md5 = read.split(",")[1];
            user = Crypto.decrypt(aes, user.replaceAll(" ", "%20"));
            md5 = Crypto.decrypt(aes, md5);
            Bot.forumName = user;
            final BufferedReader br = new BufferedReader(new InputStreamReader(
                    new URL("http://hexbot.org/sdn/index.php/bot/scripts?username=" + user + "&password=" + md5).openStream()));
            String input;
            if ((input = br.readLine()) != null) {
                out = input.trim();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }

    public static void writeFile(String file, String buffer) {
        //    System.out.println(file);
        try {
            File file1 = new File(file).getParentFile();
            file1.mkdir();
            final BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(buffer);
            bw.newLine();
            bw.flush();
            bw.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String post(final String url, final String... args) {
        try {
            final StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (final String arg : args) {
                if (first) {
                    builder.append(arg);
                    first = false;
                } else {
                    builder.append("&").append(arg);
                }
            }
            final HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            //  connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("User-Agent", CURRENT_USER_AGENT != null ? CURRENT_USER_AGENT : USER_AGENT);
            final OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(builder.toString());
            writer.flush();
            final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            final StringBuilder result = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (final IOException e) {
            return null;
        }
    }

    public abstract static class DownloadListener {

        public abstract void onChange(final double percent);
    }

    public static void download(String url, final String file, final DownloadListener listener) throws IOException {
        final HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        final int length = connection.getContentLength();
        final InputStream input = connection.getInputStream();
        final OutputStream output = new FileOutputStream(new File(file));
        final byte[] buffer = new byte[4096];
        int bit;
        int downloaded = 0;
        while ((bit = input.read(buffer)) > 0) {
            downloaded += bit;
            if (listener != null) {
                listener.onChange((downloaded * 1.0 / length) * 100);
            }
            output.write(buffer, 0, bit);
        }
        output.flush();
        output.close();
        input.close();
    }


    public static long hashcode(final String script) {
        final Checksum checksum = new CRC32();
        final byte[] bytes = script.getBytes();
        checksum.update(bytes, 0, bytes.length);
        return checksum.getValue();
    }

    public static Class<?>[] getClasses(final String script) throws IOException, ClassNotFoundException {
        final List<Class<?>> classes = new ArrayList<>();
        final long hash = hashcode(script);
        // System.out.println(hash);
        final URL url = new URL("jar:http://hexbot.org/scripts/get.php?jar=" + script + "&hash=" + hash + "!/");
        final JarURLConnection connection = (JarURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        final URLClassLoader classloader = URLClassLoader.newInstance(new URL[]{url});
        final Enumeration<JarEntry> entries = connection.getJarFile().entries();
        while (entries.hasMoreElements()) {
            final String name = entries.nextElement().getName().replace('/', '.').replace('\\', '.');
            if (name.contains("$") || !name.endsWith(".class")) {
                continue;
            }
            classes.add(classloader.loadClass(name.substring(0, name.length() - 6)));
        }
        return classes.toArray(new Class<?>[classes.size()]);
    }

    private static Manifest getManifest(final Class<?>[] classes) {
        for (final Class<?> clazz : classes) {
            if (clazz.isAnnotationPresent(Manifest.class)) {
                return clazz.getAnnotation(Manifest.class);
            }
        }
        return null;
    }

    public static Class<?> getMainClass(final String id) {
        try {
            String read = null;
            String aes = "SMR567LOL22";
            read = readFile(getAccountFile().getCanonicalPath())[0];
            String user = read.split(",")[0];
            String md5 = read.split(",")[1];
            user = Crypto.decrypt(aes, user.replaceAll(" ", "%20"));
            md5 = Crypto.decrypt(aes, md5);
            final URL url = new URL("jar:http://hexbot.org/sdn/index.php/bot/jar/" + id + "?username=" + user + "&password=" + md5 + "!/");
            final JarURLConnection connection = (JarURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", CURRENT_USER_AGENT != null ? CURRENT_USER_AGENT : USER_AGENT);
            final URLClassLoader classloader = URLClassLoader.newInstance(new URL[]{url});
            final Enumeration<JarEntry> entries = connection.getJarFile().entries();
            Class<?> main = null;
            while (entries.hasMoreElements()) {
                final String name = entries.nextElement().getName().replace('/', '.').replace('\\', '.');
                if (name.contains("$") || !name.endsWith(".class")) {
                    continue;
                }
                final Class<?> clazz = classloader.loadClass(name.substring(0, name.length() - 6));
                if (clazz.isAnnotationPresent(Manifest.class)) {
                    main = clazz;
                    //  System.out.println(main.getSimpleName());
                }
            }
            return main;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//    public static Class<?> getMainClass(final String script) {
//        try {
//            String url = "http://hexbot.org/scripts/get.php";
//            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
//                connection.setRequestMethod("POST");
//               connection.setDoOutput(true);
//                connection.setDoInput(true);
//            connection.setRequestProperty("User-Agent", CURRENT_USER_AGENT != null ? CURRENT_USER_AGENT : USER_AGENT);
//            final OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
//            writer.write("jar=" + script + "&hash=" + hashcode(script));
//            writer.flush();
//            File temp = File.createTempFile((System.currentTimeMillis() + "").substring(1, 7), ".tmp");
//            temp.deleteOnExit();
//            JarOutputStream output = new JarOutputStream(new FileOutputStream(temp));
//            InputStream jarIn = connection.getInputStream();
//            JarInputStream input = new JarInputStream(jarIn);
//            ZipEntry next;
//            List<String> classes = new ArrayList<>();
//            byte[] tmp = new byte[1024];
//            int read;
//            while ((next = input.getNextEntry()) != null) {
//                output.putNextEntry(next);
//                classes.add(next.getName());
//                while ((read = input.read(tmp)) != -1)
//                    output.write(tmp, 0, read);
//            }
//            jarIn.close();
//            connection.disconnect();
//            output.close();
//            URLClassLoader classloader = URLClassLoader.newInstance(new URL[]{temp.toURI().toURL()});
//            for (String clazz : classes) {
//                Class<?> c = classloader.loadClass(clazz.substring(0, clazz.length() - 6));
//                if (c.isAnnotationPresent(Manifest.class) && c.newInstance() instanceof Script) {
//                    return c;
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    static {
        System.setProperty("http.agent", CURRENT_USER_AGENT != null ? CURRENT_USER_AGENT : USER_AGENT);
    }

}