package org.hexbot.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Jasper
 * Date: 4-5-13
 * Time: 12:54
 * To change this template use File | Settings | File Templates.
 */
public class FileTools {

    public static void write(String string, File target) {
        try {
            FileWriter fstream = new FileWriter(target);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(string);
            out.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
