package org.hexbot.util;

/**
 * Created with IntelliJ IDEA.
 * User: Tim
 * Date: 3/25/13
 * Time: 11:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class Hook {
    public String field;
    public String owner;
    public String name;
    public String type;
    public String toInject;
    public int multiplier;
    public boolean isStatic;

    public Hook(String field, String owner, String name, String type, int multiplier, boolean aStatic) {
        this.field = field;
        this.owner = owner;
        this.name = name;
        this.type = type;
        toInject = owner;
        this.multiplier = multiplier;
        isStatic = aStatic;
    }

    public Hook(String field, String owner, String name, String type, String toInject, int multiplier, boolean aStatic) {
        this.field = field;
        this.owner = owner;
        this.name = name;
        this.type = type;
        this.toInject = toInject;
        this.multiplier = multiplier;
        isStatic = aStatic;
    }

    public String getField() {
        return field;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public boolean isStatic() {
        return isStatic;
    }
}
